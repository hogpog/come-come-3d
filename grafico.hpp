#ifndef _GRAFICO_HPP_
#define _GRAFICO_HPP_

#define MAXX 20
#define MAXY 20

#include <irrlicht.h>
#include <vector>
#include "event.hpp"
#include "cameraControl.hpp"

#include "PostProcessBloom.hpp"
#include "PostProcessRadialBlur.h"
#include "PostProcessMotionBlur.h"
#include "PostProcessBlur.h"
#include "effectWrapper.h"
#include "tumle.h"
#include <ctime>
#include <string>

#define BLOOM 1
#define MOTIONBLUR 3
#define RADIALBLUR 5
#define BLUR 7

#define VELOCIDADE 1
#define CONGELA 3


using namespace irr;

using namespace tumle;


class CGrafico
{
private:
    ///*******GUI
    IGUIEnvironment* env;
    IGUISkin* skin;
    IGUIFont* font;
    IGUIStaticText* pontosText;
    IGUIImage * vidaimg[3];
    IGUIImage * fundopts;
    IGUIImage * morreuImagem;

    ///***********
    int initnpcX,initnpcY;
    float itime;
    IPhysicsManager *pmgr;
    IMaterialManager *mmgr;
    IControllablePhysicsNode * pNode;
    int velocidadePlayer;
    int VelocidadeFantasma;
    float tempoPowerUP;
    int POWERUP;
    bool bCanDoGLSL_1_1;

    followingCamera *camera;
    void CreatTile(core::vector3df pos,
                   video::ITexture* colorMap,video::ITexture* normalMap,
                   scene::IAnimatedMesh* mesh);
    void CreatTileShadow(core::vector3df pos,
                         video::ITexture* colorMap,video::ITexture* normalMap,
                         scene::IAnimatedMesh* mesh);
    void CreatTileBolinha(core::vector3df pos);
    void PowerUpVelocidade(core::vector3df pos);
    void PowerUpCongela(core::vector3df pos);

    std::vector<video::ITexture*> colorMap;
    std::vector<video::ITexture*> normalMap;
    std::vector<scene::IAnimatedMesh*> Mesh;
    std::vector<scene::IAnimatedMeshSceneNode*> fantasmas;

    video::ITexture* playerColorMap;
    video::ITexture* playerNormalMap;
    scene::IAnimatedMesh* playerMesh;
    scene::IAnimatedMeshSceneNode* playerNode;
    int playerDir;

    /// particle system ///
    scene::IParticleSystemSceneNode* ps;
    scene::IParticleSystemSceneNode* ps2;
    scene::IParticleSystemSceneNode* ps3;
    scene::IParticleSystemSceneNode* ps4;
    scene::IParticleEmitter* EmRing;
    scene::IParticleEmitter* EmBox;
    scene::IParticleEmitter* EmMesh;
    ///

    vector3df pos;
    int contadorbolinhax;
    int contadorbolinhay;
    scene::ISceneNode* bolinha[MAXX][MAXY];
    int LastDir;
    int DirToMove;
    scene::ILightSceneNode* node;

    int Effect;
    int vida;

public:
    core::vector3df iniciofantasma;
    int ponto;
    int bolinhas;
    int playerPosX;
    int playerPosY;

    CEvent receiver;
    CGrafico();
    ~CGrafico();
    int mapa[MAXX][MAXY];
    int bmapa[MAXX][MAXY];
    void CreateMap(int map[MAXX][MAXY]);
    void CreatebMap(int map[MAXX][MAXY]);
    void addNpc(int *posx,int *posy);
    void Run();
    void Initialize();
    void LoadResources();
    void LoadNpc(core::vector3df posfantasma,
                 video::ITexture* colorMap,video::ITexture* normalMap,
                 scene::IAnimatedMesh* mesh);
    bool UpdatePos();
    void ProcessEffects();
    IPostProcessMotionBlur *MotionBLur;
    IPostProcessBloom *Bloom;
    IPostProcessRadialBlur *RadialBlur;
    IPostProcessBlur *Blur1;
    IPostProcessBlur *Blur2;
    s32 mtlToonShader;


    ///Variaveis publicas:
    video::E_DRIVER_TYPE driverType;
    IrrlichtDevice* device;
    video::IVideoDriver* driver;
    scene::ISceneManager* smgr;
    std::vector<int*> npcsx;
    std::vector<int*> npcsy;
    std::vector<core::vector3df> playerPos;
};


#endif
