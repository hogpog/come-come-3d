// CMaterialInteraction.cpp
// a part of Tumle
#include "CMaterialInteraction.h"

namespace tumle
{

//! constructor
CMaterialInteraction::CMaterialInteraction(IMaterialManager *mmgr,
                                          u32 index,
                                          materialID material1,
                                          materialID material2,
                                          f32 elasticityCoef,
                                          f32 staticFrictionCoef,
                                          f32 kineticFrictionCoef,
                                          f32 softnessCoef,
                                          bool collidableState):
IMaterialInteraction(), MaterialManager(mmgr), Index(index), PhysicsNodeOne(0), PhysicsNodeTwo(0),
MaterialOne(material1), MaterialTwo(material2), Elasticity(elasticityCoef),StaticFriction(staticFrictionCoef),
KineticFriction(kineticFrictionCoef), Softness(softnessCoef), Collidable(collidableState), userData(0)
{
    nWorld = MaterialManager->getPhysicsManager()->getNewtonWorld();

    //set the default callbacks of the interaction
    NewtonMaterialSetCollisionCallback (nWorld,
                                        MaterialOne,
                                        MaterialTwo,
                                        this,
                                        defaultBegin,
                                        defaultProcess,
                                        defaultEnd);

    //make sure the function pointers aren't dangeling
    setCollisionCallback(0,0,0);


}

void CMaterialInteraction::remove()
{
    //stop the callbacks from being called
    NewtonMaterialSetCollisionCallback (nWorld,
                                    MaterialOne,
                                    MaterialTwo,
                                    this,
                                    NULL,NULL,NULL);

    //make sure the function pointers aren't dangeling
    setCollisionCallback(0,0,0);
    //delete the material interaction class from the material manager
    if(MaterialManager)
        MaterialManager->setPointerToZero(Index);

    delete this;
}

//! set the functions to call for a collision between the two materials
void CMaterialInteraction::setCollisionCallback(void (*beginCall)(IMaterialInteraction *object),
                                                void (*processCall)(IMaterialInteraction *object),
                                                void (*endCall)(IMaterialInteraction *object))
{
    Begin = beginCall;
    Process = processCall;
    End = endCall;
}

//the default begin collision callback
int CMaterialInteraction::defaultBegin(const NewtonMaterial* material, const NewtonBody* body1, const NewtonBody* body2)
{
    // get the material interaction class that holds the materials
	CMaterialInteraction *currentInteraction = (CMaterialInteraction*)NewtonMaterialGetMaterialPairUserData(material);

	// save the colliding physics nodes
	if((IPhysicsNode*)NewtonBodyGetUserData((NewtonBody*)body1))
        currentInteraction->setCollisionNodeOne((IPhysicsNode*)NewtonBodyGetUserData((NewtonBody*)body1) );

	if((IPhysicsNode*)NewtonBodyGetUserData((NewtonBody*)body2))
        currentInteraction->setCollisionNodeTwo((IPhysicsNode*)NewtonBodyGetUserData((NewtonBody*)body2) );

	// clear the contact normal speed
	currentInteraction->setCollisionNormalSpeed(0.0f);

	// clear the contact sliding speed
	currentInteraction->setCollisionSlidingSpeed(0.0f);

    //if the user has defined a collision callback - run it
    if(currentInteraction->Begin)
        currentInteraction->Begin(currentInteraction);

	// return one the tell Newton the application wants to process this contact
	return 1;
}

//the default process collision callback
int CMaterialInteraction::defaultProcess(const NewtonMaterial* material, const NewtonContact* contact)
{
    // get the material interaction class that holds the materials
	CMaterialInteraction *currentInteraction = (CMaterialInteraction*)NewtonMaterialGetMaterialPairUserData(material);

	f32 nSpeed;
	float positionTransferVector[3];
    float normalTransferVector[3];

	// get the normal speed of this impact and put it to the interaction class
	nSpeed = NewtonMaterialGetContactNormalSpeed(material, contact);
	currentInteraction->setCollisionNormalSpeed(nSpeed);

	// get the position and normail of the impact and put it to the interaction class
	NewtonMaterialGetContactPositionAndNormal (material, positionTransferVector, normalTransferVector);
    currentInteraction->setCollisionPoint(core::vector3df(positionTransferVector[0],
                                                          positionTransferVector[1],
                                                          positionTransferVector[2]));

    currentInteraction->setCollisionNormal(core::vector3df(normalTransferVector[0],
                                                           normalTransferVector[1],
                                                           normalTransferVector[2]));


	f32 speed0;
//	f32 speed1;
	// get the maximum of the two sliding contact speeds
	speed0 = NewtonMaterialGetContactTangentSpeed (material, contact, 0);
/*	speed1 = NewtonMaterialGetContactTangentSpeed (material, contact, 1);

	//check for the bigger or smaller
	if (speed1 > speed0)
	   speed0 = speed1;
*/

	// put the speed in the interaction class
	currentInteraction->setCollisionSlidingSpeed(speed0);

    //if the user has defined a collision callback - run it
    if(currentInteraction->Process)
        currentInteraction->Process(currentInteraction);

	// return one to tell Newton we want to accept this contact
	return 1;
}


void CMaterialInteraction::defaultEnd(const NewtonMaterial* material)
{
    // get the material interaction class that holds the materials
    CMaterialInteraction *currentInteraction = (CMaterialInteraction*)NewtonMaterialGetMaterialPairUserData(material);

    //if the user has defined a collision callback - run it
    if(currentInteraction->End)
        currentInteraction->End(currentInteraction);
}



void CMaterialInteraction::disableMoveThrough()
{

    if(PhysicsNodeOne)
    {
        if(PhysicsNodeOne->getType() == EPNT_CONTROLLABLE)
        {
            //cast the IPhysicsNode in to a IControllablePhysicsNode
            IControllablePhysicsNode *cPNode = (IControllablePhysicsNode*)PhysicsNodeOne;
            //diable movement in the opposit direction of the collision normal
            //be setting the desired velocity to 0
            core::vector3df dVeloc = cPNode->getDesiredVelocity();

            //because the normal isn't always precise I use 0.01f in stead of 0
            if(CollisionNormal.X > 0.01f)
            {
                if(dVeloc.X < 0)
                    dVeloc.X = 0;
            }
            if(CollisionNormal.X < -0.01f)
            {
                if(dVeloc.X > 0)
                    dVeloc.X = 0;
            }
            if(CollisionNormal.Z > 0.01f)
            {
                if(dVeloc.Z < 0)
                    dVeloc.Z = 0;
            }
            if(CollisionNormal.Z < -0.01f)
            {
                if(dVeloc.Z > 0)
                    dVeloc.Z = 0;
            }
            cPNode->setDesiredVelocity(dVeloc);

        }//end if type
    }//en if physics node

    if(PhysicsNodeTwo)
    {
        if(PhysicsNodeTwo->getType() == EPNT_CONTROLLABLE)
        {

            //cast the IPhysicsNode in to a IControllablePhysicsNode
            IControllablePhysicsNode *cPNode = (IControllablePhysicsNode*)PhysicsNodeTwo;
            //diable movement in the opposit direction of the collision normal
            //be setting the desired velocity to 0
            core::vector3df dVeloc = cPNode->getDesiredVelocity();

            //because the normal isn't always precise I use 0.01f
            if(CollisionNormal.X > 0)
            {
                if(dVeloc.X < 0)
                    dVeloc.X = 0;
            }
            if(CollisionNormal.X < 0)
            {
                if(dVeloc.X > 0)
                    dVeloc.X = 0;
            }
            if(CollisionNormal.Z > 0)
            {
                if(dVeloc.Z < 0)
                    dVeloc.Z = 0;
            }
            if(CollisionNormal.Z < 0)
            {
                if(dVeloc.Z > 0)
                    dVeloc.Z = 0;
            }
            cPNode->setDesiredVelocity(dVeloc);
        }//end if type
    }//end if physics node

}//end function

}//end namespace tumle
