// EBodyType.h
//a part of Tumle

#ifndef TUMLE_E_BODY_TYPE_H
#define TUMLE_E_BODY_TYPE_H

namespace tumle
{

//! enum for the different kind of bodies
enum E_BODY_TYPE
{
     //! box body
     EBT_BOX,

     //! ellipsoid body
     EBT_SPHERE,

     //! cone body
     EBT_CONE,

     //! capsule body
     EBT_CAPSULE,

     //! cylinder body
     EBT_CYLINDER,

     //! chamfer cylinder body
     EBT_CHAMFER_CYLINDER,

     //! body made from the bounding box of the irrlicht node
     EBT_BOUNDING_BOX,

     //! body made from the mesh of the irrlicht node
     EBT_MESH,

     //! body made from an arbitrary mesh
     EBT_EXTERN_MESH,

     //! body made from a level mesh
     EBT_LEVEL,

     //! number of body types
     EBT_COUNT
};

}//end namespace tumle

#endif
