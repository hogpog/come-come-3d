// IControllablePhysicsNode.h
// a part of Tumle

#ifndef TUMLE_I_CONTROLLABLE_PHYSICS_NODE_H
#define TUMLE_I_CONTROLLABLE_PHYSICS_NODE_H

#include "tumle.h"

using namespace irr;

namespace tumle
{

//! controllable physics node interface
/*! this is a node that can be controlled (moved) with the keyboard */
class IControllablePhysicsNode : public IPhysicsNode
{
public:
    //! constructor
    IControllablePhysicsNode():IPhysicsNode() {};

    //! update the position of the node
    /*! use this function in the event receiver to be able to controll the node */
    virtual void update(SEvent *event) = 0;

    //! set the disered velocity of the node
    /*! this function is used by the update function */
    virtual void setDesiredVelocity(core::vector3df newVeloc) = 0;

    //! get the disered velocity of the node
    /*! this function is used by the update function */
    virtual core::vector3df getDesiredVelocity() = 0;

    //! get the disered omega of the node
    /*! this function is used by the update function */
    virtual core::vector3df getDesiredOmega() = 0;

    //!get the move speed
    /*! \return returns the move speed of the node */
    virtual f32 getMoveSpeed() = 0;

    //! get the rotation speed
    /*! \return returns the rotation speed of the node */
    virtual f32 getRotateSpeed() = 0;

    //!set the move speed
    virtual void setMoveSpeed(f32 newSpeed) = 0;

    //! get the rotation speed
    virtual void setRotateSpeed(f32 newRotSpeed) = 0;

    //! get if the node strafes
    /*! this function controlles if the node should rotate or strafe when the left and right keys are pressed */
    virtual bool doesStrafe() = 0;

    //! set if the force should be capped
    virtual void setCapForceMode(bool mode) = 0;

    //! get if the force is capped
    virtual bool getCapForceMode() = 0;

    //! set max/min force
    virtual void setCapForceAt(f32 cap) = 0;

    //! get the force cap
    virtual f32 getCapForceAt() = 0;

    //! caps the passed array
    //! \param force: an array of 3 floats
    /*! this function does nothing more on it own, other than taking the top of to large values in the
    passed array */
    virtual void capForceDo(f32* force) = 0;


};

}//end namespace tumle

#endif
