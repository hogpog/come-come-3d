// CMaterialInteraction.h
//a part of Tumle

#ifndef TUMLE_C_MATERIAL_INTERACTION_H
#define TUMLE_C_MATERIAL_INTERACTION_H


#include "tumle.h"

namespace tumle
{

class IMaterialManager;

//! this class controls what happens when to physics nodes collide
class CMaterialInteraction : public IMaterialInteraction
{
public:

    //! constructor
    CMaterialInteraction(IMaterialManager *mmgr,
                         u32 index,
                         materialID material1,
                         materialID material2,
                         f32 elasticityCoef,
                         f32 staticFrictionCoef,
                         f32 kineticFrictionCoef,
                         f32 softnessCoef,
                         bool collidableState);

    //! destructor
    virtual ~CMaterialInteraction(){};

    //! delete the material interaction
    virtual void remove();

    //! set the elasicity coefficient for the collision.
    virtual void setElasticity(f32 elasticityCoef)
        {
            NewtonMaterialSetDefaultElasticity(nWorld, MaterialOne ,MaterialTwo, elasticityCoef);
            Elasticity = elasticityCoef;
        };

    //! set the friction coefficients for the collision.
    virtual void setFriction(f32 staticFriction, f32 kineticFriction)
        {
            NewtonMaterialSetDefaultFriction(nWorld, MaterialOne, MaterialTwo, staticFriction, kineticFriction);
            StaticFriction = staticFriction;
            KineticFriction = kineticFriction;
        };

    //! set the softness coefficients for the collision.
    virtual void setSoftness(f32 softnessCoef)
        {
            NewtonMaterialSetDefaultSoftness(nWorld, MaterialOne, MaterialTwo, softnessCoef);
            Softness = softnessCoef;
        };

    //! set if a collision will take place
    virtual void setCollidable(bool state)
        {
            NewtonMaterialSetDefaultCollidable(nWorld, MaterialOne, MaterialTwo, state);
            Collidable = state;
        };

    //! set the ContinuousCollisionMode of the material
    /*! this can be used to prevent heigh speed nodes to move trough other nodes */
    virtual void setContinuousCollisionMode(bool ccm)
        {
            NewtonMaterialSetContinuousCollisionMode(nWorld, MaterialOne, MaterialTwo, ccm);
            ContinuosCollision = ccm;
        };

    //! get the ContinuousCollisionMode of the material
    virtual bool getContinuousCollisionMode() { return ContinuosCollision; };

    //! get the elasicity coefficient for the collision.
    virtual f32 getElasticity(){ return Elasticity; };

    //! get the static friction coefficient for the collision.
    virtual f32 getStaticFriction(){ return StaticFriction; };

    //! get the kinetic friction coefficient for the collision.
    virtual f32 getKineticFriction(){ return KineticFriction; };

    //! get the softness coefficients for the collision.
    virtual f32 getSoftness(){ return Softness; };

    //! get if a collision will take place
    virtual bool getCollidable(){ return Collidable; };

    //! get the position of a collision between the two materials
    /*! use this in the collision callbacks */
    virtual core::vector3df getCollisionPoint(){ return CollisionPoint; };

    //! get the normal vector of a collision between the two materials
    /*! use this in the collision callbacks */
    virtual core::vector3df getCollisionNormal(){ return CollisionNormal; };

    //! get the collision normal speed
    /*! use this in the collision callbacks */
    virtual f32 getCollisionNormalSpeed(){ return CollisionNormalSpeed;};

    //! get the collision sliding speed
    /*! use this in the collision callbacks */
    virtual f32 getCollisionSlidingSpeed(){ return CollisionSlidingSpeed;} ;

    //! the the colliding node one or two
    /*! use this in the collision callbacks */
    virtual IPhysicsNode *getCollisionNodeOne(){ return PhysicsNodeOne; };

    //! the the colliding node one or two
    /*! use this in the collision callbacks */
    virtual IPhysicsNode *getCollisionNodeTwo(){ return PhysicsNodeTwo; };


    ////

    //! set the position of a collision between the two materials
    virtual void setCollisionPoint(core::vector3df colPos){CollisionPoint = colPos; };

    //! set the normal vector of a collision between the two materials
    virtual void setCollisionNormal(core::vector3df colNorm){ CollisionNormal = colNorm; };

    //! set the collision normal speed
    virtual void setCollisionNormalSpeed(f32 colNormSpeed){ CollisionNormalSpeed = colNormSpeed;};

    //! set the collision sliding speed
    virtual void setCollisionSlidingSpeed(f32 colSlideSpeed){ CollisionSlidingSpeed = colSlideSpeed;} ;

    //! set the the colliding node one or two
    virtual void setCollisionNodeOne(IPhysicsNode* pNode){ PhysicsNodeOne = pNode; };

    //! set the the colliding node one or two
    virtual void setCollisionNodeTwo(IPhysicsNode* pNode){ PhysicsNodeTwo = pNode; };

    //! set the functions to call for a collision between the two materials
    /*! this can be used to play sounds or make sparks(paricles) ect.
    the functions must be of the type: 'myFunction(IMaterialInteraction *someName)'
        \param beginCall: the function to call when a collision begins
        \param processCall: the function to call during a collision.
        \param endCall: the function to call when a collision ends */
    virtual void setCollisionCallback(void (*beginCall)(IMaterialInteraction *object),
                                      void (*processCall)(IMaterialInteraction *object),
                                      void (*endCall)(IMaterialInteraction *object));

    //! get the material manager
    /*! \return returns the material manager that was used to create this interaction */
    virtual IMaterialManager *getMaterialManager(){ return MaterialManager; };

    //!get the physics manager
    /*! \return returns the physics manager */
    virtual IPhysicsManager *getPhysicsManager(){ return MaterialManager->getPhysicsManager(); };

    //! get the irrlicht device
    /*! \return returns the irrlicht device that was passed to the physics manager on creation */
    virtual IrrlichtDevice *getIrrlichtDevice(){ return getPhysicsManager()->getIrrlichtDevice(); };

    //! get the newton world
    /*! \return returns the newton world that was passed to the physics manager on creation */
    virtual NewtonWorld *getNewtonWorld(){ return getPhysicsManager()->getNewtonWorld(); };

    //! set user data
    virtual void setUserData(void *ud){ userData = ud; };

    //! get user data
    virtual void *getUserData(){ return userData; };

    //! disable the controlleble node from moveing fourther in to the other node
    /*! This is done be disableing movement of the controllable node in the direction
    of the other node when a collision happens, this means the controllable node will not
    'try' to move trough the other node nor to push it. <br>
    Use this function in the process callback for it to work properly. */
    virtual void disableMoveThrough();


    //default callbacks, used to get the speed, point and materials of the collision
    static int defaultBegin(const NewtonMaterial* material, const NewtonBody* body0, const NewtonBody* body1);
    static int defaultProcess(const NewtonMaterial* material, const NewtonContact* contact);
    static void defaultEnd(const NewtonMaterial* material);

    //! pointer to begin collision callback function
    void (*Begin)(IMaterialInteraction *object);

    //! pointer to process collision callback function
    void (*Process)(IMaterialInteraction *object);

    //! pointer to end collision callback function
    void (*End)(IMaterialInteraction *object);

    //! set the name of the node
    virtual void setName(const c8 *newName){ Name = newName; };

    //! get the name of the node
    virtual const c8* getName(){ return Name.c_str(); };

private:
    //! pointer to the world
    NewtonWorld *nWorld;

    //! pointer to the material manager
    IMaterialManager *MaterialManager;

    //! index of the array in the material manager
    u32 Index;

    //! physics node one
    IPhysicsNode *PhysicsNodeOne;

    //! physics node two
    IPhysicsNode *PhysicsNodeTwo;

    //! material one
    materialID MaterialOne;

    //! material two
    materialID MaterialTwo;

    //! elasicity coefficient
    f32 Elasticity;

    //! static friction coefficient
    f32 StaticFriction;

    //! kinetic friction coefficient
    f32 KineticFriction;

    //! softness coefficient
    f32 Softness;

    //! collidable
    bool Collidable;

    //! continous collision
    bool ContinuosCollision;

    //! the point of collision
    core::vector3df CollisionPoint;

    //! the normal of the collision
    core::vector3df CollisionNormal;

    //! the normal speed of a collision
    f32 CollisionNormalSpeed;

    //! the sliding speed of a collision
    f32 CollisionSlidingSpeed;

    //! user data
    void *userData;

    //! the name of the interaction
    core::stringc Name;
};

} // end namespace tumle

#endif



