//CCameraFPSPhysicsNode.cpp
//a part of Tumle

#include "CCameraFPSPhysicsNode.h"

namespace tumle
{

CCameraFPSPhysicsNode::CCameraFPSPhysicsNode(IPhysicsManager *pmgr,
                                                   s32 managerIndex,
                                                   scene::ICameraSceneNode *node,
                                                   NewtonBody *body,
                                                   E_BODY_TYPE type,
                                                   f32 mass,
                                                   SKeyMap *keyMapArray,
                                                   u32 keyMapSize,
                                                   f32 moveSpeed,
                                                   f32 rotateSpeed):
ICameraFPSPhysicsNode(), PhysicsManager(pmgr), Index(managerIndex), Node(node),
Body(body), BodyType(type), Type(EPNT_CAMERA), Mass(mass), Material(0), ms(moveSpeed), rs(rotateSpeed),
capForce(false), UserData(0), Gravity(0,0,0), useDefaultTransformCallback(true), useDefaultForceAndTorqueCallback(true)
{

    //reset the force and torque array
    force[0] = 0; force[1] = 0; force[2] = 0;
    torque[0] = 0; torque[1] = 0; torque [2] = 0;

    //reset addition callbacks
    additionalForceAndTorqueCallback = 0;
    additionalTransformCallback = 0;

    //activate continousCollision for the body (this makes collision more precise)
    NewtonBodySetContinuousCollisionMode(Body, true);

	// create key map (pasted from CCameraFPSSceneNode)
	if (!keyMapArray || !keyMapSize)
	{
		// create default key map
		KeyMap.push_back(SNodeKeyMap(0, irr::KEY_UP));
		KeyMap.push_back(SNodeKeyMap(1, irr::KEY_DOWN));
		KeyMap.push_back(SNodeKeyMap(2, irr::KEY_LEFT));
		KeyMap.push_back(SNodeKeyMap(3, irr::KEY_RIGHT));
		KeyMap.push_back(SNodeKeyMap(4, irr::KEY_KEY_J));
	}
	else
	{
		// create custom key map

		for (u32 i=0; i<keyMapSize; ++i)
		{
			switch(keyMapArray[i].Action)
			{
			case EKA_MOVE_FORWARD: KeyMap.push_back(SNodeKeyMap(0, keyMapArray[i].KeyCode));
				break;
			case EKA_MOVE_BACKWARD: KeyMap.push_back(SNodeKeyMap(1, keyMapArray[i].KeyCode));
				break;
			case EKA_STRAFE_LEFT: KeyMap.push_back(SNodeKeyMap(2, keyMapArray[i].KeyCode));
				break;
			case EKA_STRAFE_RIGHT: KeyMap.push_back(SNodeKeyMap(3, keyMapArray[i].KeyCode));
				break;
			case EKA_JUMP_UP: KeyMap.push_back(SNodeKeyMap(4, keyMapArray[i].KeyCode));
				break;
			default:
				break;
			} // end switch
		} // end for
	}// end if

    for(u32 i = 0 ; i < 6 ; i++)
    {
        Keys[i] = false;
    }

}

//! destructor
CCameraFPSPhysicsNode::~CCameraFPSPhysicsNode()
{
    if(Body)
        NewtonDestroyBody(PhysicsManager->getNewtonWorld(), Body);
/*
    //remove the node from any material interaction class
    if(Material)
    {
        core::array<IMaterialInteraction*> createdInteractions = PhysicsManager->getMaterialManager()->getCreatedInteractions();
        IMaterialInteraction *mi = 0;
        for(s32 i = 0 ; i <  createdInteractions.size(); i++)
        {
            mi = createdInteractions[i];

            if(mi->getCollisionNodeOne()->getIrrlichtNode() == Node)
                mi->removeCollisionNodes();

            if(mi->getCollisionNodeTwo()->getIrrlichtNode() == Node)
                mi->removeCollisionNodes();

        };
    }//if Material
*/
    if(Node)
        Node->remove();

}

//! delete the physics node
void CCameraFPSPhysicsNode::remove()
{
    //set the mass of the body to be removed to 0, this helps with some errors
    NewtonBodySetMassMatrix(Body, 0,0,0,0);

    //remove the pointer to the node from the array in the physics manager
    PhysicsManager->setPointerToZero(Index);
    delete this;

}

//! update the position of the node
void CCameraFPSPhysicsNode::update(SEvent *event)
{
	//get the pressed keys
	if (event->EventType == EET_KEY_INPUT_EVENT)
	{
		const u32 cnt = KeyMap.size();
		for (u32 i = 0 ; i < cnt ; ++i)
			if (KeyMap[i].keycode == event->KeyInput.Key)
			{
				Keys[KeyMap[i].action] = event->KeyInput.PressedDown;
			}//if key
	}//if key input


    //reset the desired velocity and omega
    desiredVelocity = core::vector3df(0,0,0);

    /* the node is moved in the force and torque callback with the following equation
       provided from Leadwerks on the Newton forum:
       force = ( desiredVelocity - velocity ) * mass */

    //move the node
    //Forward
    if(Keys[0])
        desiredVelocity.Z = ms;

    //Backward
    if(Keys[1])
        desiredVelocity.Z = -ms;

    //(Strafe) left
    if(Keys[2])
    {
        desiredVelocity.X = -ms;
    }

    //(Strafe) right
    if(Keys[3])
    {
        desiredVelocity.X = ms;
    }

}


//! set the position of the physics node
void CCameraFPSPhysicsNode::setPosition(core::vector3df newPos)
{
    //move the scene node
    Node->setPosition(newPos);

    //move the body to the new position with a matrix4
    core::matrix4 irrMat;
	irrMat.setTranslation(newPos);
	irrMat.setRotationDegrees(getRotation());
    NewtonBodySetMatrix(Body, irrMat.pointer());

}

//! set the rotation of the physics node
void CCameraFPSPhysicsNode::setRotation(core::vector3df newRot)
{
    //rotate the scene node
    Node->setRotation(newRot);

    //rotate the body with a matrix4
    core::matrix4 irrMat;
    irrMat.setTranslation(getPosition());
	irrMat.setRotationDegrees(newRot);
    NewtonBodySetMatrix(Body, irrMat.pointer());
}


void CCameraFPSPhysicsNode::setMaterial(materialID material)
{
    //set the material of the newton body
    NewtonBodySetMaterialGroupID(Body, material);
}

void CCameraFPSPhysicsNode::addLocalForce(core::vector3df force, core::vector3df position)
{
    float com[3];
    NewtonBodyGetCentreOfMass(Body, com);
    core::vector3df r(com[0] - position.X,
                      com[1] - position.Y,
                      com[2] - position.Z);
    core::vector3df torque;
    torque = r.crossProduct(force);

    addForce(force);
    addTorque(torque);
}

void CCameraFPSPhysicsNode::capForceDo(f32 *force)
{
    if(force[0] > capForceAt)
        force[0] = capForceAt;
    if(force[1] > capForceAt)
        force[1] = capForceAt;
    if(force[2] > capForceAt)
        force[2] = capForceAt;
    if(force[0] < -capForceAt)
        force[0] = -capForceAt;
    if(force[1] < -capForceAt)
        force[1] = -capForceAt;
    if(force[2] < -capForceAt)
        force[2] = -capForceAt;
}
}//end namespace tumle

