varying vec2 texCoords;
varying vec3 vNormal;
varying vec3 vVertex;

uniform float silhouetteThreshold;
uniform sampler2D texture;

#define shininess 20.0

void main() {
	silhouetteThreshold = 0.2;
	//vec4 materialColor = gl_FrontMaterial.diffuse;

	//vec4 materialColor = vec4(1.0,1.0,1.0,1.0);
	//vec4 materialColor = gl_Color;
	vec4 materialColor = texture2D(texture,texCoords);

	vec4 silhouetteColor = vec4(0.0,0.0,0.0,1.0);

	vec4 specularColor = gl_FrontMaterial.specular;

	vec3 eyePos = normalize(-vVertex);
	vec3 lightPos = gl_LightSource[0].position.xyz;

	vec3 Normal = vNormal;
	vec3 EyeVert = normalize(eyePos - vVertex);
	vec3 LightVert = normalize(lightPos - vVertex);
	vec3 EyeLight = normalize(LightVert+EyeVert);

	float sil = max(dot(Normal,EyeVert), 0.0);

	if(sil < silhouetteThreshold)
		gl_FragColor = silhouetteColor;
	else
	{
		gl_FragColor = materialColor;

		float spec = pow(max(dot(Normal,EyeLight),0.0), shininess);

		if(spec < 0.2)
			gl_FragColor *= 0.95;
		else if(spec < 0.6)
			gl_FragColor *= 0.99;
		else
			gl_FragColor = materialColor+specularColor*.5;

		float diffuse = max(dot(Normal,LightVert),0.0);
		if( diffuse < 0.5)
			gl_FragColor *= 0.9;
	}
}
