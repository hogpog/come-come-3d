uniform sampler2D dim3Tex;
uniform bool dim3HasClosestLight;
uniform vec3 dim3ClosestLightNormal;
varying vec3 normal;
varying vec4 uv;


void main()
{
	float		intensity;
	vec4	pix;
	
	if (!dim3HasClosestLight) {
		gl_FragColor=vec4(0.0);
		return;
	}


	pix = texture2D(dim3Tex,uv.st);
	intensity = dot(dim3ClosestLightNormal,normal);


	if (intensity > 0.95)
		gl_FragColor = pix;
	else if (intensity > 0.5)
		gl_FragColor = pix * 0.6;
	else if (intensity > 0.25)
		gl_FragColor = pix * 0.4;
	else
		gl_FragColor = pix * 0.2;
}