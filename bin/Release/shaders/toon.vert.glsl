varying vec2 texCoords;
varying vec3 vNormal;
varying vec3 vVertex;

void main() {

	gl_Position = ftransform();
	vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);
	vNormal = normalize(gl_NormalMatrix * gl_Normal);
	texCoords = gl_MultiTexCoord0.st;
	
}