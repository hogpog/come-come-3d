//! \file tumle.h
//! \brief Main header for Tumle

// a part of Tumle

#ifndef TUMLE_TUMLE_H
#define TUMLE_TUMLE_H

#include "EBodyType.h"
#include "EPhysicsNodeType.h"
#include "TumleTypes.h"

#include <irrlicht.h>
#include <newton.h>

#include "IPhysicsNode.h"
#include "IControllablePhysicsNode.h"
#include "ICameraFPSPhysicsNode.h"
#include "IMaterialManager.h"
#include "IMaterialInteraction.h"
#include "IPhysicsManager.h"


/*! \mainpage Tumle - Irrlicht, Newton mixup
 *
 * <br>
 *
 * This is a collection of classes designed to make it easier to use
 * <a href = " http://www.newtondynamics.com/">Newton Game Dynamics</a>
 * with the
 * <a href = "http://irrlicht.sourceforge.net/">Irrlich Engine</a>. <br>
 * Thanks to everybody on the forums of the two sites for the kind help! <br>
 * This will add a static sphere and a cube falling on to the sphere. <br>
 * \code
 * //example on how to use Tumle
 *
 * #include "irrlicht.h"
 * #include "newton.h"
 * #include "tumle.h"
 *
 * //the irrlicht namespaces
 * using namespace irr;
 * using namespace core;
 * using namespace scene;
 * using namespace video;
 *
 * //the Tumle namespace
 * using namespace tumle;

 * int main()
 * {
 *    //the irrlicht stuff
 *    IrrlichtDevice *device = createDevice(EDT_OPENGL);
 *    ISceneManager *smgr = device->getSceneManager();
 *    IVideoDriver *driver = device->getVideoDriver();
 *
 *    //make the physics manager from the irrlicht device
 *    IPhysicsManager *pmgr = createPhysicsManager(device);
 *
 *    // make the irrlicht scene nodes
 *    ISceneNode *cube = smgr->addCubeSceneNode(10,0,-1,vector3df(0,40,0));
 *    ISceneNode *sphere = smgr->addSphereSceneNode(5);
 *
 *    // attach the nodes to physics nodes
 *    IPhysicsNode *cubeP = pmgr->addPhysicsNode(cube, EBT_BOX, 10, vector3df(10,10,10));
 *    IPhysicsNode *sphereP = pmgr->addPhysicsNode(sphere, EBT_SPHERE, 0, vector3df(5,5,5));
 *
 *    //add a camera
 *    ICameraSceneNode *cam = device->getSceneManager()->addCameraSceneNodeMaya();
 *
 *    //the loop
 *    while(device->run())
 *    {
 *      driver->beginScene(true, true, SColor(0,100,100,100));
 *          smgr->drawAll();
 *      driver->endScene();
 *
 *      //update the newton world
 *      pmgr->update();
 *    }
 *
 *    return 0;
 * }
 * \endcode
   \bug It seems to make a difference if the NewtonWorld is created before or after the irrlicht device.
   \bug It seems to make a difference what video driver is used in the irrlicht device.
 */

//! the namespace of Tumle
namespace tumle
{
    //! create the physics manager
    /*! \param irrDevice: the IrrlichtDevice to use */
    IPhysicsManager *createPhysicsManager(IrrlichtDevice *irrDevice);
}

#endif
