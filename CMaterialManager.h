// CMaterialManager.h
// a part of Tumle

#ifndef TUMLE_C_MATERIAL_MANAGER_H
#define TUMLE_C_MATERIAL_MANAGER_H


#include "tumle.h"

using namespace irr;

namespace tumle
{

//! managers the material
class CMaterialManager : public IMaterialManager
{
public:
    //! constructor
    CMaterialManager(IPhysicsManager *pmgr);

    //! destructor
    virtual ~CMaterialManager();

    //! delete the material manager
    /*! this will delete all the material interactions created with it */
    virtual void remove();

    //! add a material to the material manager
    /*! this creates a newton material. It is not recomended to use this function on the fly. Add all the materials
    you're planning on using in you app before the real action begins.*/
    virtual materialID addMaterial(const c8* name);

    //! define a material interaction
    /*! use this to setup the different types of coefficients for a collision between two materials. Add the materials to
    the material manager before setting up the interactions.
         \param material1: one of the materials to interact
         \param material2: the other material.
         \param elasticityCoef: the elasicity coefficient
         \param staticFrictionCoef: the static friction coefficient
         \param kineticFrictionCoef: the kinetic friction coefficent
         \param softnessCoef: the softness coefficient
         \param collidableState: will a collision take place
         \param createMaterialInteractionClass: If you want to change the setting on the fly setting this
         to true will make the fuction return a pointer to a material interaction class that lets you change
         the settings, if set to false 0 is returned.
         \return IMaterialIneraction pointer or 0 */
    virtual IMaterialInteraction* addMaterialInteraction(materialID material1,
                                           materialID material2,
                                           f32 elasticityCoef,
                                           f32 staticFrictionCoef,
                                           f32 kineticFrictionCoef,
                                           f32 softnessCoef,
                                           bool collidableState = true,
                                           bool createMaterialInteractionClass = false);


    //! get the physics manager that uses the manterial manager
    virtual IPhysicsManager* getPhysicsManager(){ return PhysicsManager; };

    //! set a pointer in the CreatedInteraction array to 0
    virtual void setPointerToZero(s32 index){ CreatedInteractions[index] = 0; };

    //!get the array of material interaction classes
    virtual core::array<IMaterialInteraction*> &getCreatedInteractions(){ return CreatedInteractions; };

    //! get a material interaction from it's name
    virtual IMaterialInteraction* getMaterialInteractionFromName(const c8 *name);

    //! get a material from it's name
    virtual materialID getMaterialFromName(const c8 *name);

private:
    //! the physics manager that uses this material manager
    IPhysicsManager *PhysicsManager;
    //! the newton world
    NewtonWorld *nWorld;
    //! array of created material interaction classes
    core::array<IMaterialInteraction*> CreatedInteractions;
    //! number of created material interaction classes
    u32 MaterialInteractionCount;
    //!struct to hold the material alongside with it's name
    struct Material
    {
        Material(materialID mat, const c8* name)
            {
                Id = mat;
                Name = name;
            }
        core::stringc Name;
        materialID Id;
    };
    //! array of created materials
    core::array<Material*> CreatedMaterials;
    //! number of created materials
    u32 MaterialCount;
};

}//end namespace tumle

#endif
