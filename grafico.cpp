#include "grafico.hpp"


CGrafico::CGrafico()
{
    contadorbolinhax=0;
    contadorbolinhay=0;
    velocidadePlayer = 50;
    VelocidadeFantasma = 40;
    itime = -1;
    vida = 3;

}
CGrafico::~CGrafico()
{
}
void CGrafico::Initialize()
{
    driverType = video::EDT_OPENGL;
    device = createDevice( driverType, core::dimension2d<s32>(800, 600),
                           32, false, false, false, &receiver);
    driver = device->getVideoDriver();
    smgr = device->getSceneManager();
    pmgr = createPhysicsManager(device);
    mmgr = pmgr->getMaterialManager();

    ///MotionBlur Setup
    MotionBLur = new IPostProcessMotionBlur(smgr->getRootSceneNode(), smgr, 666);
    MotionBLur->initiate(1024,512,0.8,smgr);
    ///Bloom Setup
    Bloom = new IPostProcessBloom(smgr->getRootSceneNode(), smgr, 666);
    PPE_Bloom_Setup setup;
    setup.sampleDist=0.4;
    setup.strength=10;
    setup.multiplier=4;
    Bloom->initiate(1024,512,setup,smgr);
    ///RadialBlur Setup
    RadialBlur = new IPostProcessRadialBlur(smgr->getRootSceneNode(), smgr, 666);
    RadialBlur->initiate(1024,512,0.5,1,smgr);
    ///Blur Setup
    Blur1 = new IPostProcessBlur(smgr->getRootSceneNode(), smgr, 666);
    Blur1->initiate(1024,512,0.01,smgr);
    Blur2 = new IPostProcessBlur(smgr->getRootSceneNode(), smgr, 666);
    Blur2->initiate(1024,512,0.01,smgr);
    /// teste toon shader
    video::IGPUProgrammingServices* gpu = driver->getGPUProgrammingServices();
    mtlToonShader = video::EMT_SOLID; // Fallback material type
    bCanDoGLSL_1_1 = true;
    if (gpu && (driverType == video::EDT_OPENGL))
    {

        if (!driver->queryFeature(video::EVDF_ARB_FRAGMENT_PROGRAM_1))
        {
            printf("queryFeature(video::EVDF_ARB_FRAGMENT_PROGRAM_1) failed\n");
            bCanDoGLSL_1_1 = false;
        }
        if (!driver->queryFeature(video::EVDF_ARB_VERTEX_PROGRAM_1))
        {
            printf("queryFeature(video::EVDF_ARB_VERTEX_PROGRAM_1) failed\n");
            bCanDoGLSL_1_1 = false;
        }
    }
    if (bCanDoGLSL_1_1)
    {
        // Shader uses built-in OpenGL GLSL uniforms, hence needs no explicit constants.
        // Therefore a callback function is not needed and is NULL.
        c8* vertex = "shaders/toon.vert.glsl";
        c8* frag = "shaders/toon.frag.glsl";
        mtlToonShader = gpu->addHighLevelShaderMaterialFromFiles(
                            vertex, "main", video::EVST_VS_1_1,
                            frag, "main", video::EPST_PS_1_1,
                            NULL, video::EMT_SOLID);
    }
    else
    {
        // This demo is for OpenGL!
        printf("This demo requires OpenGL with GLSL High-Level shaders\n");
        mtlToonShader = video::EMT_SOLID;
    }
    ///Define qual efeito vai usar
    Effect = 0;
    //Effect += BLUR;
    //Effect += RADIALBLUR;
    //Effect += BLOOM;
    //Effect += MOTIONBLUR;
    ///GUI********
    env = device->getGUIEnvironment();
    skin = env->getSkin();
    font = env->getFont("../../media/fontlucida.png");
    if (font)
        skin->setFont(font);

    skin->setFont(env->getBuiltInFont(), EGDF_TOOLTIP);
    fundopts = env->addImage(driver->getTexture("../../media/fundopts.png"),
                             position2d<int>(650,5));
    vidaimg[2] = env->addImage(driver->getTexture("../../media/vida.png"),
                               position2d<int>(64,550));
    vidaimg[1] = env->addImage(driver->getTexture("../../media/vida.png"),
                               position2d<int>(32,550));
    vidaimg[0] = env->addImage(driver->getTexture("../../media/vida.png"),
                               position2d<int>(0,550));
    pontosText = env->addStaticText(L"", rect<s32>(675,10,800,100), false);
    ///***********

}
void CGrafico::ProcessEffects()
{
    driver->beginScene(true, true, video::SColor(255,113,113,133));
    if (receiver.ESC)
    {
        vida=0;
    }
    if (vida<=0)
    {
        morreuImagem = env->addImage(driver->getTexture("../../media/morreu.bmp"),
                                     position2d<int>(0,0));
    }
    if (!receiver.endGame||vida>0)
    {
        core::stringw tmp("Pontos: ");
        tmp.append(core::stringw(ponto));
        pontosText->setText(tmp.c_str());


        if (bCanDoGLSL_1_1==true)
        {
            if (Effect==BLOOM)
            {
                driver->setRenderTarget(Bloom->rt0, true, true, video::SColor(0,0,0,0));
                smgr->drawAll();
                driver->setRenderTarget(0);
                Bloom->render();
            }
            else if (Effect==MOTIONBLUR)
            {
                MotionBLur->render();
                driver->setRenderTarget(0);
                MotionBLur->renderFinal();
            }
            else if (Effect==MOTIONBLUR+BLOOM)
            {
                MotionBLur->render();
                driver->setRenderTarget(Bloom->rt0, true, true, video::SColor(0,0,0,0));
                smgr->drawAll();
                driver->setRenderTarget(0);
                Bloom->render();
                MotionBLur->renderFinal();

            }
            else if (Effect==RADIALBLUR)
            {
                driver->setRenderTarget(RadialBlur->rt0, true, true, video::SColor(0,0,0,0));
                smgr->drawAll();
                driver->setRenderTarget(0);
                RadialBlur->render();
            }
            else if (Effect==RADIALBLUR+BLOOM)
            {
                driver->setRenderTarget(Bloom->rt0, true, true, video::SColor(0,0,0,0));
                smgr->drawAll();
                driver->setRenderTarget(RadialBlur->rt0, true, true, video::SColor(0,0,0,0));
                Bloom->render();
                driver->setRenderTarget(0);
                RadialBlur->render();
            }
            else if (Effect == BLUR)
            {
                driver->setRenderTarget(Blur1->rt0, true, true, video::SColor(0,0,0,0));
                smgr->drawAll();
                driver->setRenderTarget(Blur2->rt0, true, true, video::SColor(0,0,0,0));
                Blur1->render();
                driver->setRenderTarget(0);
                Blur2->render();
            }
            else
            {
                smgr->drawAll();
            }
        }
        else
        {
            smgr->drawAll();
        }
        if (tempoPowerUP>=0)
        {
            tempoPowerUP-=(1000/driver->getFPS());
            if (POWERUP==VELOCIDADE)
            {
                Effect = MOTIONBLUR;
                velocidadePlayer = 80;
                VelocidadeFantasma = 20;
                for (int x = 0; x < fantasmas.size();x++)
                {
                    fantasmas[x]->setMaterialTexture(0, colorMap[1+x]);
                }
            }
            else if (POWERUP == CONGELA)
            {
                velocidadePlayer = 50;
                Effect = RADIALBLUR;
                VelocidadeFantasma = 0;
                for (int x = 0; x < fantasmas.size();x++)
                {
                    fantasmas[x]->setMaterialTexture(0, colorMap[5]);
                }
            }
            else
            {
                VelocidadeFantasma = 35;
                velocidadePlayer = 50;
                Effect = 0;
                for (int x = 0; x < fantasmas.size();x++)
                {
                    fantasmas[x]->setMaterialTexture(0, colorMap[1+x]);
                }
                POWERUP=0;
            }
        }
        else
        {
            VelocidadeFantasma = 35;
            velocidadePlayer = 50;
            Effect = 0;
            for (int x = 0; x < fantasmas.size();x++)
            {
                fantasmas[x]->setMaterialTexture(0, colorMap[1+x]);
            }
            POWERUP=0;
        }
    }
    else
    {
        exit(0);
    }
    //pNode->setDesiredVelocity(core::vector3df(50,0,0));
    //pmgr->showCollision();
    env->drawAll();
    driver->endScene();
    if (vida<=0)
    {
        morreuImagem->remove();
    }
}
void CGrafico::LoadResources()
{
    Mesh.push_back(smgr->getMesh("../../media/mapa 04.obj"));
    Mesh.push_back(smgr->getMesh("../../media/fantasma.x"));


    colorMap.push_back(driver->getTexture("../../media/LayOut_Mapa.png"));
    colorMap.push_back(driver->getTexture("../../media/vermelho.bmp"));
    colorMap.push_back(driver->getTexture("../../media/rosa.bmp"));
    colorMap.push_back(driver->getTexture("../../media/verde.bmp"));
    colorMap.push_back(driver->getTexture("../../media/azul.bmp"));
    colorMap.push_back(driver->getTexture("../../media/branco.bmp"));


    normalMap.push_back(driver->getTexture("../../media/LayOut_MapaB.png"));
    normalMap.push_back(driver->getTexture("../../media/pacman_bump.bmp"));


    driver->makeNormalMapTexture(normalMap[0], 20.0f);
    driver->makeNormalMapTexture(normalMap[1], 20.0f);
    ///Luz 1

    scene::ILightSceneNode* nodea = 0;
    nodea = smgr->addLightSceneNode(0, core::vector3df(-800,800,-200),
                                    video::SColorf(1.0f, 1.0f, 1.0f, 0.0f), 1000.0f);
    nodea->getLightData().CastShadows=false;

    scene::ISceneNodeAnimator* anim =
        smgr->createFlyCircleAnimator (core::vector3df(200,100,200),190.0f, -0.003f);
    nodea->addAnimator(anim);
    anim->drop();
    ///Luz 2 com sombra*************************
    node = smgr->addLightSceneNode(0, core::vector3df(0,200,-50),
                                   video::SColorf(1.0f, 1.0f, 1.0f, 2555.0f), 400000.0f);



    // attach billboard to ligh
    scene::ISceneNode* node2;
    node2 = smgr->addBillboardSceneNode(node, core::dimension2d<f32>(50, 50));
    node2->setMaterialFlag(video::EMF_LIGHTING, false);
    node2->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);
    node2->setMaterialTexture(0, driver->getTexture("../../media/particlewhite.bmp"));
    node->getLightData().CastShadows=false;
    node->getLightData().AmbientColor=(video::SColor(0,255,255,255));
    node->getLightData().DiffuseColor=(video::SColor(0,255,255,255));
    node->getLightData().SpecularColor=(video::SColor(0,255,255,255));
    ///Carrega Player***********************/

    playerColorMap = driver->getTexture("../../media/pacman.bmp");
    playerNormalMap = driver->getTexture("../../media/pacman_bump.bmp");
    driver->makeNormalMapTexture(playerNormalMap, 50.0f);
    playerMesh = smgr->getMesh("../../media/pacman.x");
    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    scene::IMesh* tangentSphereMesh = manipulator->createMeshWithTangents(playerMesh->getMesh(0));
    playerNode = smgr->addAnimatedMeshSceneNode(playerMesh);

    playerNode->setFrameLoop(1, 79);
    playerNode->setAnimationSpeed(30);
    playerNode->setMaterialTexture(0, playerColorMap);
    playerNode->setMaterialTexture(1, playerNormalMap);
    playerNode->getMaterial(0).SpecularColor.set(128,128,128,128);
    playerNode->getMaterial(0).AmbientColor.set(128,128,128,128);
    playerNode->getMaterial(0).DiffuseColor.set(128,128,128,128);
    playerNode->setMaterialType(video::EMT_SOLID);

    playerNode->setMaterialType((video::E_MATERIAL_TYPE)mtlToonShader);
    playerNode->getMaterial(0).MaterialTypeParam = 0.035f; // adjust height for parallax effect

    playerPosX = 1;
    playerPosY = 1;

    pNode = pmgr->addControllablePhysicsNode(playerNode,
            EBT_SPHERE,
            1, vector3df(7,7,7));

    pNode->setPosition(core::vector3df(20,20,20));

    pNode->setCapForceMode(true);
    pNode->setContinuousCollisionMode(true);


    ///Fim carrega player

/// Sistema de Particula ///
    ps = smgr->addParticleSystemSceneNode(false);
    ps2 = smgr->addParticleSystemSceneNode(false);
    ps3 = smgr->addParticleSystemSceneNode(false);
    ps4 = smgr->addParticleSystemSceneNode(false);

    EmRing = ps->createRingEmitter(
                 pos, 10, 1,
                 core::vector3df(0.0f,0.03f,0.0f),
                 500,
                 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0);
    EmRing = ps2->createRingEmitter(
                 pos, 10, 1,
                 core::vector3df(0.0f,0.03f,0.0f),
                 500,
                 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0);
    EmRing = ps3->createRingEmitter(
                 pos, 10, 1,
                 core::vector3df(0.0f,0.03f,0.0f),
                 500,
                 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0);
    EmRing = ps4->createRingEmitter(
                 pos, 10, 1,
                 core::vector3df(0.0f,0.03f,0.0f),
                 500,
                 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0);
    EmBox = ps->createBoxEmitter(
                core::aabbox3d<f32>(-7,0,-7,7,1,7),
                core::vector3df(0.0f,0.0f,0.0f),
                80,100,
                video::SColor(0,255,255,255), video::SColor(0,255,255,255),
                200,400);
    EmBox = ps2->createBoxEmitter(
                core::aabbox3d<f32>(-7,0,-7,7,1,7),
                core::vector3df(0.0f,0.0f,0.0f),
                80,100,
                video::SColor(0,255,255,255), video::SColor(0,255,255,255),
                200,400);
    EmBox = ps3->createBoxEmitter(
                core::aabbox3d<f32>(-7,0,-7,7,1,7),
                core::vector3df(0.0f,0.0f,0.0f),
                80,100,
                video::SColor(0,255,255,255), video::SColor(0,255,255,255),
                200,400);
    EmBox = ps4->createBoxEmitter(
                core::aabbox3d<f32>(-7,0,-7,7,1,7),
                core::vector3df(0.0f,0.0f,0.0f),
                80,100,
                video::SColor(0,255,255,255), video::SColor(0,255,255,255),
                200,400);
    EmMesh = ps->createAnimatedMeshSceneNodeEmitter(
                 playerNode,true,
                 core::vector3df(0.0f,0.0f,0.0f),
                 100.0f, -1,
                 false,
                 100, 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0 );
    EmMesh = ps2->createAnimatedMeshSceneNodeEmitter(
                 playerNode,true,
                 core::vector3df(0.0f,0.0f,0.0f),
                 100.0f, -1,
                 false,
                 100, 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0 );
    EmMesh = ps3->createAnimatedMeshSceneNodeEmitter(
                 playerNode,true,
                 core::vector3df(0.0f,0.0f,0.0f),
                 100.0f, -1,
                 false,
                 100, 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0 );
    EmMesh = ps4->createAnimatedMeshSceneNodeEmitter(
                 playerNode,true,
                 core::vector3df(0.0f,0.0f,0.0f),
                 100.0f, -1,
                 false,
                 100, 1000,
                 video::SColor(0,255,255,255),
                 video::SColor(0,255,255,255),
                 200, 400,
                 0 );
    scene::IParticleAffector* paf =
//		ps->createAttractionAffector(core::vector3df(20,10,80),100,true,true,true,true);
        ps->createFadeOutParticleAffector();
    ps->addAffector(paf);
    paf->drop();

    ps->setMaterialFlag(video::EMF_LIGHTING, false);
    ps->setMaterialFlag(video::EMF_ZWRITE_ENABLE, false);
    ps->setMaterialTexture(0, driver->getTexture("../../media/ice.bmp"));
    ps->setMaterialType(video::EMT_TRANSPARENT_VERTEX_ALPHA);

    paf =
//		ps->createAttractionAffector(core::vector3df(20,10,80),100,true,true,true,true);
        ps2->createFadeOutParticleAffector();
    ps2->addAffector(paf);
    paf->drop();

    ps2->setMaterialFlag(video::EMF_LIGHTING, false);
    ps2->setMaterialFlag(video::EMF_ZWRITE_ENABLE, false);
    ps2->setMaterialTexture(0, driver->getTexture("../../media/ice.bmp"));
    ps2->setMaterialType(video::EMT_TRANSPARENT_VERTEX_ALPHA);

    paf =
//		ps->createAttractionAffector(core::vector3df(20,10,80),100,true,true,true,true);
        ps3->createFadeOutParticleAffector();
    ps3->addAffector(paf);
    paf->drop();

    ps3->setMaterialFlag(video::EMF_LIGHTING, false);
    ps3->setMaterialFlag(video::EMF_ZWRITE_ENABLE, false);
    ps3->setMaterialTexture(0, driver->getTexture("../../media/ice.bmp"));
    ps3->setMaterialType(video::EMT_TRANSPARENT_VERTEX_ALPHA);

    paf =
//		ps->createAttractionAffector(core::vector3df(20,10,80),100,true,true,true,true);
        ps4->createFadeOutParticleAffector();
    ps4->addAffector(paf);
    paf->drop();

    ps4->setMaterialFlag(video::EMF_LIGHTING, false);
    ps4->setMaterialFlag(video::EMF_ZWRITE_ENABLE, false);
    ps4->setMaterialTexture(0, driver->getTexture("../../media/ice.bmp"));
    ps4->setMaterialType(video::EMT_TRANSPARENT_VERTEX_ALPHA);
}

void CGrafico::CreatTileShadow(core::vector3df pos,
                               video::ITexture* colorMap,video::ITexture* normalMap,
                               scene::IAnimatedMesh* mesh)
{
    scene::ISceneNode* anode = 0;
    scene::ISceneNode* anode1 = 0;

    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    scene::IMesh* tangentSphereMesh =
        manipulator->createMeshWithTangents(mesh->getMesh(0));

    anode = (scene::IAnimatedMeshSceneNode*)smgr->addMeshSceneNode(tangentSphereMesh);
    anode1 = smgr->addMeshSceneNode(mesh->getMesh(0));

    IPhysicsNode *physLevelNode = pmgr->addPhysicsLevelNode(anode1, mesh->getMesh(0));
    physLevelNode->setPosition(core::vector3df(-8,0,-8));
    smgr->addToDeletionQueue(anode1);

    anode->setMaterialTexture(0, colorMap);
    anode->setMaterialTexture(1, normalMap);
    anode->getMaterial(0).AmbientColor.set(128,128,128,128);
    anode->getMaterial(0).DiffuseColor.set(128,128,128,128);
    anode->getMaterial(0).SpecularColor.set(0,0,0,0);

    anode->setMaterialType(video::EMT_PARALLAX_MAP_SOLID);
    anode->getMaterial(0).MaterialTypeParam = 0.005f; // adjust height for parallax effect

    anode->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, true);
    anode->setPosition(pos);

    anode->setAutomaticCulling(EAC_FRUSTUM_BOX);
}

void CGrafico::CreatTileBolinha(core::vector3df pos)
{
    bolinha[contadorbolinhax][contadorbolinhay] = 0;
    bolinha[contadorbolinhax][contadorbolinhay]=smgr->addSphereSceneNode(2.0f, 4, 0, -1,
            core::vector3df(0,0,0),
            core::vector3df(0,0,0),
            core::vector3df(1.0f, 1.0f, 1.0f));
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).SpecularColor.set(255,255,255,255);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).AmbientColor.set(0,0,0,255);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).DiffuseColor.set(255,255,255,255);
    bolinha[contadorbolinhax][contadorbolinhay]->setMaterialFlag(video::EMF_LIGHTING, true);
    bolinha[contadorbolinhax][contadorbolinhay]->setPosition(pos);
    bolinha[contadorbolinhax][contadorbolinhay]->setAutomaticCulling(EAC_FRUSTUM_BOX);
}

void CGrafico::PowerUpVelocidade(core::vector3df pos)
{
    video::ITexture* velocidadeColorMap;
    IAnimatedMesh *velocidadeMesh = smgr->getMesh("../../media/cubo.obj");
    velocidadeColorMap = driver->getTexture("../../media/velocidade.bmp");


    bolinha[contadorbolinhax][contadorbolinhay] = 0;
    bolinha[contadorbolinhax][contadorbolinhay]= smgr->addAnimatedMeshSceneNode(velocidadeMesh);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).SpecularColor.set(128,128,128,128);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).AmbientColor.set(128,128,128,128);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).DiffuseColor.set(128,128,128,128);

    bolinha[contadorbolinhax][contadorbolinhay]->setMaterialTexture(0, velocidadeColorMap);

    bolinha[contadorbolinhax][contadorbolinhay]->setMaterialType(video::EMT_SOLID);

    bolinha[contadorbolinhax][contadorbolinhay]->setMaterialFlag(video::EMF_LIGHTING, true);
    bolinha[contadorbolinhax][contadorbolinhay]->setPosition(pos);
    bolinha[contadorbolinhax][contadorbolinhay]->setAutomaticCulling(EAC_FRUSTUM_BOX);
}

void CGrafico::PowerUpCongela(core::vector3df pos)
{
    video::ITexture* velocidadeColorMap;
    IAnimatedMesh *velocidadeMesh = smgr->getMesh("../../media/cubo.obj");
    velocidadeColorMap = driver->getTexture("../../media/congela.png");


    bolinha[contadorbolinhax][contadorbolinhay] = 0;
    bolinha[contadorbolinhax][contadorbolinhay]= smgr->addAnimatedMeshSceneNode(velocidadeMesh);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).SpecularColor.set(128,128,128,128);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).AmbientColor.set(128,128,128,128);
    bolinha[contadorbolinhax][contadorbolinhay]->getMaterial(0).DiffuseColor.set(128,128,128,128);

    bolinha[contadorbolinhax][contadorbolinhay]->setMaterialTexture(0, velocidadeColorMap);

    bolinha[contadorbolinhax][contadorbolinhay]->setMaterialType(video::EMT_SOLID);

    bolinha[contadorbolinhax][contadorbolinhay]->setMaterialFlag(video::EMF_LIGHTING, true);
    bolinha[contadorbolinhax][contadorbolinhay]->setPosition(pos);
    bolinha[contadorbolinhax][contadorbolinhay]->setAutomaticCulling(EAC_FRUSTUM_BOX);
}

void CGrafico::LoadNpc(core::vector3df posfantasma,
                       video::ITexture* colorMap,video::ITexture* normalMap,
                       scene::IAnimatedMesh* mesh)
{
    fantasmas.push_back(0);

    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    manipulator->createMeshWithTangents(mesh->getMesh(0));

    manipulator->setVertexColorAlpha((IMesh*)mesh, 30);

    fantasmas[fantasmas.size()-1] = smgr->addAnimatedMeshSceneNode(mesh);

    fantasmas[fantasmas.size()-1]->setMaterialTexture(0, colorMap);
    fantasmas[fantasmas.size()-1]->setMaterialTexture(1, normalMap);

    fantasmas[fantasmas.size()-1]->setMaterialType(video::EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA);
    fantasmas[fantasmas.size()-1]->getMaterial(0).MaterialTypeParam = 0.015f; // adjust height for parallax effect

    posfantasma.X*=20;
    posfantasma.Z*=20;

    fantasmas[fantasmas.size()-1]->setPosition(posfantasma);
    fantasmas[fantasmas.size()-1]->setAutomaticCulling(EAC_FRUSTUM_BOX);
    fantasmas[fantasmas.size()-1]->setMaterialFlag(video::EMF_LIGHTING, true);
}
void CGrafico::CreateMap(int map[MAXX][MAXY])
{
    core::vector3df pos;
    pos.X = -8;
    pos.Y = 0;
    pos.Z = -8;
    for (int x = 0; x<MAXX;x++)
    {
        for (int y = 0; y<MAXY;y++)
        {
            mapa[x][y] = map[x][y];
        }
    }

    CreatTileShadow(pos,colorMap[0],normalMap[0],Mesh[0]);

    for (int npcs = 0; npcs<npcsx.size();npcs++)
    {
        LoadNpc(iniciofantasma,colorMap[1+npcs],normalMap[1],Mesh[1]);
        pos.Y = 0;

    }
}
void CGrafico::CreatebMap(int map[MAXX][MAXY])
{
    vector3df posbolinha;
    for (int x = 0; x<MAXX;x++)
    {
        for (int y = 0; y<MAXY;y++)
        {
            contadorbolinhax=x;
            contadorbolinhay=y;
            bmapa[x][y] = map[x][y];
            if (map[x][y] == 1)
            {
                posbolinha.X = x*20;
                posbolinha.Y = 10;
                posbolinha.Z = y*20;
                CreatTileBolinha(posbolinha);
            }
            else
                if (map[x][y] == 2)
                {
                    posbolinha.X = x*20;
                    posbolinha.Y = 10;
                    posbolinha.Z = y*20;
                    PowerUpVelocidade(posbolinha);
                }
                else
                    if (map[x][y] == 3)
                    {
                        posbolinha.X = x*20;
                        posbolinha.Y = 10;
                        posbolinha.Z = y*20;
                        PowerUpCongela(posbolinha);
                    }
                    else
                    {
                        posbolinha.Y = 500;
                        CreatTileBolinha(posbolinha);
                        smgr->addToDeletionQueue(bolinha[x][y]);
                    }
        }
    }
}
void CGrafico::addNpc(int *posx,int *posy)
{
    npcsx.push_back(posx);
    npcsy.push_back(posy);
    playerPos.push_back(core::vector3df(*posx*20,10,*posy*20));
    initnpcX=*posx;
    initnpcY=*posy;
}

bool CGrafico::UpdatePos()
{

    float timet;
    timet = clock() - itime;
    itime  = clock();

    float fps = 1000/timet;
    bool temp = false;
    pmgr->update();

    int BlockDir;
    playerDir = DirToMove;

    if (fps>1)
    {
        ///MORRE!///
        ///uando morre volta os npcs na posicao inicial e o player tbm.///
        for (int aaaa = 0; aaaa < fantasmas.size(); aaaa++)
        {
            if (playerNode->getPosition().X > fantasmas[aaaa]->getPosition().X - 10 &&
                    playerNode->getPosition().X < fantasmas[aaaa]->getPosition().X + 10 &&
                    playerNode->getPosition().Z > fantasmas[aaaa]->getPosition().Z - 10 &&
                    playerNode->getPosition().Z < fantasmas[aaaa]->getPosition().Z + 10)
            {
                pNode->setPosition(core::vector3df(20,10,20));
                vida--;
                if (vida>0)
                    vidaimg[vida]->remove();
                POWERUP=0;
                for (int aaaaa = 0; aaaaa < fantasmas.size(); aaaaa++)
                {
                    *npcsx[aaaaa] = initnpcY;
                    *npcsy[aaaaa] = initnpcX;
                    playerPos[aaaaa].X = initnpcY*20;
                    playerPos[aaaaa].Z = initnpcX*20;
                }
            }
        }
        if (playerDir==0)
        {
            pNode->setVelocity(core::vector3df(0,0,velocidadePlayer));

            playerNode->setRotation( irr::core::vector3df(0, 270, 0) );
        }
        else
            if (playerDir==1)
            {
                pNode->setVelocity(core::vector3df(velocidadePlayer,0,0));

                playerNode->setRotation( irr::core::vector3df(0, 0, 0) );
            }
            else
                if (playerDir==2)
                {
                    pNode->setVelocity(core::vector3df(0,0,-velocidadePlayer));

                    playerNode->setRotation( irr::core::vector3df(0, 90, 0) );
                }
                else
                    if (playerDir==3)
                    {
                        pNode->setVelocity(core::vector3df(-velocidadePlayer,0,0));

                        playerNode->setRotation( irr::core::vector3df(0, 180, 0) );
                    }
        pos = pNode->getPosition();

        playerPosX=(pos.X+5)/20;
        playerPosY=(pos.Z+5)/20;


        if (bmapa[playerPosX][playerPosY]==1)
        {
            ponto++;
            bolinhas++;
            bmapa[playerPosX][playerPosY]=0;
            smgr->addToDeletionQueue(bolinha[playerPosX][playerPosY]);
        }
        else
            if (bmapa[playerPosX][playerPosY]==2)
            {
                ponto+=10;
                bolinhas++;
                bmapa[playerPosX][playerPosY]=0;
                smgr->addToDeletionQueue(bolinha[playerPosX][playerPosY]);

                tempoPowerUP = 10000;
                POWERUP = VELOCIDADE;
            }
            else
                if (bmapa[playerPosX][playerPosY]==3)
                {
                    ponto+10;
                    bolinhas++;
                    bmapa[playerPosX][playerPosY]=0;
                    smgr->addToDeletionQueue(bolinha[playerPosX][playerPosY]);

                    tempoPowerUP = 10000;
                    POWERUP = CONGELA;
                }
        camera->Update();
        node->setPosition(core::vector3df(playerNode->getPosition().X-50,100,playerNode->getPosition().Z-50));
        temp = true;
        for (int npcs = 0; npcs<npcsx.size();npcs++)
        {
            core::vector3df pos;
            pos.X = *npcsx[npcs]*20;;
            pos.Y = 0;
            pos.Z = *npcsy[npcs]*20;;


            if (pos.X>=playerPos[npcs].X&&(pos.X-playerPos[npcs].X>VelocidadeFantasma/fps))
            {
                temp=false;
                fantasmas[npcs]->setRotation( irr::core::vector3df(0, 270, 0) );
                playerPos[npcs].X+=(float)VelocidadeFantasma/fps;
            }
            else if (pos.X<playerPos[npcs].X)
            {
                temp=false;
                fantasmas[npcs]->setRotation( irr::core::vector3df(0, 90, 0) );
                playerPos[npcs].X-=(float)VelocidadeFantasma/fps;
            }
            if (pos.Z>=playerPos[npcs].Z&&(pos.Z-playerPos[npcs].Z>VelocidadeFantasma/fps))
            {
                temp=false;
                fantasmas[npcs]->setRotation( irr::core::vector3df(0, 180, 0) );
                playerPos[npcs].Z+=(float)VelocidadeFantasma/fps;
            }
            else if (pos.Z<playerPos[npcs].Z)
            {
                temp=false;
                fantasmas[npcs]->setRotation( irr::core::vector3df(0, 0, 0) );
                playerPos[npcs].Z-=(float)VelocidadeFantasma/fps;
            }
            fantasmas[npcs]->setPosition(playerPos[npcs]);
        }
        ///particle system///
        if (POWERUP==VELOCIDADE||POWERUP==CONGELA)
        {
            ps->setScale(core::vector3df(1,1,1));
            ps->setParticleSize(core::dimension2d<f32>(10.0f, 10.0f));

            ps2->setScale(core::vector3df(1,1,1));
            ps2->setParticleSize(core::dimension2d<f32>(10.0f, 10.0f));

            ps3->setScale(core::vector3df(1,1,1));
            ps3->setParticleSize(core::dimension2d<f32>(10.0f, 10.0f));

            ps4->setScale(core::vector3df(1,1,1));
            ps4->setParticleSize(core::dimension2d<f32>(10.0f, 10.0f));

            if (POWERUP==CONGELA)
            {
                EmRing->setMinParticlesPerSecond(500);
                EmRing->setMaxParticlesPerSecond(1000);
                ps->setEmitter(EmRing);
                ps->setPosition(fantasmas[0]->getPosition());
                ps2->setEmitter(EmRing);
                ps2->setPosition(fantasmas[1]->getPosition());
                ps3->setEmitter(EmRing);
                ps3->setPosition(fantasmas[2]->getPosition());
                ps4->setEmitter(EmRing);
                ps4->setPosition(fantasmas[3]->getPosition());
            }
            if (POWERUP==VELOCIDADE)
            {
                EmBox->setMinParticlesPerSecond(50);
                EmBox->setMaxParticlesPerSecond(100);
                ps->setEmitter(EmBox);
                ps2->setEmitter(EmBox);
                ps3->setEmitter(EmBox);
                ps4->setEmitter(EmBox);
                ps->setPosition(pos);
                ps2->setPosition(pos);
                ps3->setPosition(pos);
                ps4->setPosition(pos);
            }
        }
        else
        {
            EmRing->setMinParticlesPerSecond(0);
            EmRing->setMaxParticlesPerSecond(0);
            EmBox->setMinParticlesPerSecond(0);
            EmBox->setMaxParticlesPerSecond(0);
        }
    }
    return temp;
}
void CGrafico::Run()
{

    camera = new followingCamera(playerNode, smgr);
    smgr->setActiveCamera(camera->getCam());
    camera->Update();
//    scene::ICameraSceneNode * cam = smgr->addCameraSceneNodeFPS(0, 100.0f, 100.0f);
    receiver.getPlayerNode(playerNode);
    receiver.getPlayerDir(&DirToMove);
    device->getCursorControl()->setVisible(false);
}
