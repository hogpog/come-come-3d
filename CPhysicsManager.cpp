// CPhysicsManager.cpp
// a part of Tumle

#include "CPhysicsManager.h"
#include "CMaterialManager.h"
#include "CPhysicsNode.h"
#include "CControllablePhysicsNode.h"
#include "CCameraFPSPhysicsNode.h"

namespace tumle
{

//! constructor
CPhysicsManager::CPhysicsManager(IrrlichtDevice *irrDevice, NewtonWorld *newtonWorld):
IPhysicsManager(), device(irrDevice), nWorld(newtonWorld),  NewtonGravity(0,-98,0), PhysicsNodeCount(0),
dt(10), CurrentTime(0), TimeAccumulator(0)
{
    MaterialManager = new CMaterialManager(this);

    //reset the world size
    WorldMin =core::vector3df(0,0,0);
    WorldMin =core::vector3df(0,0,0);

    //reset the leave world event
    LeaveWorldEvent = 0;

    //set the default leave world event
    NewtonSetBodyLeaveWorldEvent(nWorld, leaveNewtonWorldEvent);
}

//! destructor
CPhysicsManager::~CPhysicsManager()
{
    // delete all the created physics nodes
    for(u32 i = 0; i < CreatedPhysicsNodes.size() ; i++)
    {
        if(CreatedPhysicsNodes[i])
        {
            CreatedPhysicsNodes[i]->remove();
        }//if
    }//for

    //delete the material manager
    if(MaterialManager)
        MaterialManager->remove();

    //delete the newton world
    if(nWorld)
        NewtonDestroy(nWorld);

}

void CPhysicsManager::remove()
{
    //no physics manager is active
    tumle::activePhysicsManager = 0;
    delete this;
}


//! update the newton world
void CPhysicsManager::update()
{

    //get the time from the device
    f32 newTime = device->getTimer()->getTime();
    //calculate the time step
    f32 deltaTime = newTime - CurrentTime;
    //update CurrentTime
    CurrentTime = newTime;
    //add the time step to the time accumulator
    TimeAccumulator += deltaTime;
    //update when the TimeAccumulator is big enough
    while(TimeAccumulator >= dt)
    {
        NewtonUpdate(nWorld, dt/1000); //convert to seconds
        TimeAccumulator -= dt;
    }


}

IPhysicsNode *CPhysicsManager::getPhysicsNodeFromName(const c8 *name)
{
    IPhysicsNode *pNode = 0;
	for (s32 i = 0; i < PhysicsNodeCount ; i++)
	{
        if (!(strcmp(CreatedPhysicsNodes[i]->getName(),name)))
            pNode = CreatedPhysicsNodes[i];
	}

	return pNode;
}

//! add a physics node using a irrlicht node
IPhysicsNode* CPhysicsManager::addPhysicsNode(scene::ISceneNode *node,
                                E_BODY_TYPE type,
                                f32 mass,
                                core::vector2df size,
                                core::vector3df rotation)
{
    //check if a node is passed
    if(!node)
    {
        printf("TUMLE: no node passed\n");
        return 0;
    }

    //check for wrong body type
    if( (type < EBT_CONE) || (type > EBT_CHAMFER_CYLINDER) )
    {
        printf("TUMLE: dimensions and body type does not match!\n");
        return 0;
    }

    // find the center of the bounding box of the irrlicht node
    core::matrix4 centerMatrix;
    centerMatrix.setTranslation(node->getBoundingBox().getCenter());

    centerMatrix.setRotationDegrees(core::vector3df(node->getRotation().X+rotation.X,
                                                    node->getRotation().Y+rotation.Y,
                                                    node->getRotation().Z+rotation.Z));


    //make a newton collision
    NewtonCollision *collision = makeNewtonCollision(type, size, centerMatrix.pointer() );

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());

     //make the physics node
    IPhysicsNode* pNode = new CPhysicsNode(this, PhysicsNodeCount++, node, body, type, mass);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    NewtonReleaseCollision(nWorld, collision);

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);

    return pNode;
}


//! add a physics node using a irrlicht node
IPhysicsNode* CPhysicsManager::addPhysicsNode(scene::ISceneNode *node,
                                E_BODY_TYPE type,
                                f32 mass,
                                core::vector3df size)
{
    //check if a node is passed
    if(!node)
    {
        printf("TUMLE: no node passed\n");
        return 0;
    }

    //check for wrong body type
    if(type > EBT_SPHERE)
    {
        printf("TUMLE: dimensions and body type does not match!\n");
        return 0;
    }

    // find the center of the bounding box of the irrlicht node
    core::matrix4 centerMatrix;
    centerMatrix.setTranslation(node->getBoundingBox().getCenter());

    //make a newton collision
    NewtonCollision *collision = makeNewtonCollision(type, size, centerMatrix.pointer() );

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());

    //make the physics node
    IPhysicsNode* pNode = new CPhysicsNode(this, PhysicsNodeCount++, node, body, type, mass);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    NewtonReleaseCollision(nWorld, collision);

    CreatedPhysicsNodes.push_back(pNode);
    return pNode;
}

//! add a physics node using a irrlicht node
IPhysicsNode* CPhysicsManager::addPhysicsNode(scene::ISceneNode *node,
                                              E_BODY_TYPE type,
                                              f32 mass,
                                              scene::IMesh *mesh)
{
    //check if a node is passed
    if(!node)
    {
        printf("TUMLE: no node passed\n");
        return 0;
    }


    NewtonCollision *collision = 0;
    //make a newton collision
    switch(type)
    {
        case EBT_BOUNDING_BOX:
        {
             collision = makeCollisionFromBoundingBox(nWorld, node->getBoundingBox(), node->getScale() );
             break;
        }
        case EBT_MESH:
        {
             /* make sure that the node has a mesh and that the right mesh
             is used to make the collision */
             switch(node->getType())
             {
                 case scene::ESNT_MESH:
                 {
                     scene::IMeshSceneNode *msn = (scene::IMeshSceneNode*)node;
                     collision = makeCollisionFromMesh(nWorld, msn->getMesh());
                     break;
                 }
                 case scene::ESNT_ANIMATED_MESH:
                 {
                     scene::IAnimatedMeshSceneNode *amsn = (scene::IAnimatedMeshSceneNode*)node;
                     collision = makeCollisionFromMesh(nWorld, amsn->getMesh()->getMesh(0));
                     break;
                 }
                 default:
                 {
                     printf("TUMLE: passed node has no mesh\n");
                     return 0;
                 }
             }//switch node->getType
             break;
        }// case EBT_MESH

        case EBT_EXTERN_MESH:
        {
            if(mesh)
                collision = makeCollisionFromMesh(nWorld, mesh);
            else if(!mesh)
                printf("TUMLE: no valid mesh passed\n");
            break;
        }

        default:
        {
            printf("TUMLE: dimensions and body type does not match!\n");
            return 0;
        }
    }//switch

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());

    //make the physics node
    IPhysicsNode* pNode = new CPhysicsNode(this, PhysicsNodeCount++, node, body, type, mass);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    NewtonReleaseCollision(nWorld, collision);

    CreatedPhysicsNodes.push_back(pNode);
    return pNode;
}

IControllablePhysicsNode* CPhysicsManager::addControllablePhysicsNode(scene::ISceneNode *node,
                                                                      E_BODY_TYPE type,
                                                                      f32 mass,
                                                                      core::vector3df size,
                                                                      SKeyMap *keyMapArray,
                                                                      u32 keyMapSize,
                                                                      f32 moveSpeed,
                                                                      f32 rotateSpeed,
                                                                      bool fixedUpVector,
                                                                      bool strafe)
{
    //check if a node is passed
    if(!node)
    {
        printf("TUMLE: no node passed\n");
        return 0;
    }

    //check for wrong body type
    if(type > EBT_SPHERE)
    {
        printf("TUMLE: dimensions and body type does not match!\n");
        return 0;
    }
    // find the center of the bounding box of the irrlicht node
    core::matrix4 centerMatrix;
    centerMatrix.setTranslation(node->getBoundingBox().getCenter());

    //make a newton collision
    NewtonCollision *collision = makeNewtonCollision(type, size, centerMatrix.pointer() );

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());

     //make the physics node
    IControllablePhysicsNode* pNode = new CControllablePhysicsNode(this, PhysicsNodeCount++, node, body, type, mass,
                                                                   keyMapArray, keyMapSize, strafe,
                                                                   fixedUpVector, moveSpeed, rotateSpeed);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    //don't ever let the body freeze
    NewtonBodySetAutoFreeze(body, 0);

    NewtonReleaseCollision(nWorld, collision);

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);
    //let the node know where it is in the array

    return pNode;
}

IControllablePhysicsNode* CPhysicsManager::addControllablePhysicsNode(scene::ISceneNode *node,
                                                                      E_BODY_TYPE type,
                                                                      f32 mass,
                                                                      core::vector2df size,
                                                                      core::vector3df rotation,
                                                                      SKeyMap *keyMapArray,
                                                                      u32 keyMapSize,
                                                                      f32 moveSpeed,
                                                                      f32 rotateSpeed,
                                                                      bool fixedUpVector,
                                                                      bool strafe)
{
    //check if a node is passed
    if(!node)
    {
        printf("TUMLE: no node passed\n");
        return 0;
    }

    //check for wrong body type
    if(type < EBT_CONE || type > EBT_CHAMFER_CYLINDER)
    {
        printf("TUMLE: dimensions and body type does not match!\n");
        return 0;
    }
    // find the center of the bounding box of the irrlicht node
    core::matrix4 centerMatrix;
    centerMatrix.setTranslation(node->getBoundingBox().getCenter());

    //because the types from EBT_CONE through EBT_CHAMFER_CYLINDER is on the side
    //90 degrees is added to matrix's Z rotation
    centerMatrix.setRotationDegrees(core::vector3df(node->getRotation().X+rotation.X,
                                                    node->getRotation().Y+rotation.Y,
                                                    node->getRotation().Z+rotation.Z));

    //make a newton collision
    NewtonCollision *collision = makeNewtonCollision(type, size, centerMatrix.pointer() );

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());


    //make the physics node
    IControllablePhysicsNode* pNode = new CControllablePhysicsNode(this, PhysicsNodeCount++, node, body, type, mass,
                                                                   keyMapArray, keyMapSize, strafe,
                                                                   fixedUpVector, moveSpeed, rotateSpeed);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    //don't ever let the body freeze
    NewtonBodySetAutoFreeze(body, 0);

    NewtonReleaseCollision(nWorld, collision);

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);
    //let the node know where it is in the array

    return pNode;
}

ICameraFPSPhysicsNode* CPhysicsManager::addCameraPhysicsNodeFPS(E_BODY_TYPE type,
                                                                f32 mass,
                                                                core::vector3df size,
                                                                SKeyMap *keyMapArray,
                                                                u32 keyMapSize,
                                                                f32 moveSpeed,
                                                                f32 rotateSpeed,
                                                                bool fixUpVector)
{

    //check for wrong body type
    if(type > EBT_SPHERE)
    {
        printf("TUMLE: dimensions and body type does not match!\n");
        return 0;
    }
    //make the irrlicht FPS camera scene node
    scene::ICameraSceneNode *node = device->getSceneManager()->addCameraSceneNodeFPS(0, rotateSpeed, 0);

    // find the center of the bounding box of the irrlicht node
    core::matrix4 centerMatrix;
    centerMatrix.setTranslation(node->getBoundingBox().getCenter());

    //make a newton collision
    NewtonCollision *collision = makeNewtonCollision(type, size, centerMatrix.pointer() );

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());

     //make the physics node
    ICameraFPSPhysicsNode* pNode = new CCameraFPSPhysicsNode(this, PhysicsNodeCount++, node, body, type, mass,
                                                             keyMapArray, keyMapSize, moveSpeed, rotateSpeed);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    //don't ever let the body freeze
    NewtonBodySetAutoFreeze(body, 0);

    NewtonReleaseCollision(nWorld, collision);

    //fix the up vector
    if(fixUpVector)
    {
        f32 up[3] = {0,1,0};
        NewtonConstraintCreateUpVector(nWorld, up, body);

        //keep the newton collision from spinning by ficing a horizontal vector
        f32 hor[3] = {1,0,0};
        NewtonConstraintCreateUpVector(nWorld, hor, body);
    }

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);

    return pNode;
}

ICameraFPSPhysicsNode* CPhysicsManager::addCameraPhysicsNodeFPS(E_BODY_TYPE type,
                                                                f32 mass,
                                                                core::vector2df size,
                                                                core::vector3df rotation,
                                                                SKeyMap *keyMapArray,
                                                                u32 keyMapSize,
                                                                f32 moveSpeed,
                                                                f32 rotateSpeed,
                                                                bool fixUpVector)
{
    //check for wrong body type
    if(type < EBT_CONE || type > EBT_CHAMFER_CYLINDER)
    {
        printf("TUMLE: dimensions and body type does not match!\n");
        return 0;
    }

    //make the irrlicht FPS camera scene node
    scene::ICameraSceneNode *node = device->getSceneManager()->addCameraSceneNodeFPS(0, rotateSpeed, 0);

    // find the center of the bounding box of the irrlicht node
    core::matrix4 centerMatrix;
    centerMatrix.setTranslation(node->getBoundingBox().getCenter());

    //because the types from EBT_CONE through EBT_CHAMFER_CYLINDER is on the side
    //90 degrees is added to matrix's Z rotation
    centerMatrix.setRotationDegrees(core::vector3df(node->getRotation().X+rotation.X,
                                                    node->getRotation().Y+rotation.Y,
                                                    node->getRotation().Z+rotation.Z));

    //make a newton collision
    NewtonCollision *collision = makeNewtonCollision(type, size, centerMatrix.pointer() );

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());


    //make the physics node
    ICameraFPSPhysicsNode *pNode = new CCameraFPSPhysicsNode(this, PhysicsNodeCount++, node, body, type, mass,
                                                             keyMapArray, keyMapSize, moveSpeed, rotateSpeed);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    //don't ever let the body freeze
    NewtonBodySetAutoFreeze(body, 0);

    NewtonReleaseCollision(nWorld, collision);

    //fix the up vector
    if(fixUpVector)
    {
        f32 up[3] = {0,1,0};
        NewtonConstraintCreateUpVector(nWorld, up, body);

        //keep the newton collision from spinning by ficing a horizontal vector
        f32 hor[3] = {1,0,0};
        NewtonConstraintCreateUpVector(nWorld, hor, body);
    }

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);

    return pNode;
}

ICameraFPSPhysicsNode* CPhysicsManager::addCameraPhysicsNodeFPS(E_BODY_TYPE type,
                                                                f32 mass,
                                                                scene::IMesh *mesh,
                                                                SKeyMap *keyMapArray,
                                                                u32 keyMapSize,
                                                                f32 moveSpeed,
                                                                f32 rotateSpeed,
                                                                bool fixUpVector)
{
    //check for wrong body type
    if(type != EBT_EXTERN_MESH)
    {
        printf("TUMLE: a mesh is passed to the camera, use EBT_EXTERN_MESH!\n");
        return 0;
    }

    //make the irrlicht FPS camera scene node
    scene::ICameraSceneNode *node = device->getSceneManager()->addCameraSceneNodeFPS(0, rotateSpeed, 0);

    //make a newton collision from the passed mesh
    NewtonCollision *collision = makeCollisionFromMesh(nWorld, mesh);

    //make a newton body
    NewtonBody *body = makeNewtonBody(collision, mass,
                                      node->getPosition(),
                                      node->getRotation(),
                                      node->getBoundingBox().getCenter());


    //make the physics node
    ICameraFPSPhysicsNode *pNode = new CCameraFPSPhysicsNode(this, PhysicsNodeCount++, node, body, type, mass,
                                                             keyMapArray, keyMapSize, moveSpeed, rotateSpeed);

    //make the physics node the user data of the created body
    NewtonBodySetUserData(body, pNode);

    //don't ever let the body freeze
    NewtonBodySetAutoFreeze(body, 0);

    NewtonReleaseCollision(nWorld, collision);

    //fix the up vector
    if(fixUpVector)
    {
        f32 up[3] = {0,1,0};
        NewtonConstraintCreateUpVector(nWorld, up, body);

        //keep the newton collision from spinning by ficing a horizontal vector
        f32 hor[3] = {1,0,0};
        NewtonConstraintCreateUpVector(nWorld, hor, body);
    }

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);

    return pNode;
}


IPhysicsNode *CPhysicsManager::addPhysicsLevelNode(scene::ISceneNode *levelNode, scene::IMesh *mesh)
{

    scene::IMesh *levelMesh = mesh;

    if(!levelNode)
    {
        printf("TUMLE: No node passed, level will have no collision\n");
        return 0;
    }

    switch(levelNode->getType())
    {
        case scene::ESNT_MESH:
        {
            scene::IMeshSceneNode *msn = (scene::IMeshSceneNode*)levelNode;
            levelMesh = msn->getMesh();
            break;
        }
        case scene::ESNT_ANIMATED_MESH:
        {
            scene::IAnimatedMeshSceneNode *amsn = (scene::IAnimatedMeshSceneNode*)levelNode;
            levelMesh = amsn->getMesh()->getMesh(0);
            break;
        }
        default:
        {
            printf("TUMLE: passed node has no mesh, passed mesh used to create level collision\n");
        }
    }

    if(!levelMesh)
    {
        printf("TUMLE: no mesh pressent, level will have no collision\n");
        return 0;
    }


	NewtonCollision *collision = NewtonCreateTreeCollision(nWorld, NULL);
	NewtonTreeCollisionBeginBuild(collision);
	u32 cMeshBuffer, j;
	int v1i, v2i, v3i;
	scene::IMeshBuffer *mb;

	float vArray[9]; // vertex array (3*3 floats)

	for (cMeshBuffer=0; cMeshBuffer<levelMesh->getMeshBufferCount(); cMeshBuffer++)
	{
		mb = levelMesh->getMeshBuffer(cMeshBuffer);

		//video::S3DVertex2TCoords* mb_vertices = (irr::video::S3DVertex2TCoords*)mb->getVertices();
		video::S3DVertex* mb_vertices = (irr::video::S3DVertex*)mb->getVertices();

		u16* mb_indices  = mb->getIndices();

		// add each triangle from the mesh
		for (j=0; j<mb->getIndexCount(); j+=3)
		{
			v1i = mb_indices[j];
			v2i = mb_indices[j+1];
			v3i = mb_indices[j+2];

			vArray[0] = mb_vertices[v1i].Pos.X;
			vArray[1] = mb_vertices[v1i].Pos.Y;
			vArray[2] = mb_vertices[v1i].Pos.Z;
			vArray[3] = mb_vertices[v2i].Pos.X;
			vArray[4] = mb_vertices[v2i].Pos.Y;
			vArray[5] = mb_vertices[v2i].Pos.Z;
			vArray[6] = mb_vertices[v3i].Pos.X;
			vArray[7] = mb_vertices[v3i].Pos.Y;
			vArray[8] = mb_vertices[v3i].Pos.Z;

			NewtonTreeCollisionAddFace(collision, 3, (float*)vArray, 12, 1);
		}

	}
	NewtonTreeCollisionEndBuild(collision, 0);
    NewtonBody *body = makeNewtonBody(collision, 0,
                                      levelNode->getPosition(),
                                      levelNode->getRotation(),
                                      levelNode->getBoundingBox().getCenter());


	// set the newton world size based on the map size
	float boxP0[3];
	float boxP1[3];
	float matrix[4][4];
	NewtonBodyGetMatrix (body, &matrix[0][0]);
	NewtonCollisionCalculateAABB (collision, &matrix[0][0],  &boxP0[0], &boxP1[0]);
	// you can pad the box here if you wish
	boxP0[0] -= 1000000000.0f;
	boxP0[1] -= 1000000000.0f;
	boxP0[2] -= 1000000000.0f;
	boxP1[1] += 1000000000.0f;
	boxP1[0] += 1000000000.0f;
	boxP1[2] += 1000000000.0f;
	NewtonSetWorldSize (nWorld, (float*)boxP0, (float*)boxP1);
    printf("TUMLE: World resized to fit level\n");

    //release the collision
    NewtonReleaseCollision(nWorld, collision);

    //make the physics node
    IPhysicsNode *pNode = new CPhysicsNode(this, PhysicsNodeCount++, levelNode, body, EBT_LEVEL, 0);

    //set the user date of the body
    NewtonBodySetUserData(body, pNode);

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);

    return pNode;
}

IPhysicsNode *CPhysicsManager::addPhysicsLevelNode2T(scene::ISceneNode *levelNode, scene::IMesh *mesh)
{

    scene::IMesh *levelMesh = mesh;

    if(!levelNode)
    {
        printf("TUMLE: No node passed, level will have no collision\n");
        return 0;
    }

    switch(levelNode->getType())
    {
        case scene::ESNT_MESH:
        {
            scene::IMeshSceneNode *msn = (scene::IMeshSceneNode*)levelNode;
            levelMesh = msn->getMesh();
            break;
        }
        case scene::ESNT_ANIMATED_MESH:
        {
            scene::IAnimatedMeshSceneNode *amsn = (scene::IAnimatedMeshSceneNode*)levelNode;
            levelMesh = amsn->getMesh()->getMesh(0);
            break;
        }
        default:
        {
            printf("TUMLE: passed node has no mesh, passed mesh used to create level collision\n");
        }
    }

    if(!levelMesh)
    {
        printf("TUMLE: no mesh pressent, level will have no collision\n");
        return 0;
    }


	NewtonCollision *collision = NewtonCreateTreeCollision(nWorld, NULL);
	NewtonTreeCollisionBeginBuild(collision);
	u32 cMeshBuffer, j;
	int v1i, v2i, v3i;
	scene::IMeshBuffer *mb;

	float vArray[9]; // vertex array (3*3 floats)

	for (cMeshBuffer=0; cMeshBuffer<levelMesh->getMeshBufferCount(); cMeshBuffer++)
	{
		mb = levelMesh->getMeshBuffer(cMeshBuffer);

		video::S3DVertex2TCoords* mb_vertices = (irr::video::S3DVertex2TCoords*)mb->getVertices();
		//video::S3DVertex* mb_vertices = (irr::video::S3DVertex*)mb->getVertices();

		u16* mb_indices  = mb->getIndices();

		// add each triangle from the mesh
		for (j=0; j<mb->getIndexCount(); j+=3)
		{
			v1i = mb_indices[j];
			v2i = mb_indices[j+1];
			v3i = mb_indices[j+2];

			vArray[0] = mb_vertices[v1i].Pos.X;
			vArray[1] = mb_vertices[v1i].Pos.Y;
			vArray[2] = mb_vertices[v1i].Pos.Z;
			vArray[3] = mb_vertices[v2i].Pos.X;
			vArray[4] = mb_vertices[v2i].Pos.Y;
			vArray[5] = mb_vertices[v2i].Pos.Z;
			vArray[6] = mb_vertices[v3i].Pos.X;
			vArray[7] = mb_vertices[v3i].Pos.Y;
			vArray[8] = mb_vertices[v3i].Pos.Z;

			NewtonTreeCollisionAddFace(collision, 3, (float*)vArray, 12, 1);
		}

	}
	NewtonTreeCollisionEndBuild(collision, 0);
    NewtonBody *body = makeNewtonBody(collision, 0,
                                      levelNode->getPosition(),
                                      levelNode->getRotation(),
                                      levelNode->getBoundingBox().getCenter());


	// set the newton world size based on the map size
	float boxP0[3];
	float boxP1[3];
	float matrix[4][4];
	NewtonBodyGetMatrix (body, &matrix[0][0]);
	NewtonCollisionCalculateAABB (collision, &matrix[0][0],  &boxP0[0], &boxP1[0]);
	// you can pad the box here if you wish
	//boxP0.y -= somevalue;
	//boxP1.y += somevaluef;
	NewtonSetWorldSize (nWorld, (float*)boxP0, (float*)boxP1);
    printf("TUMLE: World resized to fit level\n");

    //release the collision
    NewtonReleaseCollision(nWorld, collision);

    //make the physics node
    IPhysicsNode *pNode = new CPhysicsNode(this, PhysicsNodeCount++, levelNode, body, EBT_LEVEL, 0);

    //set the user date of the body
    NewtonBodySetUserData(body, pNode);

    //put the created node in the array
    CreatedPhysicsNodes.push_back(pNode);

    return pNode;
}

//! make a newton collision
NewtonCollision* CPhysicsManager::makeNewtonCollision(E_BODY_TYPE type, core::vector3df size, float* offset)
{

    NewtonCollision *collision = 0;
    switch(type)
    {
        case EBT_BOX:
        {
            collision = NewtonCreateBox(nWorld, size.X, size.Y, size.Z, offset);
            break;
        }
        case EBT_SPHERE:
        {
            collision = NewtonCreateSphere(nWorld, size.X, size.Y, size.Z, offset);
            break;
        }
        default:
        {
            printf("TUMLE: Wrong body type for the passed dimensions - no collision made\n");
        }
    }//switch

    return collision;
}

//! make a newton collision
NewtonCollision* CPhysicsManager::makeNewtonCollision(E_BODY_TYPE type, core::vector2df size, float* offset)
{

    NewtonCollision *collision = 0;

    switch(type)
    {
        case EBT_CONE:
        {
            collision = NewtonCreateCone(nWorld, size.X, size.Y, offset);
            break;
        }
        case EBT_CAPSULE:
        {
            collision = NewtonCreateCapsule(nWorld, size.X, size.Y, offset);
            break;
        }
        case EBT_CYLINDER:
        {
            collision = NewtonCreateCylinder(nWorld, size.X, size.Y, offset);
            break;
        }
        case EBT_CHAMFER_CYLINDER:
        {
            collision = NewtonCreateChamferCylinder(nWorld, size.X, size.Y, offset);
            break;
        }
        default:
        {
            printf("TUMLE: Wrong body type for the passed dimensions - no collision made\n");
        }
    }//switch

    return collision;
}


//! make a newton body using a newton collision
NewtonBody* CPhysicsManager::makeNewtonBody(NewtonCollision *collision,
                                            f32 mass,
                                            core::vector3df position,
                                            core::vector3df rotation,
                                            core::vector3df centerOfMass)
{

    NewtonBody *body = 0;

    //if the collision is valid
    if(collision)
    {
        //make a newton body from the collision
        body = NewtonCreateBody(nWorld, collision);

        //set the mass of the body
        NewtonBodySetMassMatrix(body, mass, 1.0f, 1.0f, 1.0f);

        //move the body to the position of the ISceneNode, and rotate it.
        core::matrix4 irrMat;
    	irrMat.setTranslation(position);
    	irrMat.setRotationDegrees(rotation);
        NewtonBodySetMatrix(body, irrMat.pointer());

        //set the Newton callbacks of the created body
        NewtonBodySetForceAndTorqueCallback(body, newtonForceAndTorqueCallback);
        NewtonBodySetTransformCallback(body, newtonTransformCallback);

        //move the mass to the center of the irrlicht node
        float com[3] = {centerOfMass.X, centerOfMass.Y, centerOfMass.Z};
        NewtonBodySetCentreOfMass(body, com);

    }//endif collision
    else
    {
        printf("TUMLE: no collision passed to makeNewtonBody()\n");
    }

    return body;

}

//! make a newton collision from an irrlicht bounding box
NewtonCollision* CPhysicsManager::makeCollisionFromBoundingBox(NewtonWorld *nWorld, core::aabbox3df box, core::vector3df scale)
{
    //calculate the dimensions of the bounding box
    core::vector3df boxSize((box.MaxEdge.X - box.MinEdge.X)*scale.X,
                            (box.MaxEdge.Y - box.MinEdge.Y)*scale.Y,
                            (box.MaxEdge.Z - box.MinEdge.Z)*scale.Z);

    //get the relative center of the bounding box and put it to a matrix4
    core::matrix4 boxCenterMatrix;
    boxCenterMatrix.setTranslation(box.getCenter());

    /* make the newton collision box from the dimensions of the bounding box
     and offset it to the center of the bounding box */
    return NewtonCreateBox(nWorld,
                           boxSize.X,
                           boxSize.Y,
                           boxSize.Z,
                           boxCenterMatrix.pointer());
}

//! make a newton collision from an irrlicht mesh
NewtonCollision* CPhysicsManager::makeCollisionFromMesh(NewtonWorld *nWorld, scene::IMesh *mesh)
{
  //Get number of vertices
  u32 nVertices = 0, nMeshBuffer;
  for( nMeshBuffer = 0 ; nMeshBuffer < mesh->getMeshBufferCount(); ++nMeshBuffer)
  {
    scene::IMeshBuffer *buffer = mesh->getMeshBuffer(nMeshBuffer);
    nVertices += buffer->getVertexCount();
  }

  // allocate block for positions of every vertex in mesh, no need to delete
  // anything, the array cleans up for us.
  core::array<core::vector3df> vertices;
  vertices.reallocate(nVertices);

  //Get mesh buffers and copy face vertices
  for( nMeshBuffer = 0 ; nMeshBuffer < mesh->getMeshBufferCount(); ++nMeshBuffer)
  {
    scene::IMeshBuffer *buffer = mesh->getMeshBuffer(nMeshBuffer);

    // handle the irrlicht supported vertex types
    switch(buffer->getVertexType())
    {
    case video::EVT_STANDARD:
      {
        video::S3DVertex* verts = (video::S3DVertex*)buffer->getVertices();
        for(u32 v = 0; v < buffer->getVertexCount(); ++v)
          vertices.push_back(verts[v].Pos);
      }
      break;

    case video::EVT_2TCOORDS:
      {
        video::S3DVertex* verts = (video::S3DVertex*)buffer->getVertices();
        for(u32 v = 0; v < buffer->getVertexCount(); ++v)
          vertices.push_back(verts[v].Pos);
      }
      break;

    case video::EVT_TANGENTS:
      {
        video::S3DVertexTangents* verts = (video::S3DVertexTangents*)buffer->getVertices();
        for(u32 v = 0; v < buffer->getVertexCount(); ++v)
          vertices.push_back(verts[v].Pos);
      }
      break;

    default:
      {
        printf("TUMLE: unknown vertex type.\n");
        return 0; // don't know vertex type! bail.
      }

    }
  }

  //Create Newton collision object
  return NewtonCreateConvexHull(nWorld, nVertices, &vertices[0].X, sizeof(core::vector3df), NULL);
}


// newton transform callback
void _cdecl CPhysicsManager::newtonTransformCallback(const NewtonBody* body, const float* matrix)
{
    //get the IPhysicsNode that called the callback
	CPhysicsNode *pNode = (CPhysicsNode*)NewtonBodyGetUserData(body);

	//copy the newton matrix to a irrlicht matrix4
    core::matrix4 irrMat;
    memcpy(irrMat.pointer(), matrix, sizeof(float)*16);

	if(pNode)
	{
        //if the physics node is set to use the default transform callback
        if(pNode->getUseDefaultTransformCallback())
        {
            //get the irrlicht node from the IPhysicsNode
            scene::ISceneNode *node = pNode->getIrrlichtNode();


            //move and rotate the irrlicht scene node
            if(node)
            {
                node->setPosition(irrMat.getTranslation());
                //don't rotate the camera
                if(!(pNode->getType() == EPNT_CAMERA))
                    node->setRotation(irrMat.getRotationDegrees());
            }
        }

        //if the physics node has an additional force and torque callback - run it
        if(pNode->additionalTransformCallback)
            pNode->additionalTransformCallback(pNode, &irrMat);

	}// if pNode
}

// newton force and torque callback
void _cdecl CPhysicsManager::newtonForceAndTorqueCallback(const NewtonBody* body)
{
    //get the IPhysicsNode that called the callback
	CPhysicsNode *pNode = (CPhysicsNode*)NewtonBodyGetUserData(body);

    if(pNode)
    {

        //if the physics node is set to use the default force and torque callback
        if(pNode->getUseDefaultForceAndTorqueCallback())
        {

            //get the force and torque array of the node
            f32 *force = pNode->getForceArray();
            f32 *torque = pNode->getTorqueArray();

            //put gravity in the force array and multiply it by the mass of the node
            force[0] += pNode->getPhysicsManager()->getGravity().X * pNode->getMass();
            force[1] += pNode->getPhysicsManager()->getGravity().Y * pNode->getMass();
            force[2] += pNode->getPhysicsManager()->getGravity().Z * pNode->getMass();

            //add the individual gravity to the force array
            force[0] += pNode->getGravity().X * pNode->getMass();
            force[1] += pNode->getGravity().Y * pNode->getMass();
            force[2] += pNode->getGravity().Z * pNode->getMass();

            //apply force and torque
            NewtonBodySetForce(body, force);
            NewtonBodySetTorque(body, torque);

            //reset the force and torque arrays
            force[0] = 0; force[1] = 0; force[2] = 0;
            torque[0] = 0; torque[1] = 0; torque[2] = 0;

            if(pNode->getType() == EPNT_CONTROLLABLE)
            {
                //cast the pNode in to a controllable physics node
                IControllablePhysicsNode *cPNode = (IControllablePhysicsNode*)pNode;
                //get the update interval in seconds
                f32 deltaT = cPNode->getPhysicsManager()->getUpdateInterval() / 1000;
                //get the current velocity
                f32 cv[3];
                NewtonBodyGetVelocity(cPNode->getNewtonBody(), cv);

                //rotate the desired velocity relative to the node
                core::vector3df dv = cPNode->getDesiredVelocity();
                cPNode->getIrrlichtNode()->getRelativeTransformation().rotateVect(dv);
                //calculate the force needed to move the node
                core::vector3df newForce = ( dv - core::vector3df(cv[0],0,cv[2]) ) * cPNode->getMass();
                //add the force
                f32 f[3] = {newForce.X / deltaT, 0, newForce.Z / deltaT};

                //cap the force
                if(cPNode->getCapForceMode())
                    cPNode->capForceDo(f);

                NewtonBodyAddForce(cPNode->getNewtonBody(), f);

                if(!cPNode->doesStrafe())
                {
                    //get the current omega
                    f32 co[3];
                    NewtonBodyGetOmega(cPNode->getNewtonBody(), co);
                    //get the disered omega
                    core::vector3df dOmega = cPNode->getDesiredOmega();
                    //rotate the omega vector ralative to the node
                    cPNode->getIrrlichtNode()->getRelativeTransformation().rotateVect(dOmega);
                    //calculate the torque needed to rotate the node
                    core::vector3df newTorque = ( dOmega - core::vector3df(co[0],co[1],co[2]) )*cPNode->getInertia()/cPNode->getMass();
                    //add the torque
                    f32 t[3] = {newTorque.X / deltaT, newTorque.Y / deltaT , newTorque.Z / deltaT};
                    NewtonBodyAddTorque(cPNode->getNewtonBody(), t);
                }
                //the disered velocity is set to zero in the update() function of the controllable node
            }

            if(pNode->getType() == EPNT_CAMERA)
            {
                //cast the pNode in to a FPS camera physics node
                ICameraFPSPhysicsNode *cPNode = (ICameraFPSPhysicsNode*)pNode;
                //get the update interval in seconds
                f32 deltaT = cPNode->getPhysicsManager()->getUpdateInterval() / 1000;
                //get the current velocity
                f32 cv[3];
                NewtonBodyGetVelocity(cPNode->getNewtonBody(), cv);
                //rotate the desired velocity relative to the camera
                core::vector3df dv = cPNode->getDesiredVelocity();
                cPNode->getIrrlichtNode()->getRelativeTransformation().rotateVect(dv);
                //calculate the force needed to move the node
                core::vector3df newForce = ( dv - core::vector3df(cv[0],0,cv[2]) ) * cPNode->getMass();
                f32 f[3] = {newForce.X / deltaT, 0, newForce.Z / deltaT};

                //cap the force
                if(cPNode->getCapForceMode())
                    cPNode->capForceDo(f);

                //add the force
                NewtonBodyAddForce(cPNode->getNewtonBody(), f);

                //the disered velocity is set to zero in the update() function of the camera node
            }

        }

        //if the physics node has an additional force and torque callback - run it
        if(pNode->additionalForceAndTorqueCallback)
            pNode->additionalForceAndTorqueCallback(pNode);
    }

}


// function to show the newton collision
void CPhysicsManager::NewtonDebugCollision(const NewtonBody* body, int vertexCount, const float* FaceArray, int faceId)
{
   core::vector3df p0(FaceArray[0], FaceArray[1], FaceArray[2]);
   const video::SColor c0(0, 0, 255, 0);
   for(int i = 2; i < vertexCount; i ++){
      core::vector3df p1(FaceArray[(i-1) * 3 + 0], FaceArray[(i-1) * 3 + 1], FaceArray[(i-1) * 3 + 2]);
      core::vector3df p2(FaceArray[i * 3 + 0], FaceArray[i * 3 + 1], FaceArray[i * 3 + 2]);
      core::triangle3df t;
      t.set(p1, p2, p0);
      tumle::activePhysicsManager->getIrrlichtDevice()->getVideoDriver()->draw3DTriangle(t, c0);
    }
}

// function to show the newton collision
void CPhysicsManager::NewtonDebugBody(const NewtonBody* body)
{
   if(!tumle::activePhysicsManager->getIrrlichtDevice()->getVideoDriver()) return;
   NewtonBodyForEachPolygonDo(body, NewtonDebugCollision);
}

//! show the newton collision geometry
void CPhysicsManager::showCollision()
{
   core::matrix4 mat;
   video::SMaterial material;
   //material.setTextures(0,0);
   material.Lighting = false;
   tumle::activePhysicsManager->getIrrlichtDevice()->getVideoDriver()->setTransform(video::ETS_WORLD, mat);
   tumle::activePhysicsManager->getIrrlichtDevice()->getVideoDriver()->setMaterial(material);
   NewtonWorldForEachBodyDo(nWorld, NewtonDebugBody);
}

void CPhysicsManager::leaveNewtonWorldEvent(const NewtonBody *body)
{
    //get the physics node that called the function
    IPhysicsNode *node = (IPhysicsNode*)NewtonBodyGetUserData(body);

    // get the physics manager from the node
    IPhysicsManager* pmgr = node->getPhysicsManager();

    //get the leave world event function pointer
    void (*leaveEvent)(IPhysicsNode*) = pmgr->getLeaveWorldEvent();

    //if a function is specified - run it
    if(leaveEvent)
        leaveEvent(node);
}


}//end namespace tumle
