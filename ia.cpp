#include "ia.hpp"

CIA::CIA(int iniciox,int inicioy)
{
    srand(time(NULL));
    posx = iniciox;
    posy = inicioy;
    jposx = 0;
    jposy = 0;
    direcao = 0;
    ///   A*   ///
    H = 0;
    G = 0;
    Caminho_Encontrado = false;
    /// Fim A* ///
}

void CIA::move(int dificuldade)
{
    if(dificuldade==1||dificuldade==2)
        {
            verCol();
            while (dirCol[direcao])
            {
                direcao = rand() % 5;
                if(dificuldade==2)
                {
                    while(lastdirecao==direcao&&numCol<3&&!dirCol[direcao])
                    {
                        direcao = rand() % 5;
                        while (dirCol[direcao])
                        {
                            direcao = rand() % 5;
                        }
                    }
                }
            }

            if (numCol<lastNumCol)
            {
                direcao = rand() % 5;
                while (dirCol[direcao])
                {
                    direcao = rand() % 5;
                }
                if(dificuldade==2)
                {
                    while(lastdirecao==direcao&&numCol<3&&!dirCol[direcao])
                    {
                        direcao = rand() % 5;
                        while (dirCol[direcao])
                        {
                            direcao = rand() % 5;
                        }
                    }
                }
            }
            lastNumCol=numCol;
            if (!dirCol[direcao])
            {
                mapa[posx][posy]=0;
                if (direcao==0)
                {
                    posy--;
                    lastdirecao=2;
                }
                if (direcao==1)
                {
                    posx++;
                    lastdirecao=3;
                }
                if (direcao==2)
                {
                    posy++;
                    lastdirecao=0;
                }
                if (direcao==3)
                {
                    posx--;
                    lastdirecao=1;
                }
            }
        }

        if(dificuldade==3)/// A* PathFind
        {
            /// Heur�stica
            H = posx > *jposx ? posx - *jposx : *jposx - posx;
            H += posy > *jposy ? posy - *jposy : *jposy - posy;

            /// Custo
            G = 0;

            /// Cria primeiro n�
            Lista_AbertaX.push_back(posx);
            Lista_AbertaY.push_back(posy);
            Lista_AbertaH.push_back(H);
            Lista_AbertaG.push_back(G);
            Lista_AbertaF.push_back(H + G);
            Lista_AbertaPx.push_back(posx);
            Lista_AbertaPy.push_back(posy);

            while( Lista_AbertaX.size() > 0 && !Caminho_Encontrado)
            {
                    /** Reorganiza lista aberta pelo valor crescente de F
                     esse algortimo vai de posicao em posi��o verificando quem eh o menor,
                     segundo menor, terceiro menor, etc e reajustando as posi��es **/
                     for(int I = 0; I < Lista_AbertaX.size(); I++)
                     {
                         int menor = I;
                         for(int II = 0; II < Lista_AbertaX.size(); II++)
                         {
                             if(Lista_AbertaF[II] < Lista_AbertaF[I])
                                menor = II;
                         }
                         if(menor != I) /// Troca o menor pelo atual...
                         {
                             int TempX = Lista_AbertaX[I],
                                 TempY = Lista_AbertaY[I],
                                 TempH = Lista_AbertaH[I],
                                 TempG = Lista_AbertaG[I],
                                 TempF = Lista_AbertaF[I],
                                 TempPx = Lista_AbertaPx[I],
                                 TempPy = Lista_AbertaPy[I];

                            Lista_AbertaX[I] = Lista_AbertaX[menor],
                            Lista_AbertaY[I] = Lista_AbertaY[menor],
                            Lista_AbertaH[I] = Lista_AbertaH[menor],
                            Lista_AbertaG[I] = Lista_AbertaG[menor],
                            Lista_AbertaF[I] = Lista_AbertaF[menor],
                            Lista_AbertaPx[I] = Lista_AbertaPx[menor],
                            Lista_AbertaPy[I] = Lista_AbertaPy[menor];

                            Lista_AbertaX[I] = TempX,
                            Lista_AbertaY[I] = TempY,
                            Lista_AbertaH[I] = TempH,
                            Lista_AbertaG[I] = TempG,
                            Lista_AbertaF[I] = TempF,
                            Lista_AbertaPx[I] = TempPx,
                            Lista_AbertaPy[I] = TempPy;
                         }
                     }

                     /// Passa o n� com menor valor de F para a Lista Fechada

                     Lista_FechadaX.push_back(Lista_AbertaX[0]);
                     Lista_FechadaY.push_back(Lista_AbertaY[0]);
                     Lista_FechadaH.push_back(Lista_AbertaH[0]);
                     Lista_FechadaG.push_back(Lista_AbertaG[0]);
                     Lista_FechadaF.push_back(Lista_AbertaF[0]);
                     Lista_FechadaPx.push_back(Lista_AbertaPx[0]);
                     Lista_FechadaPy.push_back(Lista_AbertaPy[0]);

                     /// Adiciona os lados do ultimo n� adicionado na lista aberta na Lista Aberta
                     /// PS: no Pac-Man feito para o contest s� existe movimenta��o em 4 dire��es

                     Caminho_Encontrado = false;
                     for(int Lado = 0; Lado < 4; Lado++)
                     {
                         int X_Atual,
                             Y_Atual;
                         if(Lado == 0) /// Testa direita
                         {
                             X_Atual = Lista_AbertaX[0]+1;
                             Y_Atual = Lista_AbertaY[0];
                             /// Verifica se achou o destino
                             if(X_Atual == *jposx && Y_Atual == *jposy)
                            {
                                /// No caso do Pac-Man, n�o tem n� de custo diferente, ent�o o G
                                /// conta o numero de n�s que o caminho percorreu, se tivesse
                                /// n�s de custos diferentes, precisar�amos de um contador.
                                CaminhoX.push_front(X_Atual);
                                CaminhoY.push_front(Y_Atual);
                                int Pai_Atual = Lista_FechadaX.size()-1;
                                for(int I =  Lista_FechadaG[Lista_FechadaX.size()-1]; I > 0; I--)
                                {
                                    CaminhoX.push_front(Lista_FechadaX[Pai_Atual]);
                                    CaminhoY.push_front(Lista_FechadaY[Pai_Atual]);
                                    for(int II = 0; II < Lista_FechadaX.size(); II++)
                                    {
                                        if(Lista_FechadaX[II]==Lista_FechadaPx[Pai_Atual]
                                        &&Lista_FechadaY[II]==Lista_FechadaPy[Pai_Atual])
                                        {
                                            Pai_Atual = II;
                                        }
                                    }
                                }
                                Caminho_Encontrado = true;
                            }
                            else/// Se n�o achou continua o algoritimo
                            {
                                /// Verifica se o lado direito � and�vel
                                if(mapa[X_Atual][Y_Atual] == 0)
                                {
                                    /// Verifica se o lado direito j� est� na Lista Fechada
                                    bool existe = false;
                                    for(int Verifica_LF = 0; Verifica_LF < Lista_FechadaX.size()-1; Verifica_LF++)
                                    {
                                        if(X_Atual == Lista_FechadaX[Verifica_LF] && Y_Atual == Lista_FechadaY[Verifica_LF])
                                        {
                                            existe = true;
                                        }
                                    }
                                    if(!existe)
                                    {
                                        H = X_Atual > *jposx ? X_Atual - *jposx : *jposx - X_Atual;
                                        H += Y_Atual > *jposy ? Y_Atual - *jposy : *jposy - Y_Atual;

                                        G = Lista_AbertaG[0]+1;

                                        Lista_AbertaX.push_back(X_Atual);
                                        Lista_AbertaY.push_back(Y_Atual);
                                        Lista_AbertaH.push_back(H);
                                        Lista_AbertaG.push_back(G);
                                        Lista_AbertaF.push_back(H + G);
                                        Lista_AbertaPx.push_back(Lista_FechadaX[Lista_FechadaX.size()-1]);
                                        Lista_AbertaPy.push_back(Lista_FechadaY[Lista_FechadaY.size()-1]);
                                    }
                                }
                            }
                         }

                        if(Lado == 1 && !Caminho_Encontrado) /// Testa Esquerda
                        {
                             X_Atual = Lista_AbertaX[0]-1;
                             Y_Atual = Lista_AbertaY[0];
                             /// Verifica se achou o destino
                             if(X_Atual == *jposx && Y_Atual == *jposy)
                            {
                                /// No caso do Pac-Man, n�o tem n� de custo diferente, ent�o o G
                                /// conta o numero de n�s que o caminho percorreu, se tivesse
                                /// n�s de custos diferentes, precisar�amos de um contador.
                                CaminhoX.push_front(X_Atual);
                                CaminhoY.push_front(Y_Atual);
                                int Pai_Atual = Lista_FechadaX.size()-1;
                                for(int I =  Lista_FechadaG[Lista_FechadaX.size()-1]; I > 0; I--)
                                {
                                    CaminhoX.push_front(Lista_FechadaX[Pai_Atual]);
                                    CaminhoY.push_front(Lista_FechadaY[Pai_Atual]);
                                    for(int II = 0; II < Lista_FechadaX.size(); II++)
                                    {
                                        if(Lista_FechadaX[II]==Lista_FechadaPx[Pai_Atual]
                                        &&Lista_FechadaY[II]==Lista_FechadaPy[Pai_Atual])
                                        {
                                            Pai_Atual = II;
                                        }
                                    }
                                }
                                Caminho_Encontrado = true;
                            }
                            else/// Se n�o achou continua o algoritimo
                            {
                                /// Verifica se o lado direito � and�vel
                                if(mapa[X_Atual][Y_Atual] == 0)
                                {
                                    /// Verifica se o lado direito j� est� na Lista Fechada
                                    bool existe = false;
                                    for(int Verifica_LF = 0; Verifica_LF < Lista_FechadaX.size()-1; Verifica_LF++)
                                    {
                                        if(X_Atual == Lista_FechadaX[Verifica_LF] && Y_Atual == Lista_FechadaY[Verifica_LF])
                                        {
                                            existe = true;
                                        }
                                    }
                                    if(!existe)
                                    {
                                        H = X_Atual > *jposx ? X_Atual - *jposx : *jposx - X_Atual;
                                        H += Y_Atual > *jposy ? Y_Atual - *jposy : *jposy - Y_Atual;

                                        G = Lista_AbertaG[0]+1;

                                        Lista_AbertaX.push_back(X_Atual);
                                        Lista_AbertaY.push_back(Y_Atual);
                                        Lista_AbertaH.push_back(H);
                                        Lista_AbertaG.push_back(G);
                                        Lista_AbertaF.push_back(H + G);
                                        Lista_AbertaPx.push_back(Lista_FechadaX[Lista_FechadaX.size()-1]);
                                        Lista_AbertaPy.push_back(Lista_FechadaY[Lista_FechadaY.size()-1]);
                                    }
                                }
                            }
                         }

                         if(Lado == 2 && !Caminho_Encontrado) /// Testa Cima
                        {
                             X_Atual = Lista_AbertaX[0];
                             Y_Atual = Lista_AbertaY[0]-1;
                             /// Verifica se achou o destino
                             if(X_Atual == *jposx && Y_Atual == *jposy)
                            {
                                /// No caso do Pac-Man, n�o tem n� de custo diferente, ent�o o G
                                /// conta o numero de n�s que o caminho percorreu, se tivesse
                                /// n�s de custos diferentes, precisar�amos de um contador.
                                CaminhoX.push_front(X_Atual);
                                CaminhoY.push_front(Y_Atual);
                                int Pai_Atual = Lista_FechadaX.size()-1;
                                for(int I =  Lista_FechadaG[Lista_FechadaX.size()-1]; I > 0; I--)
                                {
                                    CaminhoX.push_front(Lista_FechadaX[Pai_Atual]);
                                    CaminhoY.push_front(Lista_FechadaY[Pai_Atual]);
                                    for(int II = 0; II < Lista_FechadaX.size(); II++)
                                    {
                                        if(Lista_FechadaX[II]==Lista_FechadaPx[Pai_Atual]
                                        &&Lista_FechadaY[II]==Lista_FechadaPy[Pai_Atual])
                                        {
                                            Pai_Atual = II;
                                        }
                                    }
                                }
                                Caminho_Encontrado = true;
                            }
                            else/// Se n�o achou continua o algoritimo
                            {
                                /// Verifica se o lado direito � and�vel
                                if(mapa[X_Atual][Y_Atual] == 0)
                                {
                                    /// Verifica se o lado direito j� est� na Lista Fechada
                                    bool existe = false;
                                    for(int Verifica_LF = 0; Verifica_LF < Lista_FechadaX.size()-1; Verifica_LF++)
                                    {
                                        if(X_Atual == Lista_FechadaX[Verifica_LF] && Y_Atual == Lista_FechadaY[Verifica_LF])
                                        {
                                            existe = true;
                                        }
                                    }
                                    if(!existe)
                                    {
                                        H = X_Atual > *jposx ? X_Atual - *jposx : *jposx - X_Atual;
                                        H += Y_Atual > *jposy ? Y_Atual - *jposy : *jposy - Y_Atual;

                                        G = Lista_AbertaG[0]+1;

                                        Lista_AbertaX.push_back(X_Atual);
                                        Lista_AbertaY.push_back(Y_Atual);
                                        Lista_AbertaH.push_back(H);
                                        Lista_AbertaG.push_back(G);
                                        Lista_AbertaF.push_back(H + G);
                                        Lista_AbertaPx.push_back(Lista_FechadaX[Lista_FechadaX.size()-1]);
                                        Lista_AbertaPy.push_back(Lista_FechadaY[Lista_FechadaY.size()-1]);
                                    }
                                }
                            }
                         }

                         if(Lado == 3 && !Caminho_Encontrado) /// Testa Baixo
                        {
                             X_Atual = Lista_AbertaX[0];
                             Y_Atual = Lista_AbertaY[0]+1;
                             /// Verifica se achou o destino
                             if(X_Atual == *jposx && Y_Atual == *jposy)
                            {
                                /// No caso do Pac-Man, n�o tem n� de custo diferente, ent�o o G
                                /// conta o numero de n�s que o caminho percorreu, se tivesse
                                /// n�s de custos diferentes, precisar�amos de um contador.
                                CaminhoX.push_front(X_Atual);
                                CaminhoY.push_front(Y_Atual);
                                int Pai_Atual = Lista_FechadaX.size()-1;
                                for(int I =  Lista_FechadaG[Lista_FechadaX.size()-1]; I > 0; I--)
                                {
                                    CaminhoX.push_front(Lista_FechadaX[Pai_Atual]);
                                    CaminhoY.push_front(Lista_FechadaY[Pai_Atual]);
                                    for(int II = 0; II < Lista_FechadaX.size(); II++)
                                    {
                                        if(Lista_FechadaX[II]==Lista_FechadaPx[Pai_Atual]
                                        &&Lista_FechadaY[II]==Lista_FechadaPy[Pai_Atual])
                                        {
                                            Pai_Atual = II;
                                        }
                                    }
                                }
                                Caminho_Encontrado = true;
                            }
                            else/// Se n�o achou continua o algoritimo
                            {
                                /// Verifica se o lado direito � and�vel
                                if(mapa[X_Atual][Y_Atual] == 0)
                                {
                                    /// Verifica se o lado direito j� est� na Lista Fechada
                                    bool existe = false;
                                    for(int Verifica_LF = 0; Verifica_LF < Lista_FechadaX.size()-1; Verifica_LF++)
                                    {
                                        if(X_Atual == Lista_FechadaX[Verifica_LF] && Y_Atual == Lista_FechadaY[Verifica_LF])
                                        {
                                            existe = true;
                                        }
                                    }
                                    if(!existe)
                                    {
                                        H = X_Atual > *jposx ? X_Atual - *jposx : *jposx - X_Atual;
                                        H += Y_Atual > *jposy ? Y_Atual - *jposy : *jposy - Y_Atual;

                                        G = Lista_AbertaG[0]+1;

                                        Lista_AbertaX.push_back(X_Atual);
                                        Lista_AbertaY.push_back(Y_Atual);
                                        Lista_AbertaH.push_back(H);
                                        Lista_AbertaG.push_back(G);
                                        Lista_AbertaF.push_back(H + G);
                                        Lista_AbertaPx.push_back(Lista_FechadaX[Lista_FechadaX.size()-1]);
                                        Lista_AbertaPy.push_back(Lista_FechadaY[Lista_FechadaY.size()-1]);
                                    }
                                }
                            }
                         }

                     } /// Fim do For dos 4 lados

                     if(Lista_AbertaX.size() > 0)
                     {
                         Lista_AbertaX.pop_front();
                         Lista_AbertaY.pop_front();
                         Lista_AbertaH.pop_front();
                         Lista_AbertaG.pop_front();
                         Lista_AbertaF.pop_front();
                         Lista_AbertaPx.pop_front();
                         Lista_AbertaPy.pop_front();
                     }

            }///Fim do while

            /// Anda
            if(Caminho_Encontrado&&CaminhoX.size() > 0)
             {
                 posx=CaminhoX[0];
                 posy=CaminhoY[0];
                 CaminhoX.pop_front();
                 CaminhoY.pop_front();
             }

            /// Limpa Vetores
            while(Lista_AbertaX.size()>0)
            {
                 Lista_AbertaX.pop_front();
                 Lista_AbertaY.pop_front();
                 Lista_AbertaH.pop_front();
                 Lista_AbertaG.pop_front();
                 Lista_AbertaF.pop_front();
                 Lista_AbertaPx.pop_front();
                 Lista_AbertaPy.pop_front();
            }
            while(Lista_FechadaX.size()>0)
            {
                 Lista_FechadaX.pop_front();
                 Lista_FechadaY.pop_front();
                 Lista_FechadaH.pop_front();
                 Lista_FechadaG.pop_front();
                 Lista_FechadaF.pop_front();
                 Lista_FechadaPx.pop_front();
                 Lista_FechadaPy.pop_front();
            }

            if(Caminho_Encontrado)
            {
                while(CaminhoX.size() > 0)
                {
                    CaminhoX.pop_front();
                    CaminhoY.pop_front();
                }
                Caminho_Encontrado = false;
            }

        }/// Fim do A*
}

void CIA::verCol()
{
    numCol = 0;
    dirCol[0] = false;
    dirCol[1] = false;
    dirCol[2] = false;
    dirCol[3] = false;
    if (mapa[posx][posy-1]==1)
    {
        numCol++;
        dirCol[0] = true;
    }
    if (mapa[posx+1][posy]==1)
    {
        numCol++;
        dirCol[1] = true;
    }
    if (mapa[posx][posy+1]==1)
    {
        numCol++;
        dirCol[2] = true;
    }
    if (mapa[posx-1][posy]==1)
    {
        numCol++;
        dirCol[3] = true;
    }
}

void CIA::getMap(int map[MAXX][MAXY])
{
    for (int x = 0;x<MAXX ;x++ )
    {
        for (int y = 0;y<MAXY ;y++ )
        {
            mapa[x][y] = map[x][y];
        }
    }
}
void CIA::getPlayerPos(int *x,int *y)
{
    jposx = x;
    jposy = y;
}
