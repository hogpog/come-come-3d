// CPhysicsManager.h
// a part of Tumle

#ifndef TUMLE_C_PHYSICS_MANAGER_H
#define TUMLE_C_PHYSICS_MANAGER_H


#include "tumle.h"

using namespace irr;

namespace tumle
{

class CPhysicsManager : public IPhysicsManager
{
public:

    //! constructor
    CPhysicsManager(IrrlichtDevice *irrDevice, NewtonWorld *newtonWorld);

    //! destructor
    virtual ~CPhysicsManager();

    //! delete the physics manager
    /*! this will delete everyting it has created */
    virtual void remove();

    //! add a physics node using a irrlicht node
    /*! this function is used for bodies of the type EBT_BOX and EBT_SPHERE (vector3df is used for size)
       \param node: the irrlicht node to represent the physics node
       \param type: the body type
       \param mass: the mass of the body
       \param size: the size of the body
       \return Returns a pointer to the created physics node */
    virtual IPhysicsNode* addPhysicsNode(scene::ISceneNode *node,
                                        E_BODY_TYPE type,
                                        f32 mass,
                                        core::vector3df size);

    //! add a physics node using a irrlicht node
    /*! this function is used for bodies of the type EBT_CONE, EBT_CAPSULE, EBT_CYLINDER and
    EBT_CHAMFER_CYLINDER (vector2df is used for size)
       \param node: the irrlicht node to represent the physics node
       \param type: the body type
       \param mass: the mass of the body
       \param size: the size of the body
       \return Returns a pointer to the created physics node */
    virtual IPhysicsNode* addPhysicsNode(scene::ISceneNode *node,
                                        E_BODY_TYPE type,
                                        f32 mass,
                                        core::vector2df size,
                                        core::vector3df rotation);

    //! add a physics node using a irrlicht node
    /*! this function is used for bodies of the type EBT_BOUNDING_BOX and EBT_MESH (no size is used)
       \param node: the irrlicht node to represent the physics node
       \param type: the body type
       \param mass: the mass of the body
       \return Returns a pointer to the created physics node */
    virtual IPhysicsNode* addPhysicsNode(scene::ISceneNode *node,
                                        E_BODY_TYPE type,
                                        f32 mass,
                                        scene::IMesh *mesh);

    //! add a physics node using a irrlicht node
    /*! this function is used for bodies of the type EBT_BOX and EBT_SPHERE (vector3df is used for size)
       \param node: the irrlicht node to represent the physics node
       \param type: the body type
       \param mass: the mass of the body
       \param size: the size of the body
       \return Returns a pointer to the created physics node */
    virtual IControllablePhysicsNode* addControllablePhysicsNode(scene::ISceneNode *node,
                                                                E_BODY_TYPE type,
                                                                f32 mass,
                                                                core::vector3df size,
                                                                SKeyMap *keyMapArray,
                                                                u32 keyMapSize,
                                                                f32 moveSpeed,
                                                                f32 rotateSpeed,
                                                                bool fixedUpVector,
                                                                bool strafe);

    //! add a physics node using a irrlicht node
    /*! tthis function is used for bodies of the type EBT_CONE, EBT_CAPSULE, EBT_CYLINDER and
    EBT_CHAMFER_CYLINDER (vector2df is used for size)
       \param node: the irrlicht node to represent the physics node
       \param type: the body type
       \param mass: the mass of the body
       \param size: the size of the body
       \return Returns a pointer to the created physics node */
    virtual IControllablePhysicsNode* addControllablePhysicsNode(scene::ISceneNode *node,
                                                                E_BODY_TYPE type,
                                                                f32 mass,
                                                                core::vector2df size,
                                                                core::vector3df rotation,
                                                                SKeyMap *keyMapArray,
                                                                u32 keyMapSize,
                                                                f32 moveSpeed,
                                                                f32 rotateSpeed,
                                                                bool fixedUpVector,
                                                                bool strafe);

    //! add a physics level node
    /*! make a level collidable. This also rezises the newton world to fir the size of the level.
    Thanks to Mercior for his Newton/Irrlicht example!
       \param levelNode: the scene node that holds the level mesh
       \param mesh: if the node has no mesh, pass the mesh here */
    virtual IPhysicsNode *addPhysicsLevelNode(scene::ISceneNode *levelNode, scene::IMesh *mesh);
    virtual IPhysicsNode *addPhysicsLevelNode2T(scene::ISceneNode *levelNode, scene::IMesh *mesh);

    //! add a FPS camera
    virtual ICameraFPSPhysicsNode *addCameraPhysicsNodeFPS(E_BODY_TYPE,
                                                          f32 mass,
                                                          core::vector3df size,
                                                          SKeyMap *ketMapArray,
                                                          u32 keyMapSize,
                                                          f32 moveSpeed,
                                                          f32 rotateSpeed,
                                                          bool fixUpVector);

    //! add a FPS camera
    virtual ICameraFPSPhysicsNode *addCameraPhysicsNodeFPS(E_BODY_TYPE,
                                                          f32 mass,
                                                          core::vector2df size,
                                                          core::vector3df rotation,
                                                          SKeyMap *ketMapArray,
                                                          u32 keyMapSize,
                                                          f32 moveSpeed,
                                                          f32 rotateSpeed,
                                                          bool fixUpVector);

    //! add a FPS camera
    virtual ICameraFPSPhysicsNode *addCameraPhysicsNodeFPS(E_BODY_TYPE type,
                                                          f32 mass,
                                                          scene::IMesh *mesh,
                                                          SKeyMap *keyMapArray,
                                                          u32 keyMapSize,
                                                          f32 moveSpeed,
                                                          f32 rotateSpeed,
                                                          bool fixUpVector);

    //! get the gravity
    virtual core::vector3df getGravity(){ return NewtonGravity; };

    //! set the gravity
    virtual void setGravity(core::vector3df newGravity){ NewtonGravity = newGravity; };

    //! make a newton collision
    /*! this function is used with EBT_BOX and EBT_SPHERE (vector3df is used for size)
       \param type: the type of collision to make
       \param size: the dimensions of the collision
       \param offset: a 4x4 matrix to offset the collision
       \return returns a pointer to the created collision */
    virtual NewtonCollision* makeNewtonCollision(E_BODY_TYPE type, core::vector3df size, float* offset);

    //! make a newton collision
    /*! this function is used with EBT_CONE, EBT_CAPSULE, EBT_CYLINDER and
    EBT_CHAMFER_CYLINDER (vector2df is used for size)
       \param type: the type of collision to make
       \param size: the dimensions of the collision
       \param offset: a 4x4 matrix to offset the collision
       \return returns a pointer to the created collision */
    virtual NewtonCollision* makeNewtonCollision(E_BODY_TYPE type, core::vector2df size, float* offset);

    //! make a newton collision from an irrlicht bounding box
    virtual NewtonCollision* makeCollisionFromBoundingBox(NewtonWorld *nWorld, core::aabbox3df box, core::vector3df scale);

    //! make a newton collision from an irrlicht mesh
    virtual NewtonCollision* makeCollisionFromMesh(NewtonWorld *nWorld, scene::IMesh *mesh);


    //! make a newton body using a newton collision
    /*! \param collision: the newton collision to make the body from
       \param position: the position of the newton body
       \param rotation: the rotation of the newton body
       \return returns a pointer the created body */
    virtual NewtonBody* makeNewtonBody(NewtonCollision *collision,
                                      f32 mass,
                                      core::vector3df position,
                                      core::vector3df rotation,
                                      core::vector3df centerOfMass);

    //! get the irrlicgt device
    virtual IrrlichtDevice* getIrrlichtDevice(){ return device; };

    //! get the newton world
    virtual NewtonWorld* getNewtonWorld(){ return nWorld; };

    //! set a pointer in the CreatedPhysicsNodes array to 0
    virtual void setPointerToZero(s32 index){ CreatedPhysicsNodes[index] = 0; };

    //! update the newton world
    /*! this also updates stuff in the world that was not added with Tumle */
    virtual void update();

    //! show the newton collision geometry
    virtual void showCollision();

    //! get the material manager
    virtual IMaterialManager* getMaterialManager(){ return MaterialManager; };

    //! get the update interval
    virtual f32 getUpdateInterval(){ return dt; };

    //! set the update interval
    virtual void setUpdateInterval(f32 ms){ dt = ms; };

    //! get a node from its name
    /*! \return Returns the first node of the passed name. */
    virtual IPhysicsNode *getPhysicsNodeFromName(const c8 *name);

    //! the leave world event pointer
    void (*LeaveWorldEvent)(IPhysicsNode* node);

    //! get the leave world event
    virtual void (*getLeaveWorldEvent())(IPhysicsNode*) { return LeaveWorldEvent; };

    //! set the function to call when a physics node leaves the newton world
    virtual void setLeaveWorldEvent(void (*leaveWorldEvent)(IPhysicsNode *node))
    {
       LeaveWorldEvent = leaveWorldEvent;
    };

    //! set the size of the newton world
    virtual void setWorldSize(core::vector3df min, core::vector3df max)
    {
       WorldMin = min;
       WorldMax = max;
       f32 nMin[3] = {WorldMin.X, WorldMin.Y, WorldMin.Z};
       f32 nMax[3] = {WorldMax.X, WorldMax.Y, WorldMax.Z};
       NewtonSetWorldSize(nWorld, nMin, nMax);
    };

    virtual void getWorldSize(core::vector3df& min, core::vector3df& max)
    {
       min = WorldMin;
       max = WorldMax;
    };

    //the default leave world event
    static void leaveNewtonWorldEvent(const NewtonBody *body);

    //newton callbacks
    static void _cdecl newtonTransformCallback(const NewtonBody* body, const float* matrix);
    static void _cdecl newtonForceAndTorqueCallback(const NewtonBody* body);

    //functions that shows the newton collisions
    static void NewtonDebugBody(const NewtonBody* body);
    static void NewtonDebugCollision(const NewtonBody* body, int vertexCount, const float* FaceArray, int faceId);

	//the memory functions for newton
	static void* physicsAlloc (int sizeInBytes){ return malloc (sizeInBytes); }

	static void physicsFree (void *ptr, int sizeInBytes){ free (ptr); }



private:
    //! the irrlicht device
    IrrlichtDevice *device;

    //! the newton world
    NewtonWorld *nWorld;

    //! the material manager of the physics manager
    IMaterialManager *MaterialManager;

    //! the gravity
    core::vector3df NewtonGravity;

    //! the number of created physics nodes
    s32 PhysicsNodeCount;

    //! array to hold pointer to the created physics nodes
    core::array<IPhysicsNode*> CreatedPhysicsNodes;

    // the update interval
    f32 dt;

    // the current time
    f32 CurrentTime;

    // the time accumulator
    f32 TimeAccumulator;

    //! the min point of the world
    core::vector3df WorldMin;

    //! the max point of the world
    core::vector3df WorldMax;

};

IPhysicsManager *activePhysicsManager = 0;


IPhysicsManager *createPhysicsManager(IrrlichtDevice *irrDevice)
{

	//create the newton world
	NewtonWorld *newtonWorld =  NewtonCreate(CPhysicsManager::physicsAlloc, CPhysicsManager::physicsFree);

	//if a physics mangager is already in use
    if(tumle::activePhysicsManager)
    {
        //delete the old physics manager
        tumle::activePhysicsManager->remove();
        tumle::activePhysicsManager = 0;
        printf("TUMLE: old physics manager deleted when a new was created\n");
    }

    IPhysicsManager* pmgr = new CPhysicsManager(irrDevice, newtonWorld);
    if(pmgr)
	{
		tumle::activePhysicsManager = pmgr;
		printf("TUMLE: PhysicsManager created\n");
	}
	return pmgr;
}


}//end namespace tumle

#endif
