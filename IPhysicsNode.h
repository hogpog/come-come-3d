// IPhysicsNode.h
// a part of Tumle

#ifndef TUMLE_I_PHYSICS_NODE_H
#define TUMLE_I_PHYSICS_NODE_H

#include "tumle.h"

using namespace irr;

namespace tumle
{

class IPhysicsManager;

//! physics node iterface
/*! a physics node holds the irrlicht scene node and the newton body */
class IPhysicsNode
{
public:
    //! destructor
    virtual ~IPhysicsNode(){};

    //! delete the physics node
    /*! this deletes both the irrlicht scene node and the newton body, and
    removes the pointer to the physics node from the physics manager.
    A physics node can not be deleted in a procces collision callback */
    virtual void remove() = 0;

    //! get the irrlicht scene node of the physics node
    /*! \return returns the irrlicht scene node passed to the physics node */
    virtual scene::ISceneNode* getIrrlichtNode() = 0;

    //! get the newton body of the physics node
    /*! \return returns the newton body of the physics node. This is usefull if you want to use
    Newton functions not avalable in Tumle. */
    virtual NewtonBody* getNewtonBody() = 0;

    //! set the position of the physics node
    /*! this moves both the irrlicht scene node and the newton body */
    virtual void setPosition(core::vector3df newPos) = 0;

    //! set the rotation of the physics node
    /*! this rotates both the irrlicht scene node and the newton body */
    virtual void setRotation(core::vector3df newRot) = 0;

    //! get the position of the physics scene node
    /*! this gets the position of the irrlicht scene node
    \return returns a vector3df */
    virtual core::vector3df getPosition() = 0;

    //! get the rotation of the physics scene node
    /*! this gets the rotation of the irrlicht scene node
    \return returns a vector3df */
    virtual core::vector3df getRotation() = 0;

    //! get the body type of the physics node
    /*!  \return returns a in::E_BODY_TYPE value */
    virtual E_BODY_TYPE getBodyType() = 0;

    //! get the physics node type
    /*! \return returns the type of the physics node. See E_PHYSICS_NODE_TYPE. */
    virtual E_PHYSICS_NODE_TYPE getType() = 0;

    //! get the physics manager that created the node
    /*! \return returns the IPhysicsManager that created it */
    virtual IPhysicsManager* getPhysicsManager() = 0;

    //! set the material
    /*! set the material of the node, use the material manager to create materials */
    virtual void setMaterial(materialID material) = 0;

    //! get the material of the physics node
    /*! \return returns the material of the node */
    virtual materialID getMaterial() = 0;

    //! add force to specific point on the node
    /*! if a extension of the force vector does not intersect with the node's center of mass
    (if the force has an arm), torque will be added
        \param force: the force vector
        \param position: the position on the node to add the force */
    virtual void addLocalForce(core::vector3df force, core::vector3df position) = 0;

    //! add force
    /*! add a force to the node, the force will be applied to the center of mass*/
    virtual void addForce(core::vector3df addForce) = 0;

    //! add torque
    /*! add torque to the body, use addLocalForce to add a force the also adds torque */
    virtual void addTorque(core::vector3df addTorque) = 0;

    //! set mass
    /*! set the mass of the node */
    virtual void setMass(f32 newMass) = 0;

    //! get mass
    /*! \return returns the mass of the node */
    virtual f32 getMass() = 0;

    //! set inertia
    /*! set the inertia of the node */
    virtual void setInertia(core::vector3df interia) = 0;

    //! get inertia
    /*! \return returns a vector3df with Ixx,Iyy,Izz */
    virtual core::vector3df getInertia() = 0;

    //! set velocity
    /*! set the velocity of the node */
    virtual void setVelocity(core::vector3df newVel) = 0;

    //! set omega velocity
    /*! set the omega of the node, this will make the node rotate */
    virtual void setOmega(core::vector3df newVel) = 0;

    //! set the continuous collision mode
    /*! set this to true if you want to use continuous collision mode, a least one of the nodes in a material
    interation using continuous collision must have this set to true. The IControllablePhysicsNode and the
    ICameraFPSPhysicsNode has this set to true by default. */
    virtual void setContinuousCollisionMode(bool ccMode) = 0;

    //! get the force array
    /*! \return returns an array of 3 floats */
    virtual f32* getForceArray() = 0;

    //! get the torque array
    /*! \return returns an array of 3 floats */
    virtual f32* getTorqueArray() = 0;

    //! set the name of the node
    /*! \param newName: the name of the node */
    virtual void setName(const c8 *newName) = 0;

    //! get the name of the node
    /*! \return returns the name of the node */
    virtual const c8* getName() = 0;

    //! set user data
    /*! \param ud: any pointer */
    virtual void setUserData(void *ud) = 0;

    //! get user data
    /*! \return returns the user data of the node */
    virtual void* getUserData() = 0;

    //! add an additional force and torque callback
    /*! the passed function is called right after the force and torque callback */
    virtual void addAdditionalForceAndTorqueCallback(void (*forceAndTorque)(IPhysicsNode *node)) = 0;

    //! add an additional transform callback
    /*! the passed function is called right after the transform callback */
    virtual void addAdditionalTransformCallback(void (*transform)(IPhysicsNode *node, core::matrix4* matrix)) = 0;

    //! set the individual gravity
    /*! the individual gravity is added after the global gravity, to cancel out the global gravity for
    a node just set this to the opposit of the global gravity */
    virtual void setGravity(core::vector3df newGravity) = 0;

    //! get the individual gravity
    /*! \return returns the individual gravity vector for the node */
    virtual core::vector3df getGravity() = 0;

    //! set if the node should use the default transform callback
    /*! this is true by default, if it is set to false the default transform callback will not be called.
    The additional transform callback will still be called, if one is specified */
    virtual void setUseDefaultTransformCallback(bool use) = 0;

    //! set if the node should use the default force and torque callback
    /*! this is true by default, if it is set to false the default force and torque callback will not be called.
    The additional force and torque callback will still be called, if one is specified */
    virtual void setUseDefaultForceAndTorqueCallback(bool use) = 0;

    //! get if the node should use the default transform callback
    /*! \return returns if the node uses the default transform callback */
    virtual bool getUseDefaultTransformCallback() = 0;

    //! get if the node should use the default force and torque callback
    /*! \return returns if the node uses the default force and torque callback */
    virtual bool getUseDefaultForceAndTorqueCallback() = 0;

};

}//end namespace tumle

#endif
