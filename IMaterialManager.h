// IMaterialManager.h
//a part of Tumle

#ifndef TUMLE_I_MATERIAL_MANAGER_H
#define TUMLE_I_MATERIAL_MANAGER_H


#include "tumle.h"

using namespace irr;

namespace tumle
{

class IPhysicsManager;
class IMaterialInteraction;


//! managers the material
/*! use this class to create new materials
    \todo add some pre-made materials */
class IMaterialManager
{
public:
    //! constructor
    IMaterialManager(){};

    //! destructor
    virtual ~IMaterialManager(){}

    //! delete the material manager
    /*! this will delete all the material interactions created with it */
    virtual void remove() = 0;

    //! add a material to the material manager
    /*! this creates a newton material. It is not recomended to use this function on the fly. Add all the materials
    you're planning on using in you app before the real action begins.*/
    virtual materialID addMaterial(const c8* name = "noname") = 0;

    //! define a material interaction
    /*! use this to setup the different types of coefficients for a collision between two materials. Add the materials to
    the material manager before setting up the interactions.
         \param material1: one of the materials to interact
         \param material2: the other material.
         \param elasticityCoef: the elasicity coefficient
         \param staticFrictionCoef: the static friction coefficient
         \param kineticFrictionCoef: the kinetic friction coefficent
         \param softnessCoef: the softness coefficient
         \param collidableState: will a collision take place
         \param createMaterialInteractionClass: If you want to change the setting on the fly setting this
         to true will make the fuction return a pointer to a material interaction class that lets you change
         the settings, if set to false 0 is returned.
         \return IMaterialIneraction pointer or 0 */
    virtual IMaterialInteraction* addMaterialInteraction(materialID material1,
                                           materialID material2,
                                           f32 elasticityCoef,
                                           f32 staticFrictionCoef,
                                           f32 kineticFrictionCoef,
                                           f32 softnessCoef,
                                           bool collidableState = true,
                                           bool createMaterialInteractionClass = false) = 0;


    //! get the physics manager that uses the material manager
    /*! \return returns the physics manager */
    virtual IPhysicsManager* getPhysicsManager() = 0;

    //! set a pointer in the CreatedInteraction array to 0
    virtual void setPointerToZero(s32 index) = 0;

    //!get the array of material interaction classes
    virtual core::array<IMaterialInteraction*> &getCreatedInteractions() = 0;

    //! get a material interaction from it's name
    /*! \return returns the last material interaction with the passed name */
    virtual IMaterialInteraction* getMaterialInteractionFromName(const c8 *name) = 0;

    //! get a material from it's name
    /*! \return returns the last material with the passed name */
    virtual materialID getMaterialFromName(const c8 *name) = 0;

};

}//end namespace tumle

#endif
