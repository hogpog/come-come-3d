#include <irrlicht.h>

using namespace irr;
using namespace scene;
using namespace gui;
using namespace video;
using namespace core;
using namespace io;



/** \mainpage

\section intro_sec Introduction

This is BlindSide's HLSL shader pack's documentation. Please have a look through the Effect Manager class
for a list of commands that are available. Each command is explained in full and it is recommend that you have
a thorough look through this guide before using the shader pack.

Basic Usage:
\code
// Initialise the Effect Handler.
effectHandler* effect = new effectHandler(irrlichtDevice,dimension2d<s32>(512,512));

// Make sure every node has a material type set before applying the shadow.
myNode->setMaterialType(EMT_SOLID);

// Apply the shadow to the node (Note this allows it to RECIEVE shadows not cast,
// please see addShadowToNode() for more info).
effect->addShadowToNode(myNode,EFT_4PCF);

// Set material type for terrain. Currently only material type EMT_SOLID, EMT_DETAIL_MAP,
// and EMT_LIGHTMAP can recieve shadows. Anything can cast shadows by using addNodeToDepthPass().
myTerrainNode->setMaterialType(EMT_DETAIL_MAP);

// Allow terrain to recieve shadows, this time with 8 sample pcf filtering.
effect->addShadowToNode(myTerrainNode,EFT_8PCF);

// Our main loop.
while(irrlichtDevice->run())
{
	videoDriver->beginScene(true,true,SColor(0,0,0,0));

	// Make sure to call this before sceneManager->drawAll() before every update!
	effect->update();

	sceneManager->drawAll();
	videoDriver->endScene();
}
\endcode

See you and have fun!

*/


/// Enums for effect types.
enum E_EFFECT_TYPE
{
	EET_GOOCH,
	EET_MRWIGGLE,
	EET_ANISO,
	EET_PHONG,
	EET_BRDF,
	EET_COUNT
};

/** Enums for filter types.
The types are (In respective order):
 - No filtering.
 - 4 Sample PCF filtering.
 - 8 Sample PCF filtering.
 - 12 Sample PCF filtering.

Please note that on EMT_SOLID materials, only 4 Sample PCF filtering is available.
(For the rest you must have EMT_DETAIL_MAP or EMT_LIGHTMAP)
*/
enum E_FILTER_TYPE
{
	EFT_NONE,
	EFT_4PCF,
	EFT_8PCF,
	EFT_12PCF,
	EFT_COUNT
};

/// ShadowNode struct for internal reference keeping of shadow nodes.
typedef struct
{
	ISceneNode* node;
	E_FILTER_TYPE filterType;
	s32 materialType;
	bool depthOnly;
	bool exclude;
} shadowNode;

/// Main Effect Handler class
/**
This is the main Effect Handler class, when initialising it, you must pass the Irrlicht device,
the desired resolution of the shadow map (This must be a power of 2 and also less than your
main device size due to current Irrlicht Direct3D limitations). The last parameter defines
whether you want to use an accurate implementation for the shadows or non-accurate.
Non-accurate may show some artifacts but may also be faster. It is recommended to always use
accurate when possible.
*/
class effectHandler
{
public:
	/// Main Effect Handler class
	effectHandler(IrrlichtDevice* dev, dimension2d<s32> mapSize, bool useAccurate = true);

	/// Sets the light's position.
	void setLightPosition(vector3df position)
	{
		lightCam->setPosition(position);
	};

	/// For shadows, the light is directional/spotlight, so this sets look-at target of the light.
	void setLightTarget(vector3df target)
	{
		lightCam->setTarget(target);
	};

	/// Using this function you can directly alter the light's "camera", such as change the FOV, make it Orthographic, etc...
	ICameraSceneNode* getLightCamera()
	{
		return lightCam;
	};

	/**
	Using this you can add an effect to a scene node. This overides the node's current material and sets it to the effect's shader material.
	Current available effects are:
	Gooch lighting.
	Mr Wiggle.
	Anisotropic highlights.
	Phong per-pixel lighting.
	BRDF.
	*/
	void addEffectToNode(ISceneNode* node, E_EFFECT_TYPE etype);

	/// This returns the shadow map texture, useful for debug purposes.
	ITexture* getShadowMapTexture()
	{
		return ShadowMapTex;
	};

	/// This determines the far clip plane of the light's camera, and also the ratio by which the depth buffer is measured.
	/// Careful manipulation of this variable is neccesary to avoid artifacts and achieve accurate shadows.
	void setMaxShadowDistanceFromLight(int distance);

	/// This adds a node to the depth pass, but does not alter its material.
	/// All nodes that are within a shadowed scene should atleast be added to the depth pass,
	/// so that they do not cause strange effects when shadows are calculated.
	/// An alternative if you do not wish a node to cast a shadow, is to use
	/// excludeNodeFromDepthPass.
	void addNodeToDepthPass(irr::scene::ISceneNode *node);

	/// This excludes the node from the depth pass, so that it does not cast a shadow or interfere
	/// with the depth pass process.
	void excludeNodeFromDepthPass(irr::scene::ISceneNode *node);

	/// This sets the shadow alpha. A value from 0.0f - 1.0f should be passed.
	void setShadowDarkness(float shadalpha);

	/// This removes a node from the shadow interface, returning it to its previous state.
	/// The node will no longer recieve shadows and will default to its original material.
	void removeShadowFromNode(irr::scene::ISceneNode *node);

	/// This must be called in your main loop before "SceneManager->drawAll()"
	void update();

	/// This enables shadow fading. Please note that fading is only supported on materials
	/// of EMT_DETAIL_MAP and EMT_LIGHTMAP. A fade value parameter must be passed
	/// which should be anywhere from 0.0f - 10.0f. If the fading does not appear to your
	/// tastes please tweak this value to calibrate.
	void enableFading(float fadeval = 1.5f);

	/// This disables fading if fading has been enabled.
	void disableFading();

	/**
	This allows a node to recieve shadows. The implementation of this technique is the exact opposite
	of Stencil Shadows. You must specify which nodes RECIEVE shadows rather than casts. So this
	function does not only cause a node to cast shadows but recieve them instead. The filtering mode
	is valid for the specified node that is recieving the shadows, and not any nodes that it casts the
	shadows on. Therefore, if you want sydney to cast a terrain shadow with 8 sample filtering
	you will use:

	\code
	EffectManager->addShadowToNode(terrainNode,EFT_8PCF);
	EffectManager->addNodeToDepthPass(sydney);
	\endcode

	This will add sydney to the depth pass so that the shader tests whether sydney is intersecting
	the area between the light and the terrain, and will decide whether to light or not light pixels
	on the terrain depending on whether sydney is blocking the view or not. That is the basic
	theory of shadow maps and why you must decide which node recieves shadows rather than
	casts.

	All nodes within a shadowed scene must have a shadow added to them using addShadowToNode,
	added to the depth pass using addNodeToDepthPass or excluded from the depth pass using
	excludeNodeFromDepthPass.

	Also note that if you wanted sydney to cast shadows on herself, you can use:
	\code
	EffectManager->addShadowToNode(sydney,EFT_NONE);
	\endcode

	Instead of:
	\code
	EffectManager->addNodeToDepthPass(sydney);
	\endcode

	So that sydney will cast shadows upon herself without filtering.

	Please note that addShadowToNode already adds a node to the depth pass so theres no need
	to add it again.
	*/
	void addShadowToNode(ISceneNode* node, E_FILTER_TYPE filterType = EFT_NONE);

private:
	IrrlichtDevice* device;
	dimension2d<s32> mapRes;
	ICameraSceneNode* lightCam;
	ICameraSceneNode* cameraBuffer;
	core::array<shadowNode> shadowNodeArray;
	s32 Depth;
	s32 Detail;
	s32 DetailPCF4;
	s32 DetailPCF8;
	s32 DetailPCF12;
	s32 DetailStore;
	s32 DetailPCF4Store;
	s32 DetailPCF8Store;
	s32 DetailPCF12Store;
	s32 Solid;
	s32 SolidPCF4;
	s32 Gooch;
	s32 Phong;
	s32 Brdf;
	s32 Aniso;
	s32 Wiggle;
	s32 DepthWiggle;
	ITexture* ShadowMapTex;
	ITexture* ShadowMapBuffer;
};

// This code is covered by the LGPL, please find enclosed "License.html"
// Copyright (C) Ahmed Hilali 2007
