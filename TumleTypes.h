// EBodyType.h
//a part of Tumle

#ifndef TUMLE_TUMLE_TYPES_H
#define TUMLE_TUMLE_TYPES_H

namespace tumle
{

//! 32 bit signed variable.
/*! this typedef is in Tumle because i think it makes the materials more intuitive to create.
    if you don't like it, just use an int or a s32 */
typedef signed int materialID;

}//end namespace tumle

#endif
