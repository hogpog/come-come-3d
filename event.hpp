#ifndef _EVENT_HPP_
#define _EVENT_HPP_

#include <irrlicht.h>

using namespace irr;

class CEvent : public IEventReceiver
{
private:
    int *dir;
public:
    scene::ISceneNode* node;
    bool endGame;
    bool ESC;
    CEvent()
    {
        endGame = false;
    };
    ~CEvent(){};
    void getPlayerNode(scene::ISceneNode* scenenode)
    {
        node = scenenode;
    };
    void getPlayerDir(int *direcao)
    {
        dir = direcao;
    }
    virtual bool OnEvent(const SEvent& event)
    {
        if (event.KeyInput.Key == KEY_KEY_A && event.KeyInput.PressedDown)
        {
            *dir = 2;
            return true;
        }
        if (event.KeyInput.Key == KEY_KEY_D && event.KeyInput.PressedDown)
        {
            *dir = 0;
            return true;
        }
        if (event.KeyInput.Key == KEY_KEY_W && event.KeyInput.PressedDown)
        {
            *dir = 3;
            return true;
        }
        if (event.KeyInput.Key == KEY_KEY_S && event.KeyInput.PressedDown)
        {
            *dir = 1;
            return true;
        }
        if (event.KeyInput.Key == KEY_RETURN && event.KeyInput.PressedDown)
        {
            endGame = true;
        }
        if (event.KeyInput.Key == KEY_ESCAPE && event.KeyInput.PressedDown)
        {
            ESC = true;
        }
        return false;
    };
};

#endif
