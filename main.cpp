#include <irrlicht.h>
#include <iostream>
#include <vector>
#include "grafico.hpp"
#include "map.hpp"
#include "ia.hpp"
#include "es\es.hpp"
#include "cfg\carregavalor.hpp"
#define NUMERODEFASE 2

int main()
{
    CMapa *map;
    CIA * npc1;
    CIA * npc2;
    CIA * npc3;
    CIA * npc4;
    CGrafico *comecome;

    int lastFPS = -1;
    int fase = 0;
    bool loadedfase[NUMERODEFASE];
    loadedfase[0] = false;
    loadedfase[1] = false;

    if (!loadedfase[fase])
    {
        map = new CMapa();
        map->CarregaMapa("../../media/mapa1.ini");
        map->CarregabMapa("../../media/bmapa1.ini");
        comecome = new CGrafico();
        comecome->Initialize();
        comecome->LoadResources();

        npc1 = new CIA(map->initPosX,map->initPosY);
        npc2 = new CIA(map->initPosX,map->initPosY);
        npc3 = new CIA(map->initPosX,map->initPosY);
        npc4 = new CIA(map->initPosX,map->initPosY);

        comecome->addNpc(&npc1->posx,&npc1->posy);
        comecome->addNpc(&npc2->posx,&npc2->posy);
        comecome->addNpc(&npc3->posx,&npc3->posy);
        comecome->addNpc(&npc4->posx,&npc4->posy);

        comecome->CreateMap(map->mapa);
        comecome->CreatebMap(map->bmapa);

        npc1->getMap(map->mapa);
        npc2->getMap(map->mapa);
        npc3->getMap(map->mapa);
        npc4->getMap(map->mapa);


        comecome->Run();
        loadedfase[0] = true;
    }
    npc1->getPlayerPos(&comecome->playerPosX,&comecome->playerPosY);
    npc2->getPlayerPos(&comecome->playerPosX,&comecome->playerPosY);
    npc3->getPlayerPos(&comecome->playerPosX,&comecome->playerPosY);
    npc4->getPlayerPos(&comecome->playerPosX,&comecome->playerPosY);
    while (comecome->device->run())
    {
        if (fase==0)
        {
            if (comecome->bolinhas>=map->totalbolinhas)
            {
                //fase++;
                break;
            }

            //printf("teste: %i, %i\n",comecome->playerPosX,comecome->playerPosY);
            comecome->ProcessEffects();
            if (comecome->UpdatePos())
            {
                npc1->move(3);
                npc2->move(2);
                npc3->move(1);
                npc4->move(1);
            }

            int fps = comecome->driver->getFPS();

            if (lastFPS != fps)
            {
                core::stringw tmp(L"Come Come v. 0.0.1 [");
                tmp += comecome->driver->getName();
                tmp += L"] fps: ";
                tmp += fps;
                tmp += L" Pontos: ";
                tmp += comecome->ponto;

                comecome->device->setWindowCaption(tmp.c_str());
                lastFPS = fps;
            }
        }
        /*if (fase==1)
        {
            if (!loadedfase[fase])
            {
                comecome->device->drop();

                delete map;
                delete comecome;
                delete npc1;
                delete npc2;
                map = new CMapa();
                map->CarregaMapa("../../media/mapa2.ini");
                map->CarregabMapa("../../media/bmapa2.ini");
                comecome = new CGrafico();
                comecome->Initialize();
                comecome->LoadResources();

                npc1 = new CIA(map->initPosX,map->initPosY);
                npc2 = new CIA(map->initPosX,map->initPosY);
                printf("X:%d,Y:%d\n",map->initPosX,map->initPosY);
                comecome->addNpc(&npc1->posx,&npc1->posy);
                comecome->addNpc(&npc2->posx,&npc2->posy);
                comecome->CreateMap(map->mapa);
                comecome->CreatebMap(map->bmapa);
                npc1->getMap(map->mapa);
                npc2->getMap(map->mapa);


                comecome->Run();
                loadedfase[1] = true;
            }
            npc1->getPlayerPos(&comecome->playerPosX,&comecome->playerPosY);
            //printf("teste: %i, %i\n",comecome->playerPosX,comecome->playerPosY);
            comecome->ProcessEffects();
            if (comecome->UpdatePos())
            {
                npc1->move(3);
                npc2->move(2);
            }

            int fps = comecome->driver->getFPS();

            if (lastFPS != fps)
            {
                core::stringw tmp(L"Come Come v. 0.0.1 [");
                tmp += comecome->driver->getName();
                tmp += L"] fps: ";
                tmp += fps;
                tmp += L" Pontos: ";
                tmp += comecome->ponto;

                comecome->device->setWindowCaption(tmp.c_str());
                lastFPS = fps;
            }
        }*/
    }

    comecome->device->drop();

    delete map;
    delete comecome;
    delete npc1;
    delete npc2;
    delete npc3;
    delete npc4;

    return 0;
}

