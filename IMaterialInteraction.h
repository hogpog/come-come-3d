// IMaterialInteraction.h
//a part of Tumle

#ifndef TUMLE_I_MATERIAL_INTERACTION_H
#define TUMLE_I_MATERIAL_INTERACTION_H


#include "tumle.h"

using namespace irr;

namespace tumle
{

class IPhysicsNode;
class IMaterialManager;
class IPhysicsManager;

//! this class controls what happens when to physics nodes collide
class IMaterialInteraction
{
public:

    //! constructor
    IMaterialInteraction(){};

    //! destructor
    virtual ~IMaterialInteraction(){};

    //! delete the material interaction
    virtual void remove() = 0;

    //! set the elasicity coefficient for the collision.
    /*! From the newton docs:
        \param elasticityCoef: must be a positive value.
    It is recommended that elasticCoef be set to a value lower or equal to 1.0 */
    virtual void setElasticity(f32 elasticityCoef) = 0;

    //! set the friction coefficients for the collision.
    /*! From the newton docs: <br>
    staticFriction and kineticFriction must be positive values. kineticFriction must be lower than staticFriction.
    It is recommended that staticFriction and kineticFriction be set to a value lower or equal to 1.0,
    however because some synthetic materials can have higher than one coeficient of friction
    Newton allows for the coeficient of friction to be as high as 2.0.
        \param staticFriction: static friction coefients.
        \param kineticFriction: dynamic coefficient of friction. */
    virtual void setFriction(f32 staticFriction, f32 kineticFriction) = 0;

    //! set the softness coefficients for the collision.
    /*! From the newton docs: <br>
    softnessCoef must be a positive value. It is recommended that softnessCoef be set to value lower or equal
    to 1.0 A low value for softnessCoef will make the material soft. A typical value for softnessCoef is 0.15
        \param softnessCoef: softness coefficient */
    virtual void setSoftness(f32 softnessCoef) = 0;

    //! set if a collision will take place
    /*! \param state: true = collidable; false = non collidable */
    virtual void setCollidable(bool state) = 0;

    //! get the elasicity coefficient for the collision.
    virtual f32 getElasticity() = 0;

    //! get the static friction coefficient for the collision.
    virtual f32 getStaticFriction() = 0;

    //! get the kinetic friction coefficient for the collision.
    virtual f32 getKineticFriction() = 0;

    //! get the softness coefficients for the collision.
    virtual f32 getSoftness() = 0;

    //! get if a collision will take place
    virtual bool getCollidable() = 0;

    //! get the position of a collision between the two materials
    /*! use this in the collision callbacks */
    virtual core::vector3df getCollisionPoint() = 0;

    //! get the normal vector of a collision between the two materials
    /*! use this in the collision callbacks */
    virtual core::vector3df getCollisionNormal() = 0;

    //! get the collision normal speed
    /*! use this in the collision callbacks. This returns 0 in the begin callback */
    virtual f32 getCollisionNormalSpeed() = 0;

    //! get the collision sliding speed
    /*! use this %in the collision callbacks.  This returns 0 in the begin callback  */
    virtual f32 getCollisionSlidingSpeed() = 0;

    //! set the ContinuousCollisionMode of the material
    /*! this can be used to prevent heigh speed nodes to move trough other nodes */
    virtual void setContinuousCollisionMode(bool ccm) = 0;

    //! get the ContinuousCollisionMode of the material
    virtual bool getContinuousCollisionMode() = 0;

    //! the the colliding node one or two
    /*! use this in the collision callbacks */
    virtual IPhysicsNode *getCollisionNodeOne() = 0;

    //! the the colliding node one or two
    /*! use this in the collision callbacks */
    virtual IPhysicsNode *getCollisionNodeTwo() = 0;

    //! set the functions to call for a collision between the two materials
    /*! this can be used to play sounds or make sparks(paricles) ect.
    the functions must be of the type: 'myFunction(IMaterialInteraction *someName)'
        \param beginCall: the function to call when a collision begins
        \param processCall: the function to call during a collision.
        \param endCall: the function to call when a collision ends */
    virtual void setCollisionCallback(void (*beginCall)(IMaterialInteraction *object),
                                      void (*processCall)(IMaterialInteraction *object),
                                      void (*endCall)(IMaterialInteraction *object)) = 0;

    //! get the material manager
    /*! \return returns the material manager that was used to create this interaction */
    virtual IMaterialManager *getMaterialManager() = 0;

    //!get the physics manager
    /*! \return returns the physics manager */
    virtual IPhysicsManager *getPhysicsManager() = 0;

    //! get the irrlicht device
    /*! \return returns the irrlicht device that was passed to the physics manager on creation */
    virtual IrrlichtDevice *getIrrlichtDevice() = 0;

    //! get the newton world
    /*! \return returns the newton world that was passed to the physics manager on creation */
    virtual NewtonWorld *getNewtonWorld() = 0;

    //! set user data
    /*! this can be used to add an arbitrary pointer to the interaction
    I added this because the material callback must be static if they are members
    of a class, and in such a situation user data can be usefull */
    virtual void setUserData(void *ud) = 0;

    //! get user data
    /*! \return retuns a pointer to the added user data */
    virtual void *getUserData() = 0;

    //! disable the controllable node from moveing fourther in to the other node
    /*! This is done be disableing movement of the controllable node in the direction
    of the other node when a collision happens, this means the controllable node will not
    'try' to move trough the other node nor to push it. (if it worked, the problem is that it disables
    the movement when the collision HAS happend not before it happens) <br>
    Use this function in the process callback for it to work properly. */
    virtual void disableMoveThrough() = 0;

    //! set the name of the node
    /*! this is usefull if you want to use the interaction later.
    Use IMaterialManager::getMaterialInteractionFromName() to get it */
    virtual void setName(const c8 *newName) = 0;

    //! get the name of the node
    /*! \return returns the name of the material interaction */
    virtual const c8* getName() = 0;
};

} // end namespace tumle

#endif


