#ifndef _MAP_HPP_
#define _MAP_HPP_
#define MAXX 20
#define MAXY 20

#include <vector>
class CMapa
{
public:
    int initPosX,initPosY;
    CMapa();
    void PrintMap();
    void addNpc(int *posx,int *posy);
    void CarregaMapa(std::string str);
    void CarregabMapa(std::string str);
    ~CMapa(){};
    int mapa[MAXX][MAXY];
    int bmapa[MAXX][MAXY];
    int totalbolinhas;
    std::vector<int*> npcsx,npcsy;
private:

};

#endif
