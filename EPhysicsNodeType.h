// EPhysicsNodeType.h
//a part of Tumle

#ifndef TUMLE_E_PHYSICS_NODE_TYPE_H
#define TUMLE_E_PHYSICS_NODE_TYPE_H

namespace tumle
{
//! enum for the different kind of physics nodes
enum E_PHYSICS_NODE_TYPE
{
    //! the standard node
    EPNT_STANDARD,
    //! controllable node
    EPNT_CONTROLLABLE,
    //! camera node
    EPNT_CAMERA
};

}//end namespace tumle

#endif
