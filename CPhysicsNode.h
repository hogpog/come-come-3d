// CPhysicsNode.h
// a part of Tumle

#ifndef TUMLE_C_PHYSICS_NODE_H
#define TUMLE_C_PHYSICS_NODE_H

#include "tumle.h"

using namespace irr;

namespace tumle
{

class CPhysicsNode : public IPhysicsNode
{
public:

    //! constructor
    CPhysicsNode(IPhysicsManager *pmgr,
                s32 managerIndex,
                scene::ISceneNode *node,
                NewtonBody *body,
                E_BODY_TYPE type,
                f32 mass);

    //! destructor
    /*! \todo figure out why the mass has to be set to zero before deleting
             must have something to do with the transform and forceAndTorque callbacks
             Just use remove() and the mass will be set to zero before deletion */
    virtual ~CPhysicsNode();

    //! get the irrlicht scene node of the physics node
    virtual scene::ISceneNode* getIrrlichtNode(){ return Node; };

    //! get the newton body of the physics node
    virtual NewtonBody* getNewtonBody(){ return Body; };

    //! set the position of the physics node
    /*! this moves both the irrlicht scene node and the newton body */
    virtual void setPosition(core::vector3df newPos);

    //! set the rotation of the physics node
    /*! this rotates both the irrlicht scene node and the newton body */
    virtual void setRotation(core::vector3df newRot);

    //! get the position of the physics scene node
    /*! this gets the position of the irrlicht scene node */
    virtual core::vector3df getPosition(){ return Node->getPosition(); };

    //! get the rotation of the physics scene node
    /*! this gets the rotation of the irrlicht scene node */
    virtual core::vector3df getRotation(){ return Node->getRotation(); };

    //! delete the physics node
    /*! this deletes both the irrlicht scene node and the newton body, and
    removes the pointer to the physics node from the physics manager */
    virtual void remove();

    //! get the body type of the physics node
    virtual E_BODY_TYPE getBodyType(){ return BodyType; };

    //! get the physics node type
    virtual E_PHYSICS_NODE_TYPE getType(){ return Type; };

    //! get the physics manager that created the node
    virtual IPhysicsManager* getPhysicsManager(){ return PhysicsManager; };

    //! set the material
    virtual void setMaterial(materialID material);

    //! get the material of the physics node
    virtual materialID getMaterial(){ return NewtonBodyGetMaterialGroupID(Body); };

    //! add force to specific point on the node
    /*! if a extension of the force vector does not intersect with the node's center of mass
    (if the force has an arm), torque will be added */
    virtual void addLocalForce(core::vector3df force, core::vector3df position);

    //! add a force to the force array
    virtual void addForce(core::vector3df addForce){ force[0] += addForce.X;
                                                    force[1] += addForce.Y;
                                                    force[2] += addForce.Z; };

    //! add a force to the force array
    virtual void addTorque(core::vector3df addTorque){ torque[0] += addTorque.X;
                                                      torque[1] += addTorque.Y;
                                                      torque[2] += addTorque.Z; };
    //! set mass
    virtual void setMass(f32 newMass){f32 oldMass, Ixx, Iyy, Izz;
                                     NewtonBodyGetMassMatrix(Body, &oldMass, &Ixx, &Iyy, &Izz);
                                     NewtonBodySetMassMatrix(Body, newMass, Ixx, Iyy, Izz);
                                     Mass = newMass; } ;
    //! get mass
    virtual f32 getMass() { return Mass; };

    //! set inertia
    virtual void setInertia(core::vector3df inertia){ NewtonBodySetMassMatrix(Body, getMass(), inertia.X,
                                                                                              inertia.Y,
                                                                                              inertia.Z); };

    //! get inertia
    virtual core::vector3df getInertia() {f32 mass, Ixx, Iyy, Izz;
                                         NewtonBodyGetMassMatrix(Body, &mass, &Ixx, &Iyy, &Izz);
                                         return core::vector3df(Ixx,Iyy,Izz); };

    //! set velocity
    virtual void setVelocity(core::vector3df newVel) { f32 vel[3] = {newVel.X, newVel.Y, newVel.Z};
                                                      NewtonBodySetVelocity(Body, vel); };

    //! set omega velocity
    virtual void setOmega(core::vector3df newVel) { f32 vel[3] = {newVel.X, newVel.Y, newVel.Z};
                                                   NewtonBodySetOmega(Body, vel); };

    //! set the continuous collision mode
    virtual void setContinuousCollisionMode(bool ccMode)
    {
       NewtonBodySetContinuousCollisionMode(Body, ccMode);
    }

    //! get the force array
    virtual f32* getForceArray(){ return force; };

    //! get the torque array
    virtual f32* getTorqueArray(){ return torque; };

    //! set the name of the node
    virtual void setName(const c8 *newName){ Name = newName; };

    //! get the name of the node
    virtual const c8* getName(){ return Name.c_str(); };

    //! set user data
    virtual void setUserData(void *ud) { UserData = ud; }

    //! get user data
    virtual void* getUserData() { return UserData; };

    //! pointer to the additional force and torque callback function
    void (*additionalForceAndTorqueCallback)(IPhysicsNode *node);

    //! pointer to the additional transform callbakc
    void (*additionalTransformCallback)(IPhysicsNode *node, core::matrix4* matrix);

    //! add an additional force and torque callback
    virtual void addAdditionalForceAndTorqueCallback(void (*forceAndTorque)(IPhysicsNode *node))
    {
       additionalForceAndTorqueCallback = forceAndTorque;
    };

    //! add an additional transform callback
    virtual void addAdditionalTransformCallback(void (*transform)(IPhysicsNode *node, core::matrix4* matrix))
    {
       additionalTransformCallback = transform;
    };

    //! set the individual gravity
    virtual void setGravity(core::vector3df newGravity) { Gravity = newGravity; };

    //! get the individual gravity
    virtual core::vector3df getGravity() { return Gravity; };

    //! set if the node should use the default transform callback
    virtual void setUseDefaultTransformCallback(bool use) { useDefaultTransformCallback = use; }

    //!  set if the node should use the default force and torque callback
    virtual void setUseDefaultForceAndTorqueCallback(bool use) { useDefaultForceAndTorqueCallback = use; }

    //! get if the node should use the default transform callback
    virtual bool getUseDefaultTransformCallback() { return useDefaultTransformCallback; }

    //! get if the node should use the default force and torque callback
    virtual bool getUseDefaultForceAndTorqueCallback() { return useDefaultForceAndTorqueCallback; }

protected:
    //! a pointer to the physics manager that created the physics node
    IPhysicsManager *PhysicsManager;
    //! index of the array in the physics manager
    u32 Index;
    //! the irrlicht node of the physics node
    scene::ISceneNode *Node;
    //! the newton body of the physics node
    NewtonBody *Body;
    //! the force array
    f32 force[3];
    //! the torque array
    f32 torque[3];
    //! the body type if the physics node
    const E_BODY_TYPE BodyType;
    //! the physics node type
    const E_PHYSICS_NODE_TYPE Type;
    //! the mass of the body
    f32 Mass;
    //! the material
    materialID Material;
    //! the name of the node
    core::stringc Name;
    //! user data
    void *UserData;
    //! individual gravity
    core::vector3df Gravity;
    //! use the default transform callback
    bool useDefaultTransformCallback;
    //! use the default force and torque callback
    bool useDefaultForceAndTorqueCallback;

};


}//end namespace tumle

#endif
