#include "map.hpp"
#include "cfg\carregavalor.hpp"
#include "es\es.hpp"

CMapa::CMapa()
{
    initPosX = 0;
    initPosY = 0;
    totalbolinhas=0;
}
void CMapa::CarregaMapa(std::string str)
{
    CES *Arquivo = new CES();
    Arquivo->AbreArquivo(str,"E");
    CCarregaValor *Valor = new CCarregaValor();
    int X=0,Y=0;
    for (int a = 0; a < Arquivo->BufferSize(); a++)
    {
        for (int b = 0; b < Arquivo->Buffer(a).size(); b++)
        {
            if (Arquivo->Buffer(a)[b] != ',')
            {
                mapa[Y][X] = atoi(&Arquivo->Buffer(a)[b]);
                if (mapa[Y][X]==2)
                {
                    initPosX = X;
                    initPosY = Y;
                }
            }
            else
            {
                X++;
            }
        }
        X=0;
        Y++;
    }
}

void CMapa::CarregabMapa(std::string str)
{
    CES *Arquivo = new CES();
    Arquivo->AbreArquivo(str,"E");
    CCarregaValor *Valor = new CCarregaValor();
    int X=0,Y=0;
    for (int a = 0; a < Arquivo->BufferSize(); a++)
    {
        for (int b = 0; b < Arquivo->Buffer(a).size(); b++)
        {
            if (Arquivo->Buffer(a)[b] != ',')
            {
                bmapa[Y][X] = atoi(&Arquivo->Buffer(a)[b]);
                if (bmapa[Y][X]!=0)
                    totalbolinhas++;
            }
            else
            {
                X++;
            }
        }
        X=0;
        Y++;
    }
}

void CMapa::PrintMap()
{
    for (int npc = 0; npc<npcsx.size();npc++)
    {
        mapa[*npcsx[npc]][*npcsy[npc]] = 2;
    }
    for (int x = 0;x<MAXX ;x++ )
    {
        for (int y = 0;y<MAXY ;y++ )
        {
            if (mapa[x][y]==1)
                printf("xxx");
            else if (mapa[x][y]==2)
                printf(" 0 ",mapa[x][y]);
            else
                printf("   ");
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
    for (int npc = 0; npc<npcsx.size();npc++)
    {
        mapa[*npcsx[npc]][*npcsy[npc]] = 0;
    }
}

void CMapa::addNpc(int *posx,int *posy)
{
    npcsx.push_back(posx);
    npcsy.push_back(posy);
    for (int npc = 0; npc<npcsx.size();npc++)
    {
        mapa[*npcsx[npc]][*npcsy[npc]] = 2;
    }
}
