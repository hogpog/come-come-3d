#ifndef _IA_HPP
#define _IA_HPP
#define MAXX 20
#define MAXY 20
#include <cstdlib>
#include <cstdio>
#include <time.h>
#include <vector>
#include <deque>

using namespace std;

class CIA
{
public:
    CIA(int iniciox,int inicioy);
    void getPlayerPos(int *x,int *y);
    void move(int dificuldade);
    void verCol();;
    void getMap(int map[MAXX][MAXY]);
    ~CIA(){};
    int posx,posy;
    int *jposx,*jposy;
protected:

private:

    /** A Estrela **/
    /****** Lista Aberta *********/
    deque <int> Lista_AbertaX,
                Lista_AbertaY,
                Lista_AbertaH,
                Lista_AbertaG,
                Lista_AbertaF,
                Lista_AbertaPx,
                Lista_AbertaPy;
    /****** Fim Lista Aberta *****/

    /****** Lista Fechada *********/
    deque <int> Lista_FechadaX,
                Lista_FechadaY,
                Lista_FechadaH,
                Lista_FechadaG,
                Lista_FechadaF,
                Lista_FechadaPx,
                Lista_FechadaPy;
    /****** Fim Lista Fechada *****/

    deque <int> CaminhoX,
                CaminhoY;

    bool Caminho_Encontrado;

    int H,
        G;
    /** Fim A Estrela **/

    int direcao;
    int lastdirecao;
    bool dirCol[4];
    int lastNumCol;
    int numCol;
    int mapa[MAXX][MAXY];
};

#endif
