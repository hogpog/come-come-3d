// CMaterialManager.cpp
// a part of Tumle

#include "CMaterialManager.h"
#include "CMaterialInteraction.h"

namespace tumle
{

//! constructor
CMaterialManager::CMaterialManager(IPhysicsManager *pmgr):
PhysicsManager(pmgr), MaterialInteractionCount(0)
{
    nWorld = PhysicsManager->getNewtonWorld();

    //get the default material ID
	materialID defaultID = NewtonMaterialGetDefaultGroupID (nWorld);

	// set default material properties
	NewtonMaterialSetDefaultSoftness (nWorld, defaultID, defaultID, 0.05f);
	NewtonMaterialSetDefaultElasticity (nWorld, defaultID, defaultID, 0.4f);
	NewtonMaterialSetDefaultCollidable (nWorld, defaultID, defaultID, 1);
	NewtonMaterialSetDefaultFriction (nWorld, defaultID, defaultID, 1.0f, 0.5f);
}

//! destructor
CMaterialManager::~CMaterialManager()
{

}

void CMaterialManager::remove()
{
    // delete all the created material interactions
    for(u32 i = 0; i < CreatedInteractions.size() ; i++)
    {
        if(CreatedInteractions[i])
            CreatedInteractions[i]->remove();
    }

    // delete all materials
    for(u32 i = 0; i < CreatedMaterials.size(); i++)
    {
        if(CreatedMaterials[i])
            delete CreatedMaterials[i];
    }

    delete this;
}

//! add a material to the material manager
materialID CMaterialManager::addMaterial(const c8* name)
{
    //create the material
    Material *tmp = new Material(NewtonMaterialCreateGroupID(nWorld), name);

    //put it in the array
    CreatedMaterials.push_back(tmp);

    //return the id
    return tmp->Id;;
}

//! define a material interaction
IMaterialInteraction* CMaterialManager::addMaterialInteraction(materialID material1,
                                                               materialID material2,
                                                               f32 elasticityCoef,
                                                               f32 staticFrictionCoef,
                                                               f32 kineticFrictionCoef,
                                                               f32 softnessCoef,
                                                               bool collidableState,
                                                               bool createMaterialInteractionClass)
{
    //setup the materials
    NewtonMaterialSetDefaultElasticity(nWorld, material1, material2, elasticityCoef);
    NewtonMaterialSetDefaultFriction(nWorld, material1, material2, staticFrictionCoef, kineticFrictionCoef);
    NewtonMaterialSetDefaultSoftness(nWorld, material1, material2, softnessCoef);
    NewtonMaterialSetDefaultCollidable(nWorld, material1, material2, collidableState);

    IMaterialInteraction* mi = 0;

    //if a material interaction class is requested
    if(createMaterialInteractionClass)
    {


        //create the class
        mi = new CMaterialInteraction(this,
                                      MaterialInteractionCount++,
                                      material1,
                                      material2,
                                      elasticityCoef,
                                      staticFrictionCoef,
                                      kineticFrictionCoef,
                                      softnessCoef,
                                      collidableState);


        //put the created class in the array
        CreatedInteractions.push_back(mi);
    }

    return mi;
}

//! get a material interaction from it's name
IMaterialInteraction* CMaterialManager::getMaterialInteractionFromName(const c8 *name)
{
    IMaterialInteraction *mi = 0;
    for(u32 i = 0; i < CreatedInteractions.size(); i++)
    {
        if(!(strcmp(CreatedInteractions[i]->getName(),name)))
        {
            mi = CreatedInteractions[i];
        }
    }

    return mi;
}

//! get a material from it's name
materialID CMaterialManager::getMaterialFromName(const c8 *name)
{
    materialID mat = 0;
    for(u32 i = 0; i < CreatedMaterials.size(); i++)
    {
        if(!(strcmp(CreatedMaterials[i]->Name.c_str(),name)))
            mat = CreatedMaterials[i]->Id;
    }

    return mat;
}

}//end namespace tumle
