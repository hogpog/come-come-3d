/**
* ==========================================================
* Projeto Arret 0.0.9a
* http:\\www.projetoarret.com.br
* ==========================================================
* Desenvolvido por: Alexandro Trevisan
                    Fernando Tonon de Rossi
* Data cria��o: 23/05/2007, 23:40
* Data atualiza��o: 25/05/2007, 00:12
* Atualizado por: Alexandro Trevisan
**/

#ifndef _carregavalor_hpp_
#define _carregavalor_hpp_

#include <string>
/**
* ==========================================================
* Carrega um valor de uma string
* ==========================================================
**/
class CCarregaValor
{
    public:
/**
* ==========================================================
* Carrega um valor (STRING) de uma string
* ==========================================================
**/
      void CarregaValor(std::string& valor,std::string tipo, std::string& temp);
/**
* ==========================================================
* Carrega um valor (INT) de uma string
* ==========================================================
**/
      void CarregaValor(int& valor,std::string tipo, std::string& temp);
/**
* ==========================================================
* Carrega um valor (FLOAT) de uma string
* ==========================================================
**/
      void CarregaValor(float& valor,std::string tipo, std::string& temp);
/**
* ==========================================================
* Carrega um valor (STRING) de uma string sem passar o tipo
* ==========================================================
**/
      void CarregaValor(std::string& valor, std::string& temp);

/**
* ==========================================================
* Carrega um valor (INT) de uma string sem passar o tipo
* ==========================================================
**/
      void CarregaValor(int& valor, std::string& temp);
/**
* ==========================================================
* Carrega um valor (FLOAT) de uma string sem passar tipo
* ==========================================================
**/
      void CarregaValor(float& valor, std::string& temp);
};

#endif
