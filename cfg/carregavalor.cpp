/**
* ==========================================================
* Projeto Arret 0.0.9a
* http:\\www.projetoarret.com.br
* ==========================================================
* Desenvolvido por: Alexandro Trevisan
                    Fernando Tonon de Rossi
* Data cria��o: 23/05/2007, 23:40
* Data atualiza��o: 25/05/2007, 00:12
* Atualizado por: Alexandro Trevisan
**/

#include "carregavalor.hpp"

void CCarregaValor::CarregaValor(std::string& valor,std::string tipo, std::string& temp)
{
    sscanf(temp.c_str(), "%s = %s", tipo.c_str(), valor.c_str());
}

void CCarregaValor::CarregaValor(int& valor,std::string tipo, std::string& temp)
{
    sscanf(temp.c_str(), "%s = %d", tipo.c_str(), &valor);
}

void CCarregaValor::CarregaValor(float& valor,std::string tipo, std::string& temp)
{
    sscanf(temp.c_str(), "%s = %f", tipo.c_str(), &valor);
}

void CCarregaValor::CarregaValor(std::string& valor, std::string& temp)
{
    sscanf(temp.c_str(), "%*s = %s", valor.c_str());
}

void CCarregaValor::CarregaValor(int& valor, std::string& temp)
{
    sscanf(temp.c_str(), "%*s = %d", &valor);
}

void CCarregaValor::CarregaValor(float& valor, std::string& temp)
{
    sscanf(temp.c_str(), "%*s = %f", &valor);
}
