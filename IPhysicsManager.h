// IPhysicsManager.h
// a part of Tumle

#ifndef TUMLE_I_PHYSICS_MANAGER_H
#define TUMLE_I_PHYSICS_MANAGER_H


#include "tumle.h"


using namespace irr;

namespace tumle
{

class IPhysicsNode;
class IMaterialManager;

//! the physics manager manages the physics nodes
class IPhysicsManager
{
public:

       //! destructor
       virtual ~IPhysicsManager(){};

       //! delete the physics manager
       /*! this will delete everyting it has created*/
       virtual void remove() = 0;

       //! add a physics node using a irrlicht node
       /*! this function is used for bodies of the type EBT_BOX and EBT_SPHERE (vector3df is used for size)
           \param node: the irrlicht node to represent the physics node
           \param type: the body type
           \param mass: the mass of the body
           \param size: the size of the body
           \return Returns a pointer to the created physics node */
       virtual IPhysicsNode* addPhysicsNode(scene::ISceneNode *node,
                                            E_BODY_TYPE type,
                                            f32 mass,
                                            core::vector3df size) = 0;

       //! add a physics node using a irrlicht node
       /*! this function is used for bodies of the type EBT_CONE, EBT_CAPSULE, EBT_CYLINDER and
       EBT_CHAMFER_CYLINDER (vector2df is used for size)
           \param node: the irrlicht node to represent the physics node
           \param type: the body type
           \param mass: the mass of the body
           \param size: the size of the body
           \param rotation: the rotaion of the node (this is here because the newton collision isn't always
           rotatet right. Use showCollision() to see if the body is rotated right.
           \return Returns a pointer to the created physics node */
       virtual IPhysicsNode* addPhysicsNode(scene::ISceneNode *node,
                                            E_BODY_TYPE type,
                                            f32 mass,
                                            core::vector2df size,
                                            core::vector3df rotation = core::vector3df(0,0,0)) = 0;

       //! add a physics node using a irrlicht node
       /*! this function is used for bodies of the type EBT_BOUNDING_BOX, EBT_MESH and EBT_EXTERN_MESH (no size is used)
       Thanks to vitec <a href = "http://irrlicht.sourceforge.net/phpBB2/viewtopic.php?t=14720">his post</a>
       on the irrlicht forum!
           \param node: the irrlicht node to represent the physics node
           \param type: the body type
           \param mass: the mass of the body
           \param mesh: if you want to create the collision of the node from another mesh, pass it and use EBT_EXTERN_MESH.
           \return Returns a pointer to the created physics node */
       virtual IPhysicsNode* addPhysicsNode(scene::ISceneNode *node,
                                            E_BODY_TYPE type,
                                            f32 mass,
                                            scene::IMesh *mesh = 0) = 0;

       //! add a controllable physics node using a irrlicht node
       /*! this function is used for bodies of the type EBT_BOX and EBT_SPHERE (vector3df is used for size)
           \param node: the irrlicht node to represent the physics node
           \param type: the body type
           \param mass: the mass of the body
           \param size: the size of the body
           \param keyMapArray: the keys to control the camera
           \param keyMapSize: the size of the SKeyMap
           \param moveSpeed: the move speed of the camera
           \param rotateSpeed: the rotate speed of the camera
           \param fixedUpVector: does the node always stand upright
           \param strafe: does the node strafe
           \return Returns a pointer to the created physics node */
       virtual IControllablePhysicsNode* addControllablePhysicsNode(scene::ISceneNode *node,
                                                                    E_BODY_TYPE type,
                                                                    f32 mass,
                                                                    core::vector3df size,
                                                                    SKeyMap *keyMapArray = 0,
                                                                    u32 keyMapSize = 0,
                                                                    f32 moveSpeed = 100,
                                                                    f32 rotateSpeed = 10,
                                                                    bool fixedUpVector = true,
                                                                    bool strafe = false) = 0;

       //! add a controllable physics node using a irrlicht node
       /*! this function is used for bodies of the type EBT_CONE, EBT_CAPSULE, EBT_CYLINDER and
       EBT_CHAMFER_CYLINDER (vector2df is used for size)
           \param node: the irrlicht node to represent the physics node
           \param type: the body type
           \param mass: the mass of the body
           \param size: the size of the body
           \param rotation: the rotaion of the node (this is here because the newton collision isn't always
           rotated right. Use showCollision() to see if the body is rotatet right.
           \param keyMapArray: the keys to control the camera
           \param keyMapSize: the size of the SKeyMap
           \param moveSpeed: the move speed of the camera
           \param rotateSpeed: the rotate speed of the camera
           \param fixedUpVector: does the node always stand upright
           \param strafe: does the node strafe
           \return Returns a pointer to the created physics node */
       virtual IControllablePhysicsNode* addControllablePhysicsNode(scene::ISceneNode *node,
                                                                    E_BODY_TYPE type,
                                                                    f32 mass,
                                                                    core::vector2df size,
                                                                    core::vector3df rotation = core::vector3df(0,0,0),
                                                                    SKeyMap *keyMapArray = 0,
                                                                    u32 keyMapSize = 0,
                                                                    f32 moveSpeed = 100,
                                                                    f32 rotateSpeed = 10,
                                                                    bool fixedUpVector = true,
                                                                    bool strafe = false) = 0;

       //! add a physics level node
       /*! make a level collidable. This also rezises the newton world to fir the size of the level.
       Thanks to Mercior for his Newton/Irrlicht example!
           \param levelNode: the scene node that holds the level mesh
           \param mesh: if the node has no mesh, pass the mesh here
           \return Returns a pointer to the created physics node */
       virtual IPhysicsNode *addPhysicsLevelNode(scene::ISceneNode *levelNode, scene::IMesh *mesh = 0) = 0;
       virtual IPhysicsNode *addPhysicsLevelNode2T(scene::ISceneNode *levelNode, scene::IMesh *mesh = 0) = 0;


       //! add a FPS camera
       /*! This is different than the other physics node in two ways: The addCameraPhysicsNodeFPS doesn't take
       an irrlicht scene node as parametre, because it makes the camera it self and the ICameraFPSPhysicsNode
       holds a ICameraSceneNode in stead of a ISceneNode.
           \param type: the body type
           \param mass: the mass of the body
           \param size: the size of the body
           \param keyMapArray: the keys to control the camera
           \param keyMapSize: the size of the SKeyMap
           \param moveSpeed: the move speed of the camera
           \param rotateSpeed: the rotate speed of the camera (the rotation speed of the irrlicht camera scene node)
           \param fixUpVector: fix the up vector of the camera?
           \return Returns a pointer to the created physics node */
       virtual ICameraFPSPhysicsNode *addCameraPhysicsNodeFPS(E_BODY_TYPE type,
                                                              f32 mass,
                                                              core::vector3df size,
                                                              SKeyMap *keyMapArray = 0,
                                                              u32 keyMapSize = 0,
                                                              f32 moveSpeed = 100,
                                                              f32 rotateSpeed = 100,
                                                              bool fixUpVector = true) = 0;

       //! add a FPS camera
       /*! This is different than the other physics node in two ways: The addCameraPhysicsNodeFPS doesn't take
       an irrlicht scene node as parametre, because it makes the camera it self and the ICameraFPSPhysicsNode
       holds a ICameraSceneNode in stead of a ISceneNode.
           \param type: the body type
           \param mass: the mass of the body
           \param size: the size of the body
           \param rotation: the rotaion of the node (this is here because the newton collision isn't always
           rotated right. Use showCollision() to see if the body is rotatet right.
           \param keyMapArray: the keys to control the camera
           \param keyMapSize: the size of the SKeyMap
           \param moveSpeed: the move speed of the camera
           \param rotateSpeed: the rotate speed of the camera (the rotation speed of the irrlicht camera scene node)
           \param fixUpVector: fix the up vector of the camera?
           \return Returns a pointer to the created physics node */
       virtual ICameraFPSPhysicsNode *addCameraPhysicsNodeFPS(E_BODY_TYPE type,
                                                              f32 mass,
                                                              core::vector2df size,
                                                              core::vector3df rotation = core::vector3df(0,0,0),
                                                              SKeyMap *keyMapArray = 0,
                                                              u32 keyMapSize = 0,
                                                              f32 moveSpeed = 100,
                                                              f32 rotateSpeed = 100,
                                                              bool fixUpVector = true) = 0;

        //! add a FPS camera
       /*! This is different than the other physics node in two ways: The addCameraPhysicsNodeFPS doesn't take
       an irrlicht scene node as parametre, because it makes the camera it self and the ICameraFPSPhysicsNode
       holds a ICameraSceneNode in stead of a ISceneNode.
           \param type: the body type
           \param mass: the mass of the body
           \param mesh: use this if you want to create the collision from a mesh.
           \param keyMapArray: the keys to control the camera
           \param keyMapSize: the size of the SKeyMap
           \param moveSpeed: the move speed of the camera
           \param rotateSpeed: the rotate speed of the camera (the rotation speed of the irrlicht camera scene node)
           \param fixUpVector: fix the up vector of the camera?
           \return Returns a pointer to the created physics node */
       virtual ICameraFPSPhysicsNode *addCameraPhysicsNodeFPS(E_BODY_TYPE type,
                                                              f32 mass,
                                                              scene::IMesh *mesh,
                                                              SKeyMap *keyMapArray = 0,
                                                              u32 keyMapSize = 0,
                                                              f32 moveSpeed = 100,
                                                              f32 rotateSpeed = 100,
                                                              bool fixUpVector = true) = 0;

       //! get the gravity
       /*! \return returns the gravity */
       virtual core::vector3df getGravity() = 0;

       //! set the gravity
       /*! \param newGravity: the gravity of the world */
       virtual void setGravity(core::vector3df newGravity) = 0;

       //! get the irrlicgt device
       /*! \return returns the irrlicht device passed to the physics manager on creation */
       virtual IrrlichtDevice* getIrrlichtDevice() = 0;

       //! get the newton world
       /*! \return returns the newton world passed to the physics manager on creation */
       virtual NewtonWorld* getNewtonWorld() = 0;

       //! set a pointer in the CreatedPhysicsNodes array to 0
       virtual void setPointerToZero(s32 index) = 0;

       //! update the newton world
       /*! this also updates stuff in the world that was not added with Tumle */
       virtual void update() = 0;

       //! show the newton collision geometry
       /*! this should be placed between beginScene() and endScene() in the main loop */
       virtual void showCollision() = 0;

       //! get the material manager
       /*! \return returns the material manager */
       virtual IMaterialManager* getMaterialManager() = 0;

       //! get the update interval
       /*! \return returns the update interval in milliseconds */
       virtual f32 getUpdateInterval() = 0;

       //! set the update interval
       /*! \param ms: the update interval in milliseconds */
       virtual void setUpdateInterval(f32 ms = 10) = 0;

       //! get a node from its name
       /*! \return Returns the first node of the passed name. */
       virtual IPhysicsNode *getPhysicsNodeFromName(const c8 *name) = 0;

       //! get the leave world event
       /*! \return returns a pointer to the leave world event function */
       virtual void (*getLeaveWorldEvent())(IPhysicsNode*) = 0;

       //! set the function to call when a physics node leaves the newton world
       /*! \param leaveWorldEvent: the function to call when a physics node leaves the world */
       virtual void setLeaveWorldEvent(void (*leaveWorldEvent)(IPhysicsNode *node)) = 0;

       //! set the size of the newton world
       /*! \param min: the min point of the world
           \param max: the max point of the world */
       virtual void setWorldSize(core::vector3df min, core::vector3df max) = 0;

       //! set the size of the newton world
       /*! \param min: the vector to store the min point in.
           \param max: the vector to store the max point in. */
       virtual void getWorldSize(core::vector3df &min, core::vector3df &max) = 0;

};

}//end namespace tumle

#endif
