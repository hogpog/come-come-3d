// CPhysicsNode.cpp
// a part of Tumle

#include "CPhysicsNode.h"
#include "CMaterialInteraction.h"

namespace tumle
{

//! constructor
CPhysicsNode::CPhysicsNode(IPhysicsManager *pmgr,
                           s32 managerIndex,
                           scene::ISceneNode *node,
                           NewtonBody *body,
                           E_BODY_TYPE type,
                           f32 mass):
IPhysicsNode(), PhysicsManager(pmgr), Index(managerIndex), Node(node),
Body(body), BodyType(type),Type(EPNT_STANDARD), Mass(mass), Material(0), UserData(0), Gravity(0,0,0),
useDefaultTransformCallback(true), useDefaultForceAndTorqueCallback(true)
{
    //reset the force and torque array
    force[0] = 0; force[1] = 0; force[2] = 0;
    torque[0] = 0; torque[1] = 0; torque [2] = 0;

    //reset addition callbacks
    additionalForceAndTorqueCallback = 0;
    additionalTransformCallback = 0;

}

//! destructor
CPhysicsNode::~CPhysicsNode()
{

    if(Body)
        NewtonDestroyBody(PhysicsManager->getNewtonWorld(), Body);
/*
    //remove the node from any material interaction class
    if(Material)
    {
        core::array<IMaterialInteraction*> createdInteractions = PhysicsManager->getMaterialManager()->getCreatedInteractions();
        IMaterialInteraction *mi = 0;
        for(s32 i = 0 ; i <  createdInteractions.size(); i++)
        {
            mi = createdInteractions[i];

            if(mi->getCollisionNodeOne()->getIrrlichtNode() == Node)
                mi->removeCollisionNodes();

            if(mi->getCollisionNodeTwo()->getIrrlichtNode() == Node)
                mi->removeCollisionNodes();

        };
    }//if Material
*/
    if(Node)
        Node->remove();

}

//! delete the physics node
void CPhysicsNode::remove()
{
    //set the mass of the body to be removed to 0, this helps with some errors
    NewtonBodySetMassMatrix(Body, 0,0,0,0);

    //remove the pointer to the node from the array in the physics manager
    PhysicsManager->setPointerToZero(Index);
    delete this;
}


//! set the position of the physics node
void CPhysicsNode::setPosition(core::vector3df newPos)
{
    //move the scene node
    Node->setPosition(newPos);

    //move the body to the new position with a matrix4
    core::matrix4 irrMat;
	irrMat.setTranslation(newPos);
	irrMat.setRotationDegrees(getRotation());
    NewtonBodySetMatrix(Body, irrMat.pointer());

}

//! set the rotation of the physics node
void CPhysicsNode::setRotation(core::vector3df newRot)
{
    //rotate the scene node
    Node->setRotation(newRot);

    //rotate the body with a matrix4
    core::matrix4 irrMat;
    irrMat.setTranslation(getPosition());
	irrMat.setRotationDegrees(newRot);
    NewtonBodySetMatrix(Body, irrMat.pointer());
}


void CPhysicsNode::setMaterial(materialID material)
{
    //set the material of the newton body
    NewtonBodySetMaterialGroupID(Body, material);
}

void CPhysicsNode::addLocalForce(core::vector3df force, core::vector3df position)
{
    float com[3];
    NewtonBodyGetCentreOfMass(Body, com);
    core::vector3df r(com[0] - position.X,
                      com[1] - position.Y,
                      com[2] - position.Z);
    core::vector3df torque;
    torque = r.crossProduct(force);

    addForce(force);
    addTorque(torque);
}

}//end namespace tumle

