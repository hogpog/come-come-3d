// ICameraFPSPhysicsNode.h
// a part of Tumle

#ifndef TUMLE_I_CAMERA_FPS_PHYSICS_NODE_H
#define TUMLE_I_CAMERA_FPS_PHYSICS_NODE_H

#include "tumle.h"

using namespace irr;

namespace tumle
{

//! FPS physics camera interface
/*! This is a FPS camera controlled with the keyboard and the mouse */
class ICameraFPSPhysicsNode : public IPhysicsNode
{
public:
    //! constructor
    ICameraFPSPhysicsNode():IPhysicsNode() {};


    //! get the irrlicht camera scene node of the physics node
    virtual scene::ICameraSceneNode* getIrrlichtNode() = 0;

    //! update the position of the node
    /*! use this function in the event receiver to be able to controll the node */
    virtual void update(SEvent *event) = 0;

    //! set the disered velocity of the node
    /*! this function is used by the update function */
    virtual void setDesiredVelocity(core::vector3df newVeloc) = 0;

    //! get the disered velocity of the node
    /*! this function is used by the update function */
    virtual core::vector3df getDesiredVelocity() = 0;

    //!get the move speed
    /*! \return returns the move speed of the node */
    virtual f32 getMoveSpeed() = 0;

    //! get the rotation speed
    /*! \return returns the rotation speed of the node */
    virtual f32 getRotateSpeed() = 0;

    //!set the move speed
    virtual void setMoveSpeed(f32 newSpeed) = 0;

    //! get the rotation speed
    virtual void setRotateSpeed(f32 newRotSpeed) = 0;

    //! set if the force should be capped
    virtual void setCapForceMode(bool mode) = 0;

    //! get if the force is capped
    virtual bool getCapForceMode() = 0;

    //! set max/min force
    virtual void setCapForceAt(f32 cap) = 0;

    //! get the force cap
    virtual f32 getCapForceAt() = 0;

    //! caps the passed array
    //! \param force: an array of 3 floats
    /*! this function does nothing more on it own, other than taking the top of to large values in the
    passed array */
    virtual void capForceDo(f32* force) = 0;

//    //! fix the verical vector of the camera
//    /*! this will stop the camera from falling over */
//    virtual void fixVerticalVector(bool fix) = 0;

//    //! the the horizontal vector of the camera
//    /*! this wil prevent the camera from spinnig */
//    virtual void fixHorizontalVector(bool fix) = 0;

};

}//end namespace tumle

#endif

